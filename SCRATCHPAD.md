## Busy System

Player 100
NPC1   0
NPC2   90
NPC3   110


## DialogSystem

http://www.gamasutra.com/view/feature/132116/defining_dialogue_systems.php

http://www.valvesoftware.com/publications/2012/GDC2012_Ruskin_Elan_DynamicDialog.pdf

topic
dialog


## Command, Task, Action

[http://gafferongames.com/game-physics/fix-your-timestep/]

- [ ] Every command can either change the game state directly or involve something that has needs `time`.

- [x] Every "living" entity has an action queue.

- [ ] Every "living" entity has a timePenalty, the time it needs to wait before executing another action.

- [ ] Every action adds a timePenalty to the entity executing it.

- [ ] An action can only be executed when the entity has no timePenalty.

- [ ] A turn is defined by the player doing something (even waiting). Therefore
      the nextAction of the player must be executed every turn.

- [ ] When the player has made his move, the timePenalty of the player is
      removed from the timePenalty of all other entities.

- [ ] While the timePenalty of an entity is equal or less than 0, it can
      execute an action

- [ ] A turn is finished when no units can execute another action

- [ ] The engine is updated twice: Once for the player and once for the other entities

- [ ] A negative timePenalty is allowed only if the entity has a nextAction, otherwise it should be `0`

```coffeescript
ActionSystem.update = ->
    if player.nextAction
        player.nextAction.execute()
        player.timePenalty += player.nextAction.timePenalty

    # check whether turn is finished
    turnFinished = true
    for entity in entities
    	continue if entity == player
        entity.timePenalty -= player.timePenalty
        if entity.nextAction != null && entity.timePenalty <= 0
            turnFinished = false

    updateAllSystems()

    while not turnFinished
        for every other entity
            if entity.nextAction && entity.timePenalty < entity.nextAction.timePenalty
                    entity.nextAction.execute()
                    entity.timePenalty += entity.nextAction.timePenalty
```

### Examples
|Entity|timePenalty|Action(timePenalty)|
|------|-----------|-------------------|
|Player|0          |Move(100)          |
|NPC1  |0          |Move(40)           |
|NPC2  |0          |Move(40)           |

` vim: set nospell:`
