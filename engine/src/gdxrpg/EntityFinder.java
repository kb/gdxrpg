package gdxrpg;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;

public class EntityFinder {

	private static final Logger log = LoggerFactory.getLogger(EntityFinder.class);

	final Set<Class<? extends Component>> includeComponents = new HashSet<>();
	final Set<Class<? extends Component>> excludeComponents = new HashSet<>();
	final List<Predicate<Entity>> preds = new ArrayList<>();
	boolean includeInventory = false;
	boolean includeMap = true;
	String roomName = null;
	String label = null;
	int radius = -1;
	Pattern labelPattern = null;
	String labelOrId = null;

	public EntityFinder()
	{
	}

	public EntityFinder(EntityFinder clone)
	{
		this.includeComponents.addAll(clone.includeComponents);
		this.excludeComponents.addAll(clone.excludeComponents);
		this.preds.addAll(clone.preds);
		this.roomName = clone.roomName;
		this.label = clone.label;
		this.radius = clone.radius;
	}

	private List<Entity> filterCandidates(Position position, Iterable<Entity> entities)
	{
		List<Entity> ret = new ArrayList<>();
		if (radius >= 0 && position != null)
		{
			matches(t -> CM.get(t, Position.class).distanceTo(position) < radius);
		}
		if (labelPattern != null)
		{
			if (labelOrId != null)
			{
				matches(t -> {
					if (CM.has(t, Description.class) && labelPattern.matcher(CM.get(t, Description.class).name).find())
					{
						return true;
					}
					if (String.valueOf(t.getId()).startsWith(labelOrId))
					{
						return true;
					}
					return false;
				});
			}
			else
			{
				matches(t -> {
					// log.debug("Description: {}", description);
					return labelPattern.matcher(CM.get(t, Description.class).name).find();
				});
			}
		}
		Predicate<Entity> masterPred = t -> getFamily().matches(t);
		for (int i = 0; i < preds.size(); i++)
		{
			masterPred = masterPred.and(preds.get(i));
		}
		for (Entity candidate : entities)
		{
			if (masterPred.test(candidate))
			{
				ret.add(candidate);
			}
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	private Family getFamily()
	{
		return Family
				.exclude(excludeComponents.toArray(new Class[excludeComponents.size()]))
				.all(includeComponents.toArray(new Class[includeComponents.size()]))
				.get();
	}

	public EntityFinder excludeComponent(Class<? extends Component> c)
	{
		excludeComponents.add(c);
		return this;

	}

	public EntityFinder excludeComponent(String string)
	{
		try
		{
			this.excludeComponent(CM.fromSimpleName(string));
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return this;
	}

//	public List<Entity> find(BaseGame game)
//	{
//		return find(null, game);
//	}

	public List<Entity> find(Position position, BaseGame game)
	{
		List<Entity> ret = new ArrayList<>();
		if (includeInventory)
		{
			for (Entity entity : game.get(PlayerSystem.class).get(ItemContainer.class).contents)
			{
				if (getFamily().matches(entity))
					ret.add(entity);
			}

		}
		if (includeMap)
		{
			if (position == null)
			{
				for (Entity entity : game.getEntitiesFor(getFamily()))
				{
					ret.add(entity);
				}
			}
			else
			{
				includeComponent(Position.class);
				if (radius == 0)
				{
					ret.addAll(game.get(LevelSystem.class).getEntitiesAt(position, getFamily()));
				}
				else
				{
					for (Entity entity : game.getEntities())
					{
						ret.add(entity);
					}
				}
			}
		}
		return filterCandidates(position, ret);
	}

	public EntityFinder includeComponent(Class<? extends Component> c)
	{
		includeComponents.add(c);
		return this;
	}

	public EntityFinder includeComponent(String string)
	{
		try
		{
			this.excludeComponent(CM.fromSimpleName(string));
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return this;
	}

	public EntityFinder label(Pattern labelPattern)
	{
		includeComponent(Description.class);
		this.labelPattern = labelPattern;
		return this;
	}

	public EntityFinder label(String string)
	{
		return this.label(Pattern.compile(".*" + string + ".*"));
	}

	public EntityFinder labelOrId(String string)
	{
		this.label(string);
		this.labelOrId = string;
		return this;
	}

	public EntityFinder matches(Predicate<Entity> pred)
	{
		preds.add(pred);
		return this;
	}

	public <C extends Component> EntityFinder matchesComponent(Class<C> clazz, Predicate<C> pred)
	{
		includeComponent(clazz);
		matches(e -> pred.test(CM.get(e, clazz)));
		return this;
	}

	public EntityFinder radius(int radius)
	{
		includeComponent(Position.class);
		this.radius = radius;
		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (Field field : getClass().getDeclaredFields())
		{
			try
			{
				String name = field.getName();
				Object value = field.get(this);
				if (value == null)
					continue;
				switch (name) {
					case "log":
						continue;
					case "excludeComponents":
					case "includeComponents":
						List<Class<?>> list = new ArrayList<>((Collection<Class<?>>) value);
						if (list.size() == 0)
							continue;
						sb.append(name).append(": ");
						sb.append("[");
						for (int i = 0; i < list.size(); i++)
						{
							sb.append(list.get(i).getSimpleName());
							if (i + 1 < list.size())
							{
								sb.append(", ");
							}
						}
						sb.append("]");
						break;
					default:
						sb.append(name).append(": ").append(value);
				}
				sb.append(", ");
			} catch (IllegalArgumentException | IllegalAccessException e)
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return sb.toString();
	}

	public EntityFinder includeInventory(boolean includeInventory)
	{
		this.includeInventory = includeInventory;
		return this;
	}

	public EntityFinder includeMap(boolean includeMap)
	{
		this.includeMap = includeMap;
		return this;
	}

	public EntityFinder visible()
	{
		matchesComponent(Renderable.class, r -> r.visibility > 0);
		return this;
	}
}
