package gdxrpg;

import gdxrpg.command.CommandInvocation;
import gdxrpg.ecs.systems.RuleSystem;

import org.bitbucket.kb.keypress.KeyPress;

public class BaseModeHandler implements GameModeHandler {

	protected BaseGame game;

	public BaseModeHandler(BaseGame game)
	{
		this.game = game;
	}

	@Override
	public void onExit()
	{

	}

	@Override
	public void onEnter()
	{
		game.setProcessing(false);
	}

	@Override
	public boolean handleInputCommand(CommandInvocation invoc)
	{
		if (game.get(RuleSystem.class).trigger(invoc))
		{
			invoc.invoke(game);
		}
		return true;
	}

	@Override
	public boolean handleKey(KeyPress key)
	{
		return false;
	}

}
