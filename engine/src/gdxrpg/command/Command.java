package gdxrpg.command;

import gdxrpg.BaseGame;

public interface Command {

	CommandParser getParser();

	String getSynopsis();

	String getName();
	
	void execute(BaseGame game, CommandInvocation invoc);

	void registerInterface(BaseGame game);
	
}
