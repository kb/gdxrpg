package gdxrpg.command;

import gdxrpg.BaseGame;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandInvocation {
	
	private static final Logger log = LoggerFactory.getLogger(CommandInvocation.class);
	
	public final Command command;
	public final Object[] arguments;

	public CommandInvocation(Command command, Object... arguments)
	{
		this.command = command;
		if (null == arguments)
			arguments = new Object[] {};
		this.arguments = arguments;
	}

	public <T> T arg(int i, Class<T> clazz)
	{
		return clazz.cast(arguments[i]);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(arguments);
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommandInvocation other = (CommandInvocation) obj;
		if (!Arrays.equals(arguments, other.arguments))
			return false;
		if (command != other.command)
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return String.format("CMD> %s %s", command.getClass().getSimpleName(), Arrays.asList(arguments));
	}

	public void invoke(BaseGame game)
	{
		log.trace("Executing {}", getCommand());
		command.execute(game, this);
	}
	
	public Command getCommand()
	{
		return command;
	}
	
}
