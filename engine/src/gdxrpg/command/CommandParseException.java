package gdxrpg.command;

public class CommandParseException extends Exception {
	
	final private String message;

	public CommandParseException(String explanation)
	{
		super();
		this.message = explanation;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

}
