package gdxrpg.command;

import gdxrpg.BaseGame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCommandParser implements CommandParser {
	private static final Logger log = LoggerFactory.getLogger(BaseCommandParser.class);

	public final Command command;

	public BaseCommandParser(Command command)
	{
		this.command = command;
	}

	@Override
	public String toString()
	{
		return String.format("%s", getClass().getSimpleName());
	}

	public Command getCommand()
	{
		return this.command;
	}
	
	@Override
	public void askForInput(BaseGame game, int argNo)
	{
		
	}

}
