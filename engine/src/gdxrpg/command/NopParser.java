package gdxrpg.command;

import java.util.List;

import gdxrpg.BaseGame;

class NopParser extends BaseCommandParser {

	public NopParser(Command command)
	{
		super(command);
	}

	@Override
	public CommandInvocation parse(BaseGame game, String cmdlineInput) throws CommandParseException
	{
		throw new CommandParseException("Not implemented");
	}
	
	@Override
	public boolean match(String cmdlineInput)
	{
		return false;
	}
	
	@Override
	public List<String> tokenize(String cmdlineInput)
	{
		return null;
	}

	@Override
	public String getUsage()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfArguments()
	{
		return 0;
	}

	@Override
	public String getArgumentPrompt(int argNo)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
