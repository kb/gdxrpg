package gdxrpg.command;

import gdxrpg.BaseGame;

import java.util.List;

public interface CommandParser {

	Command getCommand();

	String getUsage();

	boolean match(String cmdlineInput);

	List<String> tokenize(String cmdlineInput);

	int getNumberOfArguments();

	String getArgumentPrompt(int argNo);

	void askForInput(BaseGame game, int argNo);

	CommandInvocation parse(BaseGame game, String cmdlineInput) throws CommandParseException;

}
