package gdxrpg.command;

import gdxrpg.BaseGame;
import gdxrpg.command.tokenparser.SmartCommandParser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractCommand implements Command {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	private final CommandParser parser;
	private final String synopsis;
	private String name;

//	public AbstractCommand(String name, String synopsis, SmartCommandParser.DefaultCommandParserBuilder parserBuilder)
//	{
//		this.name = name;
//		this.parser = parserBuilder.command(this).create();
//		this.synopsis = synopsis;
//	}
	public AbstractCommand(String synopsis, SmartCommandParser.DefaultCommandParserBuilder parserBuilder)
	{
		this.name = String.join("-", StringUtils.splitByCharacterTypeCamelCase(getClass().getSimpleName())).toLowerCase();
		this.parser = parserBuilder.command(this).create();
		this.synopsis = synopsis;
	}
	
	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public CommandParser getParser()
	{
		return parser;
	}

	@Override
	public String getSynopsis()
	{
		return synopsis;
	}

	@Override
	public void registerInterface(BaseGame game)
	{
		
	}

}
