package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.command.CommandParseException;

public interface SmartCommandParserToken {
	
	Object parseToken(BaseGame game, String parsedToken) throws CommandParseException;
	String askForInput(BaseGame game);

}