package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.CommandParseException;
import gdxrpg.command.builtin.Eval;

import java.util.function.Predicate;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ClassToken<T> extends AbstractSmartCommandParserToken {

	private static final Logger log = LoggerFactory.getLogger(ClassToken.class);

	private final Class<T> tokenClass;
	private final Predicate<T> predicate;

	public ClassToken(Class<T> tokenClass)
	{
		this(tokenClass, null);
	}

	public ClassToken(Class<T> tokenClass, Predicate<T> predicate)
	{
		this.tokenClass = tokenClass;
		this.predicate = predicate;
	}

	public boolean isSubTypeOf(Class<?> parent)
	{
		return tokenClass.isAssignableFrom(parent);
	}

	public Class<?> getTokenClass()
	{
		return tokenClass;
	}

	@Override
	public String toString()
	{
		return "<" + tokenClass.getSimpleName() + ">";
	}

	@Override
	public Object parseToken(BaseGame game, String parsedToken) throws CommandParseException
	{
		log.trace("Class token: {}", getTokenClass());
		if (isSubTypeOf(String.class))
		{
			return parsedToken;
		}
		if (isSubTypeOf(Character.class))
		{
			return parsedToken.charAt(0);
		}
		else if (isSubTypeOf(KeyPress.class))
		{
			return KeyPress.fromVimString(parsedToken);
		}
		else if (isSubTypeOf(CommandInvocation.class))
		{
			log.trace("EVAL {}", parsedToken);
			return new Eval().getParser().parse(game, "eval " + parsedToken);
		}
		throw new RuntimeException(String.format("Unhandled class %s", getTokenClass()));
	}

	@Override
	public String askForInput(BaseGame game)
	{
		game.addMessage("Add somethign of class " + tokenClass);
		return null;
	}
}