package gdxrpg.command.tokenparser;

public interface InputReceiver {
	public void onInputReceived(String input);
}