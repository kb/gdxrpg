package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.command.CommandParseException;

class StringConstantToken extends AbstractSmartCommandParserToken {

	private final String stringConstant;

	public StringConstantToken(String stringConstant)
	{
		this.stringConstant = stringConstant;
	}

	public String getStringConstant()
	{
		return stringConstant;
	}
	
	@Override
	public String toString()
	{
		return stringConstant;
	}

	@Override
	public Object parseToken(BaseGame game, String parsedToken) throws CommandParseException
	{
		return getStringConstant();
	}

	@Override
	public String askForInput(BaseGame game)
	{
		game.addMessage("Add an enum");
		return null;
	}
}