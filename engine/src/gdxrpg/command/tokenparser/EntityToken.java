package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.CommandParseException;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public class EntityToken extends AbstractSmartCommandParserToken {

	private static final Logger log = LoggerFactory.getLogger(EntityToken.class);

	private final EntityFinder finder;

	public EntityToken(EntityFinder finder)
	{
		this.finder = finder;
	}

	public EntityFinder getFinder()
	{
		return finder;
	}

	@Override
	public String toString()
	{
		return "<Entity[" + finder + "]>";
	}

	@Override
	public Object parseToken(BaseGame game, String parsedToken) throws CommandParseException
	{
		log.debug("Resolving {}", parsedToken);
		EntityFinder finder = new EntityFinder(getFinder());
		log.debug("FINDER: {}", finder.toString());
		finder.labelOrId(parsedToken);
		List<Entity> find = finder.find(game.getPlayer(Position.class), game);
		log.debug("Resolving '{}' -> {}", parsedToken, find);
		if (find.size() == 1)
		{
			return find.get(0);
		}
		else if (find.size() == 0)
		{
			throw new CommandParseException(String.format("Could not resolve '%s'", parsedToken));
		}
		else if (find.size() > 1)
		{
			throw new CommandParseException(String.format("Ambiguous '%s':\n%s",
					parsedToken,
					game.get(LevelSystem.class).entitiesWithPath(game.get(PlayerSystem.class).get(), find)));
		}
		else
		{
			throw new CommandParseException("PROBLEM");
		}
	}

	@Override
	public String askForInput(BaseGame game)
	{
		game.addMessage("Add an entity");
		List<Entity> find = getFinder().find(game.getPlayer(Position.class), game);
		String entitiesWithPath = game.get(LevelSystem.class).entitiesWithPath(game.getPlayer(), find);
		game.addMessage(entitiesWithPath);
		return null;
	}

}
