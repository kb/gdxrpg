package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.BaseCommandParser;
import gdxrpg.command.Command;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.CommandParseException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartCommandParser extends BaseCommandParser {

	private static final Logger log = LoggerFactory.getLogger(SmartCommandParser.class);
	private final List<SmartCommandParserToken> tokens;

	public SmartCommandParser(Command command, List<SmartCommandParserToken> tokens)
	{
		super(command);
		this.tokens = tokens;
	}

	@Override
	public CommandInvocation parse(BaseGame game, String cmdlineInput) throws CommandParseException
	{
		if (tokens.size() <= 1)
		{
			return new CommandInvocation(getCommand());
		}
		List<String> tokenized = tokenize(cmdlineInput);
		log.trace("Tokens: {}", tokens);
		log.trace("Tokenized: {}", tokenized);
		if (tokenized.size() != tokens.size())
		{
			final String problem;
			if (tokenized.size() < tokens.size())
			{
				problem = String.format("Not enough arguments (%s < %s)\n%s\n%s", tokenized.size(), tokens.size(), tokens, tokenized);
			}
			else
			{
				problem = String.format("Too many arguments (%s > %s)\n%s", tokenized.size(), tokens.size(), tokenized);
			}
			throw new CommandParseException(String.format("%s\n Usage: %s", problem, tokens));
		}
		log.trace("Tokenized: {}", tokenized);
		log.trace("Tokens: {}", tokens);
		Object[] invocationArgs = new Object[tokens.size() - 1];
		for (int i = 1; i < tokens.size(); i++)
		{
			SmartCommandParserToken token = tokens.get(i);
			log.trace("Interpreting token as {}", token.getClass().getSimpleName());
			String parsedToken = tokenized.get(i);
			invocationArgs[i - 1] = token.parseToken(game, parsedToken);
		}
		return new CommandInvocation(getCommand(), invocationArgs);
	}

	@Override
	public String toString()
	{
		return String.format("%s %s", super.toString(), tokens);
	}

	@Override
	public List<String> tokenize(String cmdlineInput)
	{
//		log.debug("TOKSI: {} " , tokens.size());
//		log.error("HERE DEBUG, {}", cmdlineInput);
//		log.error("HERE DEBUG");
		return Arrays.asList(cmdlineInput.split(" ", tokens.size()));
	}

	@Override
	public boolean match(String cmdlineInput)
	{
		String startString = StringConstantToken.class.cast(tokens.get(0)).getStringConstant();
//		log.debug("CMDLINE: {}", cmdlineInput);
//		log.debug("TOKENS: {}", tokens);
//		log.debug("START STRING: {}", startString);
		if (cmdlineInput.toLowerCase().startsWith(startString.toLowerCase()))
		{
//			log.debug("YAY MATCHED");
			return true;
		}
		return false;
	}

	public static DefaultCommandParserBuilder builder()
	{
		return new DefaultCommandParserBuilder();
	}

	public static class DefaultCommandParserBuilder {
		private final List<SmartCommandParserToken> tokens = new ArrayList<>();
		private Command command;

		public DefaultCommandParserBuilder()
		{
		}

		public <T> DefaultCommandParserBuilder token(Class<T> tokenClass)
		{
			tokens.add(new ClassToken<T>(tokenClass));
			return this;
		}

		public <T> DefaultCommandParserBuilder token(Class<T> tokenClass, Predicate<T> pred)
		{
			tokens.add(new ClassToken<T>(tokenClass, pred));
			return this;
		}

		public DefaultCommandParserBuilder entity(EntityFinder finder)
		{
			tokens.add(new EntityToken(finder));
			return this;
		}

		public DefaultCommandParserBuilder string(String string)
		{
			tokens.add(new StringConstantToken(string));
			return this;
		}

		public BaseCommandParser create()
		{
			if (null == command) {
				throw new RuntimeException("Must set command for parser");
			}
			tokens.add(0, new StringConstantToken(command.getName()));
			return new SmartCommandParser(command, tokens);
		}

		public <T extends Enum<T>> DefaultCommandParserBuilder enumToken(Class<T> enumClass)
		{
			tokens.add(new EnumToken<T>(enumClass));
			return this;
		}

		public DefaultCommandParserBuilder command(Command command)
		{
			this.command = command;
			return this;
		}

	}

	@Override
	public String getUsage()
	{
		StringBuilder sb = new StringBuilder();
		for (SmartCommandParserToken token : tokens) {
			sb.append(token.toString());
			sb.append(" ");
		}
		return sb.toString();
	}

	@Override
	public int getNumberOfArguments()
	{
		return tokens.size() - 1;
	}

	@Override
	public String getArgumentPrompt(int argNo)
	{
		return tokens.get(argNo + 1).getClass().getSimpleName();
	}

	@Override
	public void askForInput(BaseGame game, int argNo)
	{
		tokens.get(argNo + 1).askForInput(game);
		
	}
}
