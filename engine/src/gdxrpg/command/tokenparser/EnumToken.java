package gdxrpg.command.tokenparser;

import gdxrpg.BaseGame;
import gdxrpg.command.CommandParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class EnumToken<T extends Enum<T>> extends AbstractSmartCommandParserToken {
	private static final Logger log = LoggerFactory.getLogger(EnumToken.class);
	private final Class<T> tokenEnum;

	public EnumToken(Class<T> tokenEnum)
	{
		this.tokenEnum = tokenEnum;
	}

	public T parseToken(BaseGame game, String in) throws CommandParseException {
		for (T t : tokenEnum.getEnumConstants()) {
			log.trace("{} == {}", in, t);
			if (in.toLowerCase().equals(t.name().toLowerCase())) {
				return t;
			}
		}
		throw new CommandParseException(String.format("Could not parse %s as %s", in, tokenEnum));
	}

	public Class<?> getTokenEnum()
	{
		return tokenEnum;
	}

	@Override
	public String toString()
	{
		return "<Enum[" + tokenEnum.getSimpleName() + "]>";
	}

	@Override
	public String askForInput(BaseGame game)
	{
		game.addMessage("Add an enum");
		for (T t : tokenEnum.getEnumConstants()) {
			game.addMessage(t.name());
		}
		return null;
	}

}