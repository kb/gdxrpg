package gdxrpg.command;

import gdxrpg.BaseGame;

import java.util.Arrays;
import java.util.List;


public class NoArgParser extends BaseCommandParser{

	public NoArgParser(Command command, String string)
	{
		super(command);
	}

//	@Override
//	public void setPrefixes(List<String> prefixes)
//	{
//		StringBuilder sb = new StringBuilder();
//		sb.append("^");
//		sb.append("\\s*");
//		sb.append(String.join("|", prefixes));
//		sb.append("\\b");
//		sb.append("$");
//		setPattern(Pattern.compile(sb.toString(), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
//	}

	@Override
	public CommandInvocation parse(BaseGame game, String cmdlineInput) throws CommandParseException
	{
		return new CommandInvocation(getCommand());
	}

	@Override
	public List<String> tokenize(String cmdlineInput)
	{
		return Arrays.asList(cmdlineInput);
	}

	@Override
	public boolean match(String cmdlineInput)
	{
		return cmdlineInput.equals(command.getName());
	}

	@Override
	public String getUsage()
	{
		return getCommand().getName();
	}

	@Override
	public int getNumberOfArguments()
	{
		return 0;
	}

	@Override
	public String getArgumentPrompt(int argNo)
	{
		return "";
	}

}
