package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;

public class SaveGame extends AbstractCommand{
	
	public SaveGame()
	{
		super("Save the game", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.saveGame("/tmp/gdxrpg.sav");
	}

}
