package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;

public class LoadGame extends AbstractCommand{
	
	public LoadGame()
	{
		super("Load a game", SmartCommandParser.builder());
	}
	
	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.loadGame("/tmp/gdxrpg.sav");
	}

}
