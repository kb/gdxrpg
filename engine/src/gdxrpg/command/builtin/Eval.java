package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class Eval extends AbstractCommand {

	public Eval()
	{
		super("Evaluate string as command", SmartCommandParser.builder()
				.token(String.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		String nestedCommandStr = invoc.arg(0, String.class);
		log.debug("Evaluating {}", invoc.arguments);
		log.debug("Evaluating {}", nestedCommandStr);
		game.get(ShellSystem.class).parseCmdline(nestedCommandStr);
	}

}
