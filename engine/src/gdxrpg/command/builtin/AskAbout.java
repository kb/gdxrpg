package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.components.types.NPC;

import com.badlogic.ashley.core.Entity;

public class AskAbout extends AbstractCommand {
	
	public AskAbout()
	{
		super("Ask an NPC about something", SmartCommandParser.builder()
				.entity(new EntityFinder()
					.includeComponent(NPC.class)
					)
				.token(String.class)
				);
	}
	
	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		Entity entity = invoc.arg(0, Entity.class);
		String topic = invoc.arg(1, String.class);
		game.addMessage(String.format("You ask %s about '%s'", entity, topic));
	}

}
