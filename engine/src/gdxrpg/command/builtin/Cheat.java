package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.UndirectedLightSource;
import gdxrpg.ecs.constants.CheatType;
import gdxrpg.ecs.systems.AttackSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import com.badlogic.ashley.core.Entity;

public class Cheat extends AbstractCommand {

	public Cheat()
	{
		super("Command to cheat for testing", SmartCommandParser.builder()
				.enumToken(gdxrpg.ecs.constants.CheatType.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		CheatType cheatType = invoc.arg(0, CheatType.class);
		Entity player = game.get(PlayerSystem.class).get();
		switch (cheatType) {
			case HEALTH_9999:
				CM.get(player, Health.class).health = 9999;
				break;
			case KILLALL:
				game.get(AttackSystem.class).killall();
				break;
			case SEEALL:
				game.get(PlayerSystem.class).get(UndirectedLightSource.class).radius = 1000;
				break;
		}
	}

}
