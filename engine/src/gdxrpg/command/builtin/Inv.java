package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.constants.GameMode;

public class Inv extends AbstractCommand{
	
	public Inv()
	{
		super("Open the inventory", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.enterMode(GameMode.INVENTORY_MODE);
	}

}
