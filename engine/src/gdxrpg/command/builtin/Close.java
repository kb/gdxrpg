package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.components.Openable;
import gdxrpg.ecs.systems.OpenCloseSystem;

import com.badlogic.ashley.core.Entity;

public class Close extends AbstractCommand {

	public Close()
	{
		super("Close something around the player", SmartCommandParser.builder()
				.entity(new EntityFinder()
						.includeComponent(Openable.class)
						.visible()));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		Entity entity = invoc.arg(0, Entity.class);
		log.debug("Closing {}", entity);
		game.get(OpenCloseSystem.class).close(entity);
	}

}
