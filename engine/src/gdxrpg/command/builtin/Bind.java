package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.MainInputHandler;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.constants.GameMode;

import org.bitbucket.kb.keypress.KeyPress;

public class Bind extends AbstractCommand {

	public Bind()
	{
		super("Bind a key to a command", SmartCommandParser.builder()
				.enumToken(GameMode.class)
				.token(KeyPress.class)
				.token(CommandInvocation.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		GameMode mode = invoc.arg(0, GameMode.class);
		KeyPress nestedKey = invoc.arg(1, KeyPress.class);
		CommandInvocation nestedInvoc = invoc.arg(2, CommandInvocation.class);
		log.debug("Create key binding for mode {}, key {}, invoc {}", mode, nestedKey, nestedInvoc);
		MainInputHandler.mappings.get(mode).put(nestedKey, nestedInvoc);
	}

}
