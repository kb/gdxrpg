package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.constants.GameMode;

public class Cmdline extends AbstractCommand{
	
	public Cmdline()
	{
		super("Start the CommandLine mode", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.enterMode(GameMode.SHELL_MODE);
	}

}
