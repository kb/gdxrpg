package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class ShellLeft extends AbstractCommand {

	public ShellLeft()
	{
		super("Move cursor left", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(ShellSystem.class).left();
	}

}
