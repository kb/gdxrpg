package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class ShellInsert extends AbstractCommand {

	public ShellInsert()
	{
		super("Insert a character", SmartCommandParser.builder()
				.token(Character.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(ShellSystem.class).insert(invoc.arg(0, Character.class));
	}

}
