package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.systems.PlayerSystem;

import com.badlogic.ashley.core.Entity;

public class Possess extends AbstractCommand {

	public Possess()
	{
		super("Possess an NPC", SmartCommandParser.builder()
				.entity(new EntityFinder()
						.includeComponent(NPC.class)
				));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(PlayerSystem.class).possess(invoc.arg(0, Entity.class));

	}

}
