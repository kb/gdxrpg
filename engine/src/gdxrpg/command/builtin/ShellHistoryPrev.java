package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class ShellHistoryPrev extends AbstractCommand {

	public ShellHistoryPrev()
	{
		super("Previous Command in history", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(ShellSystem.class).historyPrev();
	}

}
