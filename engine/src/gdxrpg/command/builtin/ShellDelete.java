package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class ShellDelete extends AbstractCommand {

	public ShellDelete()
	{
		super("Delete char under cursor", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(ShellSystem.class).delete();
	}

}
