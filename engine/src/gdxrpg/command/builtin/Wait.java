package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.PlayerSystem;

public class Wait extends AbstractCommand{
	
	public Wait()
	{
		super("Wait for 100 ticks", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(PlayerSystem.class).doNothing(100);
	}

}
