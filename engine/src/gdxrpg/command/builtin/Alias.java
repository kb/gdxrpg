package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class Alias extends AbstractCommand {

	public Alias()
	{
		super("Create an alias for another command (invocation)",
				SmartCommandParser.builder()
						.token(String.class, s -> !s.contains("1"))
						.token(String.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		String alias = invoc.arg(0, String.class);
		String cmd = invoc.arg(1, String.class);
		log.debug("Setting alias '{}' => '{}'", alias, cmd);
		game.get(ShellSystem.class).addAlias(alias, cmd);
	}

}
