package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.components.Openable;
import gdxrpg.ecs.scripting.IScript;
import gdxrpg.ecs.systems.OpenCloseSystem;
import gdxrpg.ecs.systems.RuleSystem;

import com.badlogic.ashley.core.Entity;

public class Open extends AbstractCommand {
	
	public interface OnOpenScript extends IScript {
		public void onOpen(BaseGame game, Entity toOpen);
		
	}

	public Open()
	{
		super("Open something around the player", SmartCommandParser.builder()
				.entity(new EntityFinder()
						.includeInventory(true)
						.includeComponent(Openable.class)
//						.radius(1)
						.visible()));
	}
	
	@Override
	public void registerInterface(BaseGame game)
	{
		game.get(RuleSystem.class).registerInterface(OnOpenScript.class);
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		Entity entity = invoc.arg(0, Entity.class);
		game.get(OpenCloseSystem.class).open(entity);
	}

}
