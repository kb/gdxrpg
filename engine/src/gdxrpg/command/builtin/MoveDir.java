package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.systems.MovementSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import com.badlogic.ashley.core.Entity;

public class MoveDir extends AbstractCommand {

	public MoveDir()
	{
		super("Move the player in the xy direction", SmartCommandParser.builder()
			.enumToken(DirectionHorizontal.class));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		Entity player = game.get(PlayerSystem.class).get();
		DirectionHorizontal dir = invoc.arg(0, DirectionHorizontal.class);
		game.get(MovementSystem.class).move(player, dir);
	}

}
