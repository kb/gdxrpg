package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.constants.GameMode;

public class StartHelp extends AbstractCommand{
	
	public StartHelp()
	{
		super("Open the on-line help", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.enterMode(GameMode.HELP_MODE);
	}

}
