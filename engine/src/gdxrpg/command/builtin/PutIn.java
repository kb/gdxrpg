package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.types.Item;
import gdxrpg.ecs.systems.ItemSystem;

import com.badlogic.ashley.core.Entity;

public class PutIn extends AbstractCommand {

	public PutIn()
	{
		super("Put an Item in an ItemContainer", SmartCommandParser.builder()
				.entity(new EntityFinder()
					.includeInventory(true)
                	.includeComponent(ItemContainer.class))
				.entity(new EntityFinder()
					.includeInventory(true)
					.includeComponent(Item.class)));
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		Entity container = invoc.arg(0, Entity.class);
		Entity item = invoc.arg(1, Entity.class);
		game.get(ItemSystem.class).putIn(container, item);
	}

}
