package gdxrpg.command.builtin;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.tokenparser.SmartCommandParser;
import gdxrpg.ecs.systems.ShellSystem;

public class ShellBackspace extends AbstractCommand {

	public ShellBackspace()
	{
		super("Delete to the left", SmartCommandParser.builder());
	}

	@Override
	public void execute(BaseGame game, CommandInvocation invoc)
	{
		game.get(ShellSystem.class).backspace();
	}

}
