package gdxrpg.utils;

import gdxrpg.ecs.components.position.Position;

import java.util.ArrayList;
import java.util.List;

public class MathUtils {

	public static int randomInt(int min, int max)
	{
		return min + (int) Math.rint(Math.random() * (max - min));
	}

	public static int randomInt(int max)
	{
		return randomInt(0, max);
	}
	public static List<Position> line(int x,int y,int x2, int y2) {
	    int w = x2 - x ;
	    int h = y2 - y ;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0 ;
	    if (w<0) dx1 = -1 ; else if (w>0) dx1 = 1 ;
	    if (h<0) dy1 = -1 ; else if (h>0) dy1 = 1 ;
	    if (w<0) dx2 = -1 ; else if (w>0) dx2 = 1 ;
	    int longest = Math.abs(w) ;
	    int shortest = Math.abs(h) ;
	    if (!(longest>shortest)) {
	        longest = Math.abs(h) ;
	        shortest = Math.abs(w) ;
	        if (h<0) dy2 = -1 ; else if (h>0) dy2 = 1 ;
	        dx2 = 0 ;            
	    }
	    int numerator = longest >> 1 ;
	    List<Position> ret= new ArrayList<>();
		for (int i = 0; i <= longest; i++) {
			ret.add(new Position(x, y));
			numerator += shortest;
			if (!(numerator < longest)) {
				numerator -= longest;
				x += dx1;
				y += dy1;
			} else {
				x += dx2;
				y += dy2;
			}
		}
		return ret;
	}

	public static List<Position> line(double x, double y, double x2, double y2)
	{
		return line((int)x, (int)y, (int)x2, (int)y2);
	}

	public static List<Position> line(float x, float y, float x2, float y2)
	{
		return line(Math.round(x), Math.round(y), Math.round(x2), Math.round(y2));
	}

}
