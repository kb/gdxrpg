package gdxrpg.utils;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.lang.reflect.Method;

import org.aeonbits.owner.Converter;

public class OwnerAwtFontConverter implements Converter<Font>{

	@Override
	public Font convert(Method method, String input)
	{
		String[] split = input.split(":");
		try
		{
			Font font = Font.createFont(Font.TRUETYPE_FONT, OwnerAwtFontConverter.class.getResourceAsStream(
					"/fonts/" + split[0] + ".ttf"));
			if (split.length > 1)
				font = font.deriveFont(Float.parseFloat(split[1]));
			return font;
		} catch (FontFormatException | IOException e)
		{
			throw new RuntimeException(e);
		}
	}

}
