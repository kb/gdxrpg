package gdxrpg.utils;

import org.aeonbits.owner.Config;

public interface GameMetadata extends Config {

	String title();
	String version();
	String description();
	String id();

}
