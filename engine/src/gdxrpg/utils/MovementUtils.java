package gdxrpg.utils;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public class MovementUtils {
	private static final Logger log = LoggerFactory.getLogger(MovementUtils.class);
	
	public static Position calculateNextPosition(BaseGame game, Entity entity) {
//		double x;
//		double y;
		Movement mov = CM.get(entity, Movement.class);
//		Speed speed = CM.get(entity, Speed.class);
		AbstractPosition curPos = CM.get(entity, Position.class);
		// System.out.println(entity.getComponent(Creature.class).name);
		// System.out.println(mov.dy);
//		Entity player = game.get(PlayerSystem.class).get();
//		if (entity.equals(player)) {
//			// TODO make speed work so that the player moves at most one square per move
//			x = curPos.x + mov.dx * (speed.speed / 100f);
//			y = curPos.y + mov.dy * (speed.speed / 100f);
//		}else {
//			Speed playerSpeed = CM.get(player, Speed.class);
//			x = curPos.x + mov.dx * (speed.speed) / playerSpeed.speed;
//			y = curPos.y + mov.dy * (speed.speed) / playerSpeed.speed;
////		}
//		Position newPos = new Position(x, y);
		Position newPos = new Position(curPos.x + mov.dx, curPos.y + mov.dy);
//		log.debug("{}", newPos);
		return newPos;
	}

}
