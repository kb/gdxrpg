package gdxrpg.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.io.LineReader;

public class NamingUtils extends StaticUtils {
	
	private static final List<String> NAMES;

	static {
		InputStream namesStream = NamingUtils.class.getResourceAsStream("/names-list.txt");
		LineReader reader = new LineReader(new InputStreamReader(namesStream));
		Builder<String> names = ImmutableList.builder();
		try {
			String name = reader.readLine();
			while (name != null){
				names.add(name);
				name = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		NAMES = names.build();
	}
	
	public static String getRandomName()
	{
		return NAMES.get(RNG.nextInt(NAMES.size()));
	}

}
