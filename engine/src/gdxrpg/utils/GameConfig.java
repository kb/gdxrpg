package gdxrpg.utils;

import java.awt.Font;
import java.time.LocalDateTime;
import java.util.List;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.LoadPolicy;
import org.aeonbits.owner.Config.LoadType;
import org.aeonbits.owner.Config.Sources;

@LoadPolicy(LoadType.MERGE)
@Sources({
		"file:/tmp/game-config.properties",
		"file:~/.config/${metadata.id}/config.properties",
		"file:${metadata.dir}/config.properties"
})
public interface GameConfig extends Config {

	@DefaultValue("gdxrpg.ecs.components, gdxrpg.ecs.components.position, gdxrpg.ecs.components.types")
	List<String> componentPackages();
	
	@DefaultValue("com.badlogic.ashley.core.Component, gdxrpg.ecs.components.types.TypeComponent")
	List<Class<?>> componentClasses();

	@DefaultValue("42")
	int maxKeyRepeat();

	@DefaultValue("20")
	int targetFPS();
	
	String startLevel();
	
//	@DefaultValue("SourceCodePro-Powerline-Awesome-Regular:19, ubuntu-mono:23, fontawesome-regular:22, unifont:28, DejaVuSansMono:20")
	@DefaultValue("ubuntu-mono:20, fontawesome-regular:22, unifont:28, DejaVuSansMono:20")
	@ConverterClass(OwnerAwtFontConverter.class)
	Font[] fonts();

	@DefaultValue("2015-01-01T00:00:00")
	@ConverterClass(OwnerLocalDateTimeConverter.class)
	LocalDateTime startDate();

	@DefaultValue("1000000")
	int nanosPerTick();

}
