package gdxrpg.utils;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.aeonbits.owner.Converter;

public class OwnerLocalDateTimeConverter implements Converter<LocalDateTime>{

	@Override
	public LocalDateTime convert(Method method, String input)
	{
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		return LocalDateTime.parse(input, formatter);
	}

}
