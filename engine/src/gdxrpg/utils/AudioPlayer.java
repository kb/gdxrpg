package gdxrpg.utils;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineEvent.Type;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AudioPlayer implements LineListener {

	private static final Logger log = LoggerFactory.getLogger(AudioPlayer.class);

	// private static AudioPlayer defaultPlayer = new AudioPlayer();
	//
	private Clip currentClip = null;
	private boolean done = false;

	@Override
	public synchronized void update(LineEvent event)
	{
		Type eventType = event.getType();
		if (eventType == Type.STOP || eventType == Type.CLOSE)
		{
			// log.debug("Got STOP or CLOSE event");
			done = true;
			notifyAll();
		}
	}

	public synchronized void waitUntilDone() throws InterruptedException
	{
		while (!done)
		{
			wait();
		}
		// log.debug("Finished playing.");
	}

	public static void playClip(InputStream stream)
	{
		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					AudioPlayer player = new AudioPlayer();
					player.doPlayClip(stream);
				} catch (IOException | UnsupportedAudioFileException | LineUnavailableException | InterruptedException e)
				{
					throw new RuntimeException(e);
				}
			}
		}).start();
	}

	private void doPlayClip(InputStream stream) throws IOException, UnsupportedAudioFileException, LineUnavailableException, InterruptedException
	{
//		resetPlayer();
		try ( AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(stream))
		{
			AudioFormat format = audioInputStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, format);
			currentClip = (Clip) AudioSystem.getLine(info);
			currentClip.addLineListener(this);
			currentClip.open(audioInputStream);
			try
			{
				currentClip.start();
				waitUntilDone();
			} finally
			{
				currentClip.close();
			}
		}
	}

	public void resetPlayer() throws LineUnavailableException
	{
		done = false;
		if (null != currentClip)
		{
			currentClip.close();
		}
		currentClip = AudioSystem.getClip();
	}

}
