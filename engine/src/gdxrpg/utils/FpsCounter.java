package gdxrpg.utils;

public class FpsCounter {
	private int framesThisSecond;
	private long lastUpdate;
	private long nsSinceLastSecond = 0;
	public int fps;

	public long tick()
	{
		long now = System.nanoTime();
		long nsSinceLastUpdate = now - lastUpdate;
		nsSinceLastSecond += nsSinceLastUpdate;
		framesThisSecond += 1;
		if (nsSinceLastSecond > 1_000_000_000) {
			fps = framesThisSecond;
			framesThisSecond = 0;
			nsSinceLastSecond = 0;
		}
		lastUpdate = now;
		return nsSinceLastUpdate;
	}
}