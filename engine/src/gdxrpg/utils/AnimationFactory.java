package gdxrpg.utils;

import com.badlogic.ashley.core.Entity;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.animation.AnimationBuilder;
import gdxrpg.ecs.animation.AnimationType;
import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;

public class AnimationFactory {
	public static void createShootingAnimation(Entity source, Entity target)
	{
		CM.get(source, Animatable.class).add(new AnimationBuilder()
				.speed(2f)
				.content("*")
				.color("ff0000")
				.sourcePositions(CM.get(source, Position.class))
				.targetPositions(CM.get(target, Position.class))
				.frameLag(5)
				.looping(false)
				.build(AnimationType.LINE_TO_POSITION));
	}

	public static void createBlinkingAnimation(int repeat, Entity target)
	{
		CM.get(target, Animatable.class).add(new AnimationBuilder()
				.content(CM.get(target, Renderable.class).fgChar)
				.repeat(repeat)
				.looping(false)
				.build(AnimationType.SINGLE_TILE_BLINK));
	}
	public static void createHitAnimation(Entity source, Entity target, int hit)
	{
		Position sourcePos = CM.get(source, Position.class);
		Position targetPos = CM.get(target, Position.class);
		double dx = (targetPos.x - sourcePos.x);
		double dy = (targetPos.y  - sourcePos.y);
		if (dx > 0) dx = 1;
		else if (dx < 0) dx = -1;
		if (dy > 0) dy = 1;
		else if (dy < 0) dy = -1;
		// if (sourcePos.x > targetPos.x)
		// dx = +1;
		// else if (sourcePos.x < targetPos.x)
		// dx = -1;
		// else
		// dx = 0;
		//
		// if (sourcePos.roundY() > targetPos.roundY())
		// dy = -1;
		// else if (sourcePos.y < targetPos.y)
		// dy = +1;
		// else
//		dy = 0;
		CM.get(target, Animatable.class).add(new AnimationBuilder()
				.speed(1f)
				.looping(false)
				.content(String.valueOf(hit))
				.movement(new Movement(dx, dy))
				.frameLag(0)
				.build(AnimationType.RISE_AND_FADE_TEXT));
	}


}
