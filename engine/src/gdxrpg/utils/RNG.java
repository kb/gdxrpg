package gdxrpg.utils;

public class RNG extends StaticUtils {

	public static int nextInt(int bound)
	{
		return (int) Math.floor(Math.random() * bound);
	}
	public static int nextInt(int lowerLimit, int bound)
	{
		return lowerLimit + nextInt(bound);
	}

}
