package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.systems.AttackSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import com.badlogic.ashley.core.Entity;

public class RangedAttackMode extends RangedSelectionMode {

	public RangedAttackMode(BaseGame game)
	{
		super(game, new EntityFinder()
				.includeComponent(NPC.class)
				.includeComponent(Position.class)
				.visible());
	}

	@Override
	public void onSelected(Entity targetEntity)
	{
		Entity player = game.get(PlayerSystem.class).get();
		game.get(AttackSystem.class).rangedAttack(player, targetEntity);

	}

}
