package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelpMode extends BaseModeHandler {
	
	private static final Logger log = LoggerFactory.getLogger(HelpMode.class);

	public HelpMode(BaseGame game)
	{
		super(game);
	}

}