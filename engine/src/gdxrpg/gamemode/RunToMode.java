package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.position.TargetPosition;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.systems.PlayerSystem;

import com.badlogic.ashley.core.Entity;

public class RunToMode extends RangedSelectionMode{

	public RunToMode(BaseGame game)
	{
		super(game, new EntityFinder()
				.includeComponent(MapGround.class)
				.includeComponent(Position.class)
				.visible());
	}

	@Override
	public void onSelected(Entity targetEntity)
	{
		Position targetPosition = CM.get(targetEntity, Position.class);
		Entity player = game.get(PlayerSystem.class).get();
		player.add(new TargetPosition(targetPosition.x, targetPosition.y));
//		player.add(new AIComponent(AIStrategy.BEE_LINE));
//		game.setOnAutoPilot(true);
//		game.setProcessing(true);
	}

}
