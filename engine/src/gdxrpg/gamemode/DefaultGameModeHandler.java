package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultGameModeHandler extends BaseModeHandler {

	private static final Logger log = LoggerFactory.getLogger(DefaultGameModeHandler.class);

	private BaseGame game;

	public DefaultGameModeHandler(BaseGame game)
	{
		super(game);
		this.game = game;
	}

//	@Override
//	public void onExit()
//	{
//		game.setProcessing(true);
//	}
//
//	@Override
//	public void onEnter()
//	{
//		game.setProcessing(true);
//	}

//	@Override
//	public boolean handleInputCommand(CommandInvocation invoc)
//	{
//		boolean ret = true;
//		invoc.invoke(game);
////		switch (invoc.command) {
////			case EXAMINE:
////				game.addMessage(EntitySerializer.dump(invoc.arg(0, Entity.class)));
////				break;
////			case START_RANGED_EXAMINE:
////				game.enterMode(GameMode.RANGED_EXAMINE_MODE);
////				break;
////			case START_RUN_TO:
////				game.enterMode(GameMode.RUN_TO_MODE);
////				break;
////			case START_RANGED_ATTACK:
////				game.enterMode(GameMode.RANGED_ATTACK_MODE);
////				break;
////			case SAVE_CONFIG:
////				Conf.saveToFile(invoc.arg(0, String.class));
////				break;
////			case PAUSE:
////				game.setProcessing(! game.isProcessing());
////				break;
////			case SET_CONFIG_OPTION:
////				Prop k = invoc.arg(0, Conf.Prop.class);
////				String value = invoc.arg(1, String.class);
////				Conf.set(k, value);
////				break;
//	}
}
