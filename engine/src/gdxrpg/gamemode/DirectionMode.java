package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DirectionMode extends BaseModeHandler {

	private static final Logger log = LoggerFactory.getLogger(DirectionMode.class);

	public DirectionMode(BaseGame game)
	{
		super(game);
	}
}