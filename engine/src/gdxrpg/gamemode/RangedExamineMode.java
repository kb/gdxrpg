package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public class RangedExamineMode extends RangedSelectionMode {
	
	private static final Logger log = LoggerFactory.getLogger(RangedExamineMode.class);
	
	public RangedExamineMode(BaseGame game)
	{
		super(game, new EntityFinder()
				.includeComponent(Item.class)
				.includeComponent(Position.class)
				.visible());
	}

	@Override
	public void onSelected(Entity targetEntity)
	{
		log.debug("YAY");
//		game.getInputHandler().handleInputCommand(Enumommand.EXAMINE, targetEntity);
	}

}
