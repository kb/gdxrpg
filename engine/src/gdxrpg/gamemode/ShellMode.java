package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;
import gdxrpg.ecs.systems.ShellSystem;

import org.bitbucket.kb.keypress.KeyPress;

public class ShellMode extends BaseModeHandler {

	public ShellMode(BaseGame game)
	{
		super(game);
	}

	@Override
	public boolean handleKey(KeyPress key)
	{
		if (key.isCharacterOnly()) {
			game.get(ShellSystem.class).insert(key.getCharacterNormalized());
			game.get(ShellSystem.class).updateAutoComplete();
		}
		return true;
	}

}