package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;
import gdxrpg.ecs.systems.ShellSystem;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InventoryMode extends BaseModeHandler {
	
	private static final Logger log = LoggerFactory.getLogger(InventoryMode.class);

	public InventoryMode(BaseGame game)
	{
		super(game);
	}

	@Override
	public boolean handleKey(KeyPress key)
	{
		log.debug("KEY {}", key);
		if (key.isCharacterOnly())
		{
			game.get(ShellSystem.class).insert(key.getCharacterNormalized());
		}
		else
		{
			switch (key.toVimString().toLowerCase())
			{
				case ("<bs>"):
					game.get(ShellSystem.class).backspace();
					break;
				case ("<home>"):
				case ("<c-a>"):
					game.get(ShellSystem.class).pos1();
					break;
				case ("<end>"):
				case ("<c-e>"):
					game.get(ShellSystem.class).end();
					break;
				case ("<right>"):
				case ("<c-f>"):
					game.get(ShellSystem.class).right();
					break;
				case ("<left>"):
				case ("<c-b>"):
					game.get(ShellSystem.class).left();
					break;
				case ("<del>"):
				case ("<c-d>"):
					game.get(ShellSystem.class).delete();
					break;
				case ("<c-k>"):
					game.get(ShellSystem.class).deleteToEOL();
					break;
				case ("<insert>"):
					game.get(ShellSystem.class).pasteFromClipboard();
					break;
				case ("<cr>"):
				case ("<c-j>"):
					game.get(ShellSystem.class).enter();
					break;
				case ("<up>"):
				case ("<c-p>"):
					game.get(ShellSystem.class).historyPrev();
					break;
				case ("<down>"):
				case ("<c-n>"):
					game.get(ShellSystem.class).historyNext();
					break;
				case ("<esc>"):
					game.exitMode();
			}
		}
		game.get(ShellSystem.class).updateAutoComplete();
		return false;
	}

}