package gdxrpg.gamemode;

import gdxrpg.BaseGame;
import gdxrpg.BaseModeHandler;
import gdxrpg.EntityFinder;
import gdxrpg.command.CommandInvocation;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.systems.SelectionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public abstract class RangedSelectionMode extends BaseModeHandler {
	
	private static final Logger log = LoggerFactory.getLogger(RangedSelectionMode.class);

	protected final BaseGame game;
	private final EntityFinder entityFinder;

	public RangedSelectionMode(BaseGame game, EntityFinder entityFinder)
	{
		super(game);
		this.game = game;
		this.entityFinder = entityFinder;
	}

	public abstract void onSelected(Entity targetEntity);

	@Override
	public void onExit()
	{
		this.game.get(SelectionManager.class).reset();
//		game.setProcessing(true);
	}

	@Override
	public void onEnter()
	{
		game.get(SelectionManager.class).reset(this.entityFinder);
		game.get(SelectionManager.class).selectClosestEntity(game.get(PlayerSystem.class).get());
//		game.setProcessing(false);

	}
	
	@Override
	public boolean handleInputCommand(CommandInvocation invoc)
	{
		boolean ret = true;
//		switch(invoc.command) {
//
//			case SELECT:
//				game.get(SelectionManager.class).selectNextEntity(invoc.arg(0, DirectionHorizontal.class), invoc.arg(1, Integer.class));
//				break;
//
//			case CANCEL:
//				game.get(SelectionManager.class).reset();
//				game.exitMode();
//				break;
//
//			case CONFIRM:
//				Entity selectedEntity = game.get(SelectionManager.class).getSelectedEntity();
//				game.get(SelectionManager.class).reset();
//				game.exitMode();
//				onSelected(selectedEntity);
//				break;
//
//			default:
//				ret = false;
//		}
		return ret;
		
	}

}