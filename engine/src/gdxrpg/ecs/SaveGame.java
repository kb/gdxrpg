package gdxrpg.ecs;

import gdxrpg.ecs.constants.GameMode;

import java.util.AbstractList;
import java.util.List;
import java.util.Map;

import com.badlogic.ashley.core.Component;

public class SaveGame {

	public boolean processing;
	public AbstractList<GameMode> modeStack;
	public List<List<Component>> activeEntities;
	public List<String> messages;
	public Map<String, List<List<Component>>> entityCache;

	public SaveGame(
			boolean processing,
			AbstractList<GameMode> modeStack,
			List<List<Component>> activeEntities,
			Map<String,List<List<Component>>> entityCache,
			List<String> messages)
	{
		this.processing = processing;
		this.modeStack = modeStack;
		this.activeEntities = activeEntities;
		this.entityCache = entityCache;
		this.messages = messages;
	}

}
