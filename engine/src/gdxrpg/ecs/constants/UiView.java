package gdxrpg.ecs.constants;

import java.util.Arrays;
import java.util.List;
import static gdxrpg.ecs.constants.UiComponent.*;

public enum UiView {
	DEFAULT_VIEW(Arrays.asList(MAP, HUD, LOG, PROMPT)),
	;
	public final List<UiComponent> uiComponents;

	private UiView(List<UiComponent>uiComponents)
	{
		this.uiComponents = uiComponents;

	}

}
