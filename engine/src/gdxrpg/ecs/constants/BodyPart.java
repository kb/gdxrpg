package gdxrpg.ecs.constants;


public enum BodyPart {
	BODY(null),
	HEAD(BODY),
	EYES(HEAD),
	TORSO(BODY),
	BREASTS(TORSO),
	ARMS(TORSO),
	HANDS(ARMS),
	LEGS(TORSO),
	FEET(LEGS),
	;
	private BodyPart parentPart;
	
	private BodyPart(BodyPart parentPart)
	{
		this.parentPart = parentPart;
	}
}
