package gdxrpg.ecs.constants;

public enum DirectionVertical {
	UP(+ 1),
	DOWN(- 1),
	;
	
	private final int dz;

	private DirectionVertical(int dz)
	{
		this.dz = dz;
	}
	

}
