package gdxrpg.ecs.constants;

public enum SetOp {
	all,
	one,
	exclude
}