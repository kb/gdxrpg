package gdxrpg.ecs.constants;

public enum Sounds {
	
	DEATH("Explosion.wav"),
	HIT("Hit_Hurt.wav"), 
	PORTAL("Jump6.wav"), 
	PICKUP("Pickup_Coin2.wav"),
	;
	
	public final String soundPath;

	private Sounds(String soundPath) {
		this.soundPath = soundPath;
	}

}
