package gdxrpg.ecs.constants;

public enum CollisionType {
	OUT_OF_BOUNDS,
	WALK_INTO_MOVING_ENTITY,
	WALK_INTO_NONPASSABLE,
	ENTER_PORTAL
}
