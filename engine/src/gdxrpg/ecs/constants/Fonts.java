package gdxrpg.ecs.constants;


public enum Fonts {
	
	UNIFONT("fonts/unifont.ttf", 22),
	M1M("fonts/m1m.ttf", 18),
	UBUNTU("fonts/ubuntu-mono.ttf", 18),
	UBUNTU_BOLD("fonts/ubuntu-mono-bold.ttf", 22),
	STANDARD(UNIFONT),
	SPEECH(M1M),
	;
	
	public final String fontPath;
	public final int fontSize;
//	public final int width;
//	public final int height;

	private Fonts(Fonts copy)
	{
		this.fontPath = copy.fontPath;
		this.fontSize = copy.fontSize;
	}
	private Fonts(String fontPath, int fontSize)
	{
		this.fontPath = fontPath;
		this.fontSize = fontSize;
	}

}
