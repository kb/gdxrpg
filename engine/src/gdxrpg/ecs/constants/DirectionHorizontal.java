package gdxrpg.ecs.constants;

import java.util.Random;


public enum DirectionHorizontal {
	HERE(0, 0),
	N(0, -1),
	NW(-1, -1),
	W(-1, 0),
	SW(-1, +1),
	S(0, +1),
	SE(+1, +1),
	E(+1, 0),
	NE(+1, -1)
	;
	
	private final static int SIZE = values().length;
	private final static Random RANDOM = new Random();
	public static DirectionHorizontal parseString(String str)
	{
		if (str == null)
			return null;
		str = str.toUpperCase().substring(0, Math.min(2, str.length()));
		if (str.length() == 2 && (str.endsWith("A") || str.endsWith("O"))) {
			str = str.substring(0,1);
		}
		try {
			return DirectionHorizontal.valueOf(str);
		} catch (RuntimeException e) {
			return null;
		}

	}

	public int dx;
	public int dy;
	DirectionHorizontal(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}
	public static DirectionHorizontal randomDirection()
	{
		DirectionHorizontal ret = values()[RANDOM.nextInt(SIZE)];
		while (ret == HERE)
			ret = values()[RANDOM.nextInt(SIZE)];
		return ret;
	}
}