package gdxrpg.ecs.constants;

import gdxrpg.BaseGame;
import gdxrpg.GameModeHandler;
import gdxrpg.gamemode.DefaultGameModeHandler;
import gdxrpg.gamemode.DialogMode;
import gdxrpg.gamemode.DirectionMode;
import gdxrpg.gamemode.HelpMode;
import gdxrpg.gamemode.InventoryMode;
import gdxrpg.gamemode.RangedAttackMode;
import gdxrpg.gamemode.RangedExamineMode;
import gdxrpg.gamemode.RunToMode;
import gdxrpg.gamemode.ShellMode;

import java.lang.reflect.InvocationTargetException;

public enum GameMode {
	DEFAULT_MODE(DefaultGameModeHandler.class),
	RANGED_ATTACK_MODE(RangedAttackMode.class),
	RANGED_EXAMINE_MODE(RangedExamineMode.class),
	RUN_TO_MODE(RunToMode.class),
	SHELL_MODE(ShellMode.class),
	DIALOG_MODE(DialogMode.class),
	DIRECTION_MODE(DirectionMode.class),
	HELP_MODE(HelpMode.class),
	INVENTORY_MODE(InventoryMode.class),
	;
	
	public final Class<? extends GameModeHandler> implementationClass;

	private GameMode(Class<? extends GameModeHandler> clazz)
	{
		this.implementationClass = clazz;
	}
	
	public GameModeHandler createHandler(BaseGame game) {
		try {
			return this.implementationClass.getDeclaredConstructor(BaseGame.class).newInstance(game);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}
}