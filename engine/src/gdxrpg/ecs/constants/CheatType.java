package gdxrpg.ecs.constants;

public enum CheatType {
	HEALTH_9999,
	KILLALL,
	SEEALL
}
