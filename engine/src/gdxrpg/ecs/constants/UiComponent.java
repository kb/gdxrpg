package gdxrpg.ecs.constants;

public enum UiComponent {
	MAP,
	HUD,
	PROMPT,
	LOG,
	HELP,
	INVENTORY
}
