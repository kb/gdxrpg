package gdxrpg.ecs;

import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.BelongsToLevel;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.Collision;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.ExperienceLevel;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.EyeSight;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.LabelComponent;
import gdxrpg.ecs.components.LookingDirection;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.Pickup;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.RpgAttributes;
import gdxrpg.ecs.components.Speed;
import gdxrpg.ecs.components.UndirectedLightSource;
import gdxrpg.ecs.components.Volume;
import gdxrpg.ecs.components.Weight;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.position.TargetPosition;
import gdxrpg.ecs.components.types.Item;
import gdxrpg.ecs.components.types.Level;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.components.types.Portal;
import gdxrpg.ecs.components.types.Viewport;
import gdxrpg.ecs.constants.CollisionType;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.constants.MyColor;
import gdxrpg.ecs.constants.Units;
import gdxrpg.utils.MathUtils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import squidpony.squidgrid.mapping.ClassicRogueMapGenerator;
import squidpony.squidgrid.mapping.Terrain;

import com.badlogic.ashley.core.Entity;

public abstract class EntityFactory {

	private final static Logger log = LoggerFactory.getLogger(EntityFactory.class);

	private EntityFactory()
	{
		throw new AssertionError();
	}

	public static Entity createTile(String levelName, String ch, int x, int y)
	{
		return createTile(levelName, ch, x, y, false, null, 0);
	}

	public static Entity createCollision(CollisionType type, Entity source, Entity target, Position collPos)
	{
		Entity collision = new Entity();
		collision.add(new Collision(source, type, target, collPos));
		return collision;
	}

	public static Entity createCollision(CollisionType type, Entity source, Position pos)
	{
		Entity collision = new Entity();
		collision.add(new Collision(source, type, pos));
		return collision;
	}

	public static Entity createTile(String levelName, String ch, int x, int y, boolean passable, String fgColor, int alpha)
	{
		Entity entity = new Entity();
		entity.add(new MapGround());
		Renderable rend = new Renderable(ch);
		if (null == fgColor)
			fgColor = MyColor.DARK_GREEN;
		rend.fgColor = fgColor;
		rend.visibility -= alpha;
		rend.bgColor = MyColor.BLACK;
		Passable pass = new Passable(passable);
		AbstractPosition pos = new Position(x, y);
		entity.add(pass);
		entity.add(rend);
		entity.add(pos);
		return entity;
	}

	public static List<Entity> createMapEmpty(String levelName, int width, int height, String ch)
	{
		log.debug("Char: {}", ch);
		List<Entity> ret = new ArrayList<>();

		List<Entity> bottomRow = new ArrayList<>();
		ret.add(createTile(levelName, "0", 0, 0));
		bottomRow.add(createTile(levelName, "0", 0, height - 1));
		for (int x = 1; x < width - 1; x++)
		{
			ret.add(createTile(levelName, String.valueOf(x % 10), x, 0));
			bottomRow.add(createTile(levelName, String.valueOf(x % 10), x, height - 1));
		}
		ret.add(createTile(levelName, "0", width - 1, 0));
		bottomRow.add(createTile(levelName, "0", width - 1, height - 1));

		for (int y = 1; y < height - 1; y++)
		{
			ret.add(createTile(levelName, String.valueOf(y % 10), 0, y));
			for (int x = 1; x < width - 1; x++)
			{
				ret.add(createTile(levelName, ch, x, y, true, null, 50));
			}
			ret.add(createTile(levelName, String.valueOf(y % 10), width - 1, y));
		}

		ret.addAll(bottomRow);

		return ret;
	}

	public static List<Entity> createMapRogueGen(String levelName, int width, int height)
	{
		ClassicRogueMapGenerator gen = new ClassicRogueMapGenerator(
				width / 3, height / 2,
				width, height,
				8, 8,
				5, 4);
		Terrain[][] map = gen.create();
		List<Entity> ret = new ArrayList<>();
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				Terrain terrain = map[x][y];
				Entity createTile = fromTerrain(levelName, x, y, terrain);
				ret.add(createTile);
			}
		}
		return ret;
	}

	private static Entity fromTerrain(String levelName, int x, int y, Terrain terrain)
	{
		String ch = String.valueOf(terrain.symbol());
		switch (ch) {
			case "#":
				return createTile(levelName, ch, x, y, false, "aaaa00", 50);
			default:
				return createTile(levelName, ch, x, y, true, null, 50);
		}
	}

	public static Entity createPlayer(int x, int y)
	{
		Entity thePlayer = new Entity();
		thePlayer.add(new Player());
		thePlayer.add(new Busy());
		RpgAttributes rpgAttributes = new RpgAttributes();
		rpgAttributes.str = 10;
		thePlayer.add(rpgAttributes);
		thePlayer.add(new ExperienceLevel(1));
		thePlayer.add(new Animatable());
		thePlayer.add(new EyeSight(100));
		thePlayer.add(new UndirectedLightSource(20));
		thePlayer.add(new Speed(200));
		thePlayer.add(new Position(x, y));
		thePlayer.add(new Health(100, 100));
		thePlayer.add(new Movement(0, 0));
		thePlayer.add(new Passable(false));
		thePlayer.add(new LookingDirection(DirectionHorizontal.N));
		thePlayer.add(new Description("Me"));
		thePlayer.add(new Renderable("@", MyColor.MAGENTA));
		thePlayer.add(new ItemContainer(30 * Units.KILOGRAM, 20 * Units.LITER));
		return thePlayer;
	}

	public static Entity createNPC(int x, int y, String name, boolean passable)
	{
		Entity npc = new Entity();
		npc.add(new NPC());
		npc.add(new Position(x, y));
		npc.add(new Health(MathUtils.randomInt(100), 100));
		npc.add(new Movement(0, 0));
		npc.add(new Passable(passable));
		npc.add(new Description(name));
		// npc.add(new AIComponent(AIComponent.Strategy.FLEE_PLAYER));
		npc.add(new Renderable("X", MyColor.BLUE));
		return npc;
	}

	public static Entity item(int x, int y, String ch, String color, boolean passable)
	{
		Entity item = new Entity();
		item.add(new Item());
		item.add(new Renderable(ch, color));
		item.add(new Passable(passable));
		item.add(new Position(x, y));
		item.add(new Volume(1 * Units.LITER));
		item.add(new Weight(1 * Units.KILOGRAM));
		item.add(new Position(x, y));
		item.add(new Description("A thing"));
		item.add(new Pickup(true));
		return item;
	}

//	public static Entity createMessage(String string, Entity player)
//	{
//		Entity msg = new Entity();
//		msg.add(new Utterance(player, string, game.getConfig().targetFPS()));
//		return msg;
//	}

	public static Entity portal(int fromX, int fromY, int toX, int toY)
	{
		Entity portal = new Entity();
		portal.add(new Portal());
		portal.add(new Position(fromX, fromY));
		portal.add(new TargetPosition(toX, toY));
		portal.add(new Renderable("*", "ff0000"));
		return portal;
	}

	public static Entity level(String string, int width, int height)
	{
		Entity level = new Entity();
		level.add(new Level());
		level.add(new LabelComponent(string));
		level.add(new Extent(width, height));
		return level;
	}

	public static Entity createViewPort(String belongsTo)
	{
		Entity viewport = new Entity();
		viewport.add(new BelongsToLevel(belongsTo));
		viewport.add(new Viewport());
		viewport.add(new Extent(0, 0));
		viewport.add(new Offset(0, 0));
		return viewport;
	}

//	public static Entity fromSimpleJSON(String json)
//	{
//		return EntitySerializer.fromSimpleJSON(json);
//	}

	// public static Entity room(String roomLabel, int x, int y, int width, int height, )

}
