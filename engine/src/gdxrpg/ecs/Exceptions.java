package gdxrpg.ecs;

import java.util.List;

import javax.management.InstanceNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public abstract class Exceptions {

	private static final Logger log = LoggerFactory.getLogger(Exceptions.class);

	public static class MissingComponentsException extends NoSuchFieldException {

		private MissingComponentsException()
		{
			super();
		}

		private MissingComponentsException(String s)
		{
			super(s);
		}

	}

	public static class MissingEntityException extends InstanceNotFoundException {

		public MissingEntityException(String msg)
		{
			super(msg);
		}
	}

	public static class TooManyEntitiesException extends Exception {

		public TooManyEntitiesException(String msg)
		{
			super(msg);
		}

	}

	public static MissingComponentsException missingComponents(Entity entity, List<String> missing)
	{
		String msg = String.format("entity#%s missing components: %s\n%s", entity.getId(), missing, EntitySerializer.dump(entity));
		log.error(msg);
		return new MissingComponentsException(msg);
	}

	public static MissingEntityException missingEntity(Class<? extends Component> type)
	{
		String msg = String.format("No entity with component of type %s", type);
		log.error(msg);
		return new MissingEntityException(msg);
	}
	public static TooManyEntitiesException tooManyEntities(Class<? extends Component> type)
	{
		String msg = String.format("Too many entities match component type %s", type);
		log.error(msg);
		return new TooManyEntitiesException(msg);
	}

}
