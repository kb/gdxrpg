package gdxrpg.ecs.scripting;

import gdxrpg.BaseGame;
import gdxrpg.command.Command;

import com.badlogic.ashley.core.Entity;

public interface CommandScript extends IScript {
	
	Boolean onCommand(BaseGame game, Entity self, Command command, Object...args);

}
