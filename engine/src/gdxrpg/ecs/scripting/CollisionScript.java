package gdxrpg.ecs.scripting;

import gdxrpg.BaseGame;

import com.badlogic.ashley.core.Entity;

public interface CollisionScript extends IScript {
	
	Boolean onCollision(BaseGame game, Entity self, Entity otherEntity);

}
