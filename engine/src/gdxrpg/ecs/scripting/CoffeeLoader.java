package gdxrpg.ecs.scripting;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntityFactory;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.BelongsToLevel;
import gdxrpg.ecs.components.types.TypeComponent;
import gdxrpg.ecs.systems.SingletonEntitySystem;
import gdxrpg.utils.StaticUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.systems.IteratingSystem;

public class CoffeeLoader {

	private static final int SCRIPT_LINE_CONTEXT = 30;
	private static final String SCRIPTX_ENGINE_NAME = "nashorn";
	private static final Logger log = LoggerFactory.getLogger(CoffeeLoader.class);
	private static final Map<String, String> cache = new HashMap<>();
	// private static final String COFFEE_JAVA_TYPES;
	private static final String COFFEE_API;
	private static ScriptEngineManager manager = new ScriptEngineManager();

	static
	{
		try
		{
			String coffeeApiCode = compileCoffeeScriptFile("/api.coffee");
			String types = getJavaTypesAsCoffeeScript();
			COFFEE_API = types + coffeeApiCode;
		} catch (IOException | InterruptedException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static void lineDumpScript(String script, int lineNumber)
	{
		String[] lines = script.split("\n");
		StringBuilder sb = new StringBuilder();
		int min = Math.max(0, lineNumber - SCRIPT_LINE_CONTEXT);
		int max = Math.min(lineNumber > 0 ? lineNumber + SCRIPT_LINE_CONTEXT : lines.length, lines.length);
		for (int i = min; i < max; i++)
		{
			sb.append(i + 1);
			if (lineNumber > 0 && i == lineNumber - 1)
			{
				sb.append(" **");
			}
			sb.append("\t");
			sb.append(lines[i]);
			sb.append("\n");
		}
		System.out.println(sb.toString());
	}

	@SuppressWarnings("unchecked")
	public static void evalLevel(final String levelName, BaseGame game)
	{
		Path scriptName = game.getGameDirectory().resolve("levels/" + levelName + ".coffee");
		ScriptEngine scriptEngine = evalCoffeeScript(scriptName, game, levelName);

		/*
		 * Entities
		 */
		Object levelEntities = evalJavascript(scriptEngine, "levelBuilder.getEntities()", game, levelName);
		for (Entity entity : (Iterable<Entity>) levelEntities)
		{
			entity.add(new BelongsToLevel(levelName));
			game.addEntity(entity);
		}

		/*
		 * Triggers
		 */

	}

	private static ScriptEngine evalCoffeeScript(final Path scriptPath, BaseGame game, String logName)
	{
		try
		{
			log.debug("Compile javascript->coffee: {}", scriptPath);
			return evalJavascript(game, compileCoffeeScriptFile(scriptPath.toString()), logName);
		} catch (IOException | InterruptedException e)
		{
			e.printStackTrace();
			System.exit(1);
			throw new RuntimeException(e);
		}
	}

	public static ScriptEngine evalCoffeeScript(String ruleCode, BaseGame game, String logName)
	{
		log.debug("Compile javascript->coffee: {}", logName);
		try
		{
			return evalJavascript(game, coffee2javascript(IOUtils.toInputStream(ruleCode)), logName);
		} catch (IOException | InterruptedException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static ScriptEngine evalJavascript(BaseGame game, String compiledScript, String logName)
	{
		log.trace("Create new engine instance");
		ScriptEngine scriptEngine = manager.getEngineByName(SCRIPTX_ENGINE_NAME);
		evalJavascript(scriptEngine, COFFEE_API + compiledScript, game, logName);
		return scriptEngine;
	}

	private static Object evalJavascript(ScriptEngine scriptEngine, String js, BaseGame game, final String logName)
	{
		log.trace("Add bindings");
		Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
		/*
		 * Instances
		 */
		bindings.put("game", game);
		bindings.put("log", LoggerFactory.getLogger("javascript:" + logName));
		log.debug("Starting eval '{}'", logName);
		try
		{
			return scriptEngine.eval(js, bindings);
		} catch (ScriptException e)
		{
			int lineNumber = ((ScriptException) e).getLineNumber();
			lineDumpScript(COFFEE_API + js, lineNumber);
			throw new RuntimeException(e);
		}
	}

	private static String getJavaTypesAsCoffeeScript()
	{
		/*
		 * Types
		 */
		Set<Class<?>> apiTypesToInject = new TreeSet<>(new Comparator<Class<?>>() {
			@Override
			public int compare(Class<?> o1, Class<?> o2)
			{
				// log.debug("{} <=> {}", o1, o2);
				return o1.getCanonicalName().compareTo(o2.getCanonicalName());
			}
		});
		// Add components
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.components").getSubTypesOf(Component.class));
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.components.types").getSubTypesOf(TypeComponent.class));
		// Add event-related
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.event").getSubTypesOf(Enum.class));
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.event", new SubTypesScanner(false)).getSubTypesOf(Object.class));
		// Add systems
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.systems").getSubTypesOf(SingletonEntitySystem.class));
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.systems").getSubTypesOf(IteratingSystem.class));
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.systems").getSubTypesOf(EntitySystem.class));
		// Add constants
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.constants").getSubTypesOf(Enum.class));
		apiTypesToInject.addAll(new Reflections("gdxrpg.ecs.constants", new SubTypesScanner(false)).getSubTypesOf(Object.class));
		// Add utils
		apiTypesToInject.addAll(new Reflections("gdxrpg.utils").getSubTypesOf(StaticUtils.class));

		// Static utility classes
		apiTypesToInject.add(CM.class);
		// apiTypesToInject.add(EntityBuilder.class);
		apiTypesToInject.add(EntityFactory.class);
		apiTypesToInject.add(EntitySerializer.class);

		// ArrayList, hashMap etc.
		apiTypesToInject.add(HashSet.class);
		apiTypesToInject.add(HashMap.class);
		apiTypesToInject.add(ArrayList.class);

		// for (Class<?> type : apiTypesToInject)
		// {
		// globalBindings.put(type.getSimpleName(), type);
		// }
		StringBuilder sb = new StringBuilder();
		for (Class<?> type : apiTypesToInject)
		{
			sb.append(String.format("%s = Java.type(\"%s\")\n", type.getSimpleName(), type.getCanonicalName()));
		}
		sb.append("api = {types:{");
		for (Class<?> type : apiTypesToInject)
		{
			sb.append(String.format("%s: Java.type(\"%s\"),", type.getSimpleName(), type.getCanonicalName()));
		}
		sb.append("}}\n");
		return sb.toString();
	}

	private static String coffee2javascript(InputStream is) throws IOException, InterruptedException
	{
		List<String> command = new ArrayList<>();
		command.add("coffee");
		command.add("-c");
		command.add("-p");
		command.add("-b");
		command.add("-s");
		ProcessBuilder builder = new ProcessBuilder(command);
		Process p = builder.start();
		try (OutputStream os = p.getOutputStream())
		{
			IOUtils.copy(is, os);
			os.close();
			p.waitFor();
			String string = IOUtils.toString(p.getInputStream());
			return string;
		} catch (IOException e)
		{
			throw e;
		}
	}

	public static String compileCoffeeScriptFile(String path) throws IOException, InterruptedException
	{
		log.debug("Loading coffeescript {}", path);
		if (cache.containsKey(path))
		{
			log.debug("Loaded from cache!");
			return cache.get(path);
		}
		else
		{
			InputStream is;
			if (Files.exists(Paths.get(path)))
			{
				log.debug("Load from disk {}", path);
				is = Files.newInputStream(Paths.get(path));
			}
			else
			{
				log.debug("Load from classpath {}", path);
				is = CoffeeLoader.class.getResourceAsStream(path);
			}
			if (is == null)
			{
				throw new FileNotFoundException(path);
			}
			log.debug("Loaded coffeescript, compiling to JavaScript.");
			String js = coffee2javascript(is);
			log.debug("Compiled to JavaScript.");
			cache.put(path, js);
			return js;
		}
	}

}
