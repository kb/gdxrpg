package gdxrpg.ecs.animation;


public enum AnimationType {
	
	SINGLE_TILE_BLINK,
	CIRCLE_AROUND_ENTITY,
	LINE_TO_POSITION,
	RISE_AND_FADE_TEXT,
	SPEECH_BUBBLE,
//	;
//	public final Class<? extends Animator> animatorClass;
//
//	private Animations(Class<? extends Animator> animatorClass)
//	{
//		this.animatorClass = animatorClass;
//	}

}
