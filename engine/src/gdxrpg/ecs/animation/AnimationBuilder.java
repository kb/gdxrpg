package gdxrpg.ecs.animation;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.components.Animation;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.constants.MyColor;
import gdxrpg.ecs.texttable.BorderType;
import gdxrpg.utils.MathUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.google.common.base.Preconditions;

public class AnimationBuilder {

	private static final Logger log = LoggerFactory.getLogger(AnimationBuilder.class);

	private static final Map<DirectionHorizontal, Renderable> defaultDirRendMapping = new HashMap<>();
	/*
	 * [color=#ff0000]E[/color][color=#ff2000]n[/color][color=#ff4000]t[/color][
	 * color=#ff5f00]e[/color][color=#ff7f00]r[/color] [color=#ffbf00]y[/color][color
	 * =#ffdf00]o[/color][color=#ffff00]u[/color][color=#ccff00]r[/color] [color=
	 * #66ff00]m[/color][color=#33ff00]e[/color][color=#00ff00]s[/color][color =#00f
	 * f40]s[/color][color=#00ff80]a[/color][color=#00ffbf]g[/color][color=# 00ffff]e[/color]
	 * [color=#0080ff]h[/color][color=#0040ff]e[/color][color=#0000f f]r[/color][color
	 * =#2300ff]e[/color][color=#4600ff].[/color][color=#6800ff] .[/color][color=#8b00ff].[/color]
	 */
	private static final List<String> COLORS_RAINBOW = Arrays.asList(
			"ff0000", "ff2000", "ff4000", "ff5f00", "ff7f00", "ffbf00", "ffdf00", "ffff00",
			"ccff00", "66ff00", "33ff00", "00ff00", "00ff40", "00ff80", "00ffbf", "00ffff",
			"0080ff", "0040ff", "0000ff", "2300ff", "4600ff", "6800ff", "8b00ff");

	private static final int ArrayList = 0;
	static
	{
		String fgColor = "ff0000";
		defaultDirRendMapping.put(DirectionHorizontal.N, new Renderable("|", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.NW, new Renderable("\\", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.W, new Renderable("-", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.SW, new Renderable("/", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.S, new Renderable("|", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.SE, new Renderable("\\", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.E, new Renderable("-", fgColor));
		defaultDirRendMapping.put(DirectionHorizontal.NE, new Renderable("/", fgColor));
	}

	private double speed = 1f;
	private double fps = -1;
	private int frameLag = 0;
	private int durationInSeconds = 1;
	private Map<DirectionHorizontal, Renderable> dirRendMapping = defaultDirRendMapping;
	private List<List<Entity>> frameList = new ArrayList<>();
	private boolean looping;
	private String content = "ABCDEFG";
	private int repeat;
	private List<String> colors = COLORS_RAINBOW;
	private Position targetPosition;
	private Position sourcePosition;
	private String borderFg = MyColor.GOLD;
	private String borderBg = MyColor.DARK_BLUE;
	private String textFg = MyColor.WHITE;
	private String textBg = MyColor.DARK_BLUE;
	private int height = 5;
	private AnimationType animationType;
	private Movement movement = Movement.fromDirection(DirectionHorizontal.N);

	private void buildCircleEntity()
	{
		for (int i = 0; i < 8; i++)
		{
			List<Entity> frame = new ArrayList<>();
			int dirIdx = 0;
			for (DirectionHorizontal dir : DirectionHorizontal.values())
			{
				if (dir == DirectionHorizontal.HERE || dirIdx++ != i)
				{
					continue;
				}
				Entity tile = new Entity();
				tile.add(new Offset(dir.dx, dir.dy));
				tile.add(dirRendMapping.get(dir));
				frame.add(tile);
			}
			frameList.add(frame);
		}
		log.debug("{}", frameList);
	}

	private void buildLineToPosition()
	{
		Preconditions.checkNotNull(targetPosition);
		Preconditions.checkNotNull(sourcePosition);
		Offset offset = Offset.fromPositions(sourcePosition, targetPosition);
		List<Position> line = MathUtils.line(0, 0, offset.dx, offset.dy);
		log.debug("source pos {}", sourcePosition);
		log.debug("target pos {}", targetPosition);
		log.debug("total offset {}", offset);
		for (int i = 1; i < line.size(); i++)
		{
			Position point = line.get(i);
			Entity tile = new Entity();
			Offset pointOffset = new Offset(point.roundX(), point.roundY());
			log.debug("point offset {}", pointOffset);
			tile.add(pointOffset);
			tile.add(new Renderable(content.substring(0, 1)));
			frameList.add(Arrays.asList(tile));
		}
	}

	private void buildRiseAndFade()
	{
		// TODO handle movement!
		int width = content.length();
		int xOffset = width / 2;
		int yOffset = (int) movement.dy * (-1);
		for (int y = 0; y < height; y++)
		{
			List<Entity> frame = new ArrayList<>();
			for (int x = 0; x < width; x++)
			{
				Entity tile = new Entity();
				Offset offset = new Offset(x + ((int) movement.dx * (y + xOffset)), (int) movement.dy * (y + yOffset));
//				log.debug("MOVEMENT {}", movement);
//				log.debug("OFFSET {}", offset);
				tile.add(offset);
				tile.add(new Renderable(content.substring(x, x + 1)));
				frame.add(tile);
			}
			frameList.add(frame);
		}
	}

	private void buildSingleTileBlink()
	{
		char[] charArray = content.toCharArray();
		for (int i = 0; i < charArray.length; i++)
		{
			Entity tile = new Entity();
			tile.add(new Offset(DirectionHorizontal.HERE.dx, DirectionHorizontal.HERE.dy));
			tile.add(new Renderable(String.valueOf(charArray[i])));
			frameList.add(Arrays.asList(tile));
		}
	}

	private void buildSpeechBubble()
	{
		String[] contentLines = content.split("\n");
		int maxLength = 0;
		List<Entity> frame = new ArrayList<>();
		int contentHeight = contentLines.length;
		for (int y = 0; y < contentHeight; y++)
		{
			maxLength = Math.max(maxLength, contentLines[y].length() + 2);
		}
		BorderType borderType = BorderType.SINGLE;

		// Top line
		{
			Entity nw = new Entity();
			nw.add(new Offset(0, 0));
			nw.add(new Renderable(borderType.nw, borderFg, borderBg));
			frame.add(nw);
		}
		for (int x = 1; x < maxLength; x++)
		{
			Entity n = new Entity();
			n.add(new Offset(x, 0));
			n.add(new Renderable(borderType.n, borderFg, borderBg));
			frame.add(n);
		}
		{
			Entity ne = new Entity();
			ne.add(new Offset(maxLength, 0));
			ne.add(new Renderable(borderType.ne, borderFg, borderBg));
			frame.add(ne);
		}

		// Content lines

		for (int y = 0; y < contentHeight; y++)
		{
			String line = contentLines[y];
			char[] chars = line.toCharArray();
			{
				Entity w = new Entity();
				w.add(new Offset(0, y + 1));
				w.add(new Renderable(borderType.w, borderFg, borderBg));
				frame.add(w);
			}
			for (int x = 1; x < maxLength; x++)
			{
				Entity here = new Entity();
				here.add(new Offset(x, y + 1));
				if (x - 1 < chars.length)
				{
					here.add(new Renderable(String.valueOf(chars[x - 1]), textFg, textBg));
				}
				else
				{
					here.add(new Renderable(" ", textFg, textBg));
				}
				frame.add(here);
			}
			{
				Entity e = new Entity();
				e.add(new Offset(maxLength, y + 1));
				e.add(new Renderable(borderType.e, borderFg, borderBg));
				frame.add(e);
			}
		}

		// Bottom line
		{
			Entity sw = new Entity();
			sw.add(new Offset(0, contentHeight + 1));
			// sw.add(new Renderable(borderType.sw));
			sw.add(new Renderable(borderType.w, borderFg, borderBg));
			frame.add(sw);
		}
		for (int x = 1; x < maxLength; x++)
		{
			Entity s = new Entity();
			s.add(new Offset(x, contentHeight + 1));
			s.add(new Renderable(borderType.s, borderFg, borderBg));
			frame.add(s);
		}
		{
			Entity se = new Entity();
			se.add(new Offset(maxLength, contentHeight + 1));
			se.add(new Renderable(borderType.se, borderFg, borderBg));
			frame.add(se);
		}

		// TODO Adjust to viewport
		for (Entity tile : frame)
		{
			Offset offset = CM.get(tile, Offset.class);
			tile.add(new Offset(offset.dx + 1, offset.dy - contentHeight - 3));
		}

		// Connector
		{
			{
				Entity connector = new Entity();
				connector.add(new Offset(2, -2));
				connector.add(new Renderable(" ", textFg, textBg));
				frame.add(connector);
			}
			{
				Entity connector = new Entity();
				connector.add(new Offset(3, -2));
				connector.add(new Renderable("/", borderFg, borderBg));
				frame.add(connector);
			}
			{
				Entity connector = new Entity();
				connector.add(new Offset(1, -1));
				connector.add(new Renderable("\u2502", borderFg, borderBg));
				frame.add(connector);
			}
			{
				Entity connector = new Entity();
				connector.add(new Offset(2, -1));
				connector.add(new Renderable("\u2571", borderFg, borderBg));
				frame.add(connector);
			}
		}
		frameList.add(frame);
	}

	private Entity cloneTile(Entity tile)
	{
		Entity tileCopy = new Entity();
		tileCopy.add(Renderable.fromRenderable(tile.getComponent(Renderable.class)));
		tileCopy.add(Offset.fromOffset(tile.getComponent(Offset.class)));
		return tileCopy;
	}

	private void colorize()
	{
		if (animationType == AnimationType.SPEECH_BUBBLE)
		{
			return;
		}
		else
		{
			int totalIdx = 0;
			for (List<Entity> frame : frameList)
			{
				for (Entity tile : frame)
				{
					try
					{
						CM.ensure(tile, Arrays.asList(Renderable.class, Offset.class));
					} catch (MissingComponentsException e)
					{
						e.printStackTrace();
						throw new RuntimeException(e);
					}
					tile.getComponent(Renderable.class).fgColor = colors.get(totalIdx % colors.size());
					// log.debug(EntitySerializer.dump(tile));
					totalIdx += 1;
				}
			}
		}
	}

	private void handleFrameLag()
	{
		if (frameLag <= 0)
		{
			return;
		}
		List<List<Entity>> frameListCopy = new ArrayList<>();
		for (int i = 0; i < frameList.size(); i++)
		{
			List<Entity> frameCopy = new ArrayList<>();
			for (Entity tile : frameList.get(i))
			{
				frameCopy.add(cloneTile(tile));
				for (int backIdx = Math.max(0, i - frameLag); backIdx < i; backIdx++)
				{
					for (Entity backTile : frameList.get(backIdx))
					{
						frameCopy.add(cloneTile(backTile));
					}
				}
			}
			frameListCopy.add(frameCopy);
		}
		frameList = frameListCopy;
	}

	private void handleRepeat()
	{
		if (repeat <= 0)
		{
			return;
		}
		List<List<Entity>> frameListCopy = new ArrayList<>();
		for (int i = 0; i <= repeat; i++)
		{
			for (List<Entity> frame : frameList)
			{
				List<Entity> frameCopy = new ArrayList<>();
				for (Entity tile : frame)
				{
					frameCopy.add(cloneTile(tile));
				}
				frameListCopy.add(frameCopy);
			}
		}
		frameList = frameListCopy;
	}

	private void setFPS()
	{
		if (fps <= 0)
		{
			fps = frameList.size();
		}
	}

	public Animation build(AnimationType animationType)
	{
		this.animationType = animationType;
		log.debug("Creating animation of type {}", animationType);
		switch (animationType) {
			case CIRCLE_AROUND_ENTITY:
				buildCircleEntity();
				break;
			case SINGLE_TILE_BLINK:
				buildSingleTileBlink();
				break;
			case LINE_TO_POSITION:
				buildLineToPosition();
				break;
			case RISE_AND_FADE_TEXT:
				buildRiseAndFade();
				break;
			case SPEECH_BUBBLE:
				buildSpeechBubble();
				break;
			default:
				throw new RuntimeException();
		}
		handleFrameLag();
		handleRepeat();
		colorize();
		setFPS();
		log.debug("FPS: {}", fps);
		return new Animation(frameList, looping, fps * speed);

	}

	/*
	 * Public builder methods
	 */

	public AnimationBuilder color(String color)
	{
		this.colors = Arrays.asList(color);
		return this;
	}

	public AnimationBuilder colors(List<String> colors)
	{
		this.colors = colors;
		return this;
	}

	public AnimationBuilder content(String content)
	{
		this.content = content;
		return this;
	}

	public AnimationBuilder durationInSeconds(int durationInSeconds)
	{
		this.durationInSeconds = durationInSeconds;
		return this;
	}

	public AnimationBuilder fps(double fps)
	{
		this.fps = fps;
		return this;
	}

	public AnimationBuilder frameLag(int frameLag)
	{
		this.frameLag = frameLag;
		return this;
	}

	public AnimationBuilder height(int height)
	{
		this.height = height;
		return this;
	}

	public AnimationBuilder looping(boolean looping)
	{
		this.looping = looping;
		return this;
	}

	public AnimationBuilder repeat(int repeat)
	{
		this.repeat = repeat;
		return this;
	}

	public AnimationBuilder sourcePositions(Position pos)
	{
		this.sourcePosition = pos;
		return this;
	}

	public AnimationBuilder speed(double speed)
	{
		this.speed = speed;
		return this;
	}

	public AnimationBuilder targetPositions(Position pos)
	{
		this.targetPosition = pos;
		return this;
	}

	public AnimationBuilder movement(Movement movement)
	{
		this.movement = movement;
		return this;
	}
}