package gdxrpg.ecs.ai.tasks;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.ai.EntityLeafTask;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.MovementSystem;
import gdxrpg.ecs.systems.PlayerSystem;

import java.util.List;

import com.badlogic.ashley.core.Entity;

public class RunToPlayerTask extends EntityLeafTask
{
	
	@Override
	public void run(BaseGame game, Entity object)
	{
		Position curPos = CM.get(object, Position.class);
		Position playPos = game.get(PlayerSystem.class).get(Position.class);
		List<Position> path = game.get(LevelSystem.class).findPath(curPos, playPos);
		if (path.size() == 0) {
			fail();
		} else if (path.size() <= 2) {
			success();
		} else {
			Position targetPos = path.get(1);
			game.get(MovementSystem.class).move(object, new Movement(targetPos.x - curPos.x , targetPos.y - curPos.y));
			running();
		}

	}

}
