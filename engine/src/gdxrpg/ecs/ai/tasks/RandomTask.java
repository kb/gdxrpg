package gdxrpg.ecs.ai.tasks;

import gdxrpg.BaseGame;
import gdxrpg.ecs.ai.EntityLeafTask;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute;

public class RandomTask extends EntityLeafTask
{
	
	@TaskAttribute(required=true)
	public double chance;

	@Override
	public void run(BaseGame game, Entity entity)
	{
		if (chance > Math.random())
			success();
		else
			fail();
	}

}
