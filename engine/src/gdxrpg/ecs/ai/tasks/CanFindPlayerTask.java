package gdxrpg.ecs.ai.tasks;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.ai.EntityLeafTask;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.systems.VisibilitySystem;

import com.badlogic.ashley.core.Entity;

public class CanFindPlayerTask extends EntityLeafTask{

	@Override
	public void run(BaseGame game, Entity entity)
	{
		Position from = CM.get(entity, Position.class);
		Position to = game.get(PlayerSystem.class).get(Position.class);
		if (game.get(VisibilitySystem.class).canSee(entity, to)) 
			success();
		else
			fail();
	}

}
