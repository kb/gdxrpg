package gdxrpg.ecs.ai.tasks;

import gdxrpg.BaseGame;
import gdxrpg.ecs.ai.EntityLeafTask;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.systems.MovementSystem;

import com.badlogic.ashley.core.Entity;

public class WanderTask extends EntityLeafTask {

	@Override
	public void run(BaseGame game, Entity entity)
	{
		 DirectionHorizontal randomDir = DirectionHorizontal.randomDirection();
		 game.get(MovementSystem.class).move(entity, randomDir);
		 success();
	}

}
