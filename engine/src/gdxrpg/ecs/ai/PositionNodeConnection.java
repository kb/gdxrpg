package gdxrpg.ecs.ai;

import com.badlogic.gdx.ai.pfa.DefaultConnection;

public class PositionNodeConnection extends DefaultConnection<PositionNode> {

	private int cost;

	public PositionNodeConnection(PositionNode fromNode, PositionNode toNode, int cost)
	{
		super(fromNode, toNode);
		this.cost = cost;
	}

	@Override
	public float getCost()
	{
		return cost;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(this.getFromNode().position);
		sb.append(" -> ");
		sb.append(this.getToNode().position);
		if (cost != 1)
		{
			sb.append("(");
			sb.append(cost);
			sb.append(")");
		}
		return sb.toString();
	}

}
