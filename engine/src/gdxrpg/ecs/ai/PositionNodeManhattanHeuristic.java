package gdxrpg.ecs.ai;

import gdxrpg.ecs.components.position.AbstractPosition;

import com.badlogic.gdx.ai.pfa.Heuristic;

public class PositionNodeManhattanHeuristic implements Heuristic<PositionNode> {

	@Override
	public float estimate(PositionNode node, PositionNode endNode)
	{
		AbstractPosition nodePos = node.position;
		AbstractPosition endNodePos = endNode.position;
		return (float) (Math.abs(endNodePos.x - nodePos.x) + Math.abs(endNodePos.y - nodePos.y));
	}

}
