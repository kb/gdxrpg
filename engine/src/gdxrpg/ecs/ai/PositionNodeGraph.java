package gdxrpg.ecs.ai;

import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.position.Position;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.DefaultIndexedGraph;
import com.badlogic.gdx.utils.Array;

public class PositionNodeGraph extends DefaultIndexedGraph<PositionNode>
{
	private static final Logger log = LoggerFactory.getLogger(PositionNodeGraph.class);
	private Extent levelExtent;
	
	public PositionNodeGraph(Extent levelExtent)
	{
		super();
		reset(levelExtent);
	}

	public void reset(Extent levelExtent)
	{
		this.levelExtent = levelExtent;
		this.nodes.ensureCapacity(levelExtent.height * levelExtent.width);
		for (int y = 0 ; y < levelExtent.height ; y++) {
			for (int x = 0 ; x < levelExtent.width ; x++) {
				Position pos = new Position(x, y);
				getOrAddNode(pos);
			}
		}
		log.debug("SIZE: {}", this.nodes.size);
	}

	public PositionNode getOrAddNode(Position pos)
	{
		PositionNode ret;
		try
		{
			ret = getNode(pos);
		} catch (IndexOutOfBoundsException e)
		{
			if (log.isTraceEnabled())
				log.trace("Inserting {} @ {}", pos, pos.getIndex(levelExtent.width));
			ret = new PositionNode(pos, levelExtent.width);
			nodes.insert(ret.getIndex(), ret);
		}
		return ret;
	}

	public PositionNode getNode(Position pos)
	{
		return nodes.get(pos.getIndex(levelExtent.width));
	}
	
	public Array<Connection<PositionNode>> getConnections(Position position)
	{
		return nodes.get(position.getIndex(levelExtent.width)).getConnections();
	}

}