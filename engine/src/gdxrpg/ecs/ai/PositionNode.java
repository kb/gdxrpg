package gdxrpg.ecs.ai;

import gdxrpg.ecs.components.position.Position;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;

public class PositionNode implements IndexedNode<PositionNode>
{
	private final int index;
	public final Position position;
	private final Array<Connection<PositionNode>> connections = new Array<>();

	public PositionNode(Position position, int levelWidth)
	{
		this.position = position;
		this.index = position.getIndex(levelWidth);
	}

	@Override
	public int getIndex()
	{
		return this.index;
	}
	
	public void addConnection(PositionNode to, int cost)
	{
		this.connections.add(new PositionNodeConnection(this, to, cost));
	}

	@Override
	public Array<Connection<PositionNode>> getConnections()
	{
		return this.connections;
	}
	
	@Override
	public String toString()
	{
		return "NODE[" + position.toString()  + "]";
	}
}
