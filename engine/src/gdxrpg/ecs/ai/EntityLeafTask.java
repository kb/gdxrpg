package gdxrpg.ecs.ai;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.game.GameComponent;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;

public abstract class EntityLeafTask extends LeafTask<Entity> {

	public EntityLeafTask()
	{
		super();
	}
	
	@Override
	public void run(Entity entity)
	{
		BaseGame game = CM.get(entity, GameComponent.class).game;
		run(game, entity);
		
	}

	abstract public void run(BaseGame game, Entity entity);

	@Override
	protected Task<Entity> copyTo(Task<Entity> task)
	{
		return task;
	}

}