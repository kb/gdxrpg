package gdxrpg.ecs.event;

import gdxrpg.ecs.constants.GameMode;

public interface GameModeChangeListener {
	void onExitMode(GameMode mode, Object... args);
	void onEnterMode(GameMode mode, Object... args);
}