package gdxrpg.ecs.event;

public interface GameSaveListener {
	void onLoad();
	void onSave();
}