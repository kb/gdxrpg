package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.UndirectedLightSource;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.components.types.MapScenery;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.components.types.Room;
import gdxrpg.ecs.event.GameSaveListener;
import gdxrpg.ecs.texttable.TextTable;
import gdxrpg.ecs.texttable.TextTable.TextTableAlign;
import gdxrpg.ecs.texttable.TextTable.TextTableBuilder;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;

public class PlayerSystem extends SingletonEntitySystem<Player> {

	private static final Logger log = LoggerFactory.getLogger(PlayerSystem.class);

	private BaseGame game;

	public PlayerSystem(Integer priority, BaseGame game)
	{
		super(priority, game, Player.class);
		this.game = game;
		game.addGameSaveListener(new GameSaveListener() {

			@Override
			public void onSave()
			{

			}

			@Override
			public void onLoad()
			{
				try
				{
					reset();
				} catch (MissingEntityException | TooManyEntitiesException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		});
	}

	public void possess(Entity npc)
	{
		if (!Family.all(NPC.class, Movement.class, Position.class).get().matches(npc))
		{
			log.error("Can't posess non-player entity: {}", EntitySerializer.dump(npc));
			return;
		}
		Entity player = get();
		npc.add(player.remove(Player.class));
		// npc.add(player.remove(UndirectedLightSource.class));
		npc.add(new UndirectedLightSource(30));
		player.add(npc.remove(NPC.class));
		npc.add(new UniqueLabel("POSSESSED!!!! MWAHA!!!"));
		log.debug("Player was {}", EntitySerializer.dump(player));
		log.debug("NPC was {}", EntitySerializer.dump(npc));
		try
		{
			reset();
		} catch (MissingEntityException | TooManyEntitiesException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public TextTable playerAsTable()
	{
		TextTableBuilder ret = TextTable.builder();
		Entity player = get();
		ret.row();
		ret.cell("Weight carried:");
		ret.cell("" + game.get(ItemSystem.class).getTotalWeight(CM.get(player, ItemContainer.class)));
		for (Component comp : player.getComponents())
		{
			ret.row();
			ret.cell(comp.getClass().getSimpleName());
			ret.cell(comp.toString()).align(TextTableAlign.Right).fgColor("ff0000");
		}
		return ret.build();
	}

	public void surveySurroundings()
	{
		try
		{
			CM.ensure(get(), Arrays.asList(Position.class, Description.class));
		} catch (MissingComponentsException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
//		Position needlePos = get(Position.class);
		EntityFinder finder = new EntityFinder();
		finder.radius(20);
		finder.excludeComponent(MapGround.class);
		finder.excludeComponent(MapScenery.class);
		finder.excludeComponent("player");
		finder.excludeComponent(Room.class);
		finder.includeComponent(Description.class);
//		finder.visible();
		List<Entity> find = finder.find(get(Position.class), game);
		game.addMessage(game.get(LevelSystem.class).entitiesWithPath(get(), find));

	}

	public void doNothing(int i)
	{
//		get(Busy.class).inc(i);
	}

}
