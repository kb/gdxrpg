package gdxrpg.ecs.systems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntityFactory;
import gdxrpg.ecs.components.Attack;
import gdxrpg.ecs.components.Behavior;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.RpgAttributes;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Corpse;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.constants.AttackType;
import gdxrpg.ecs.constants.MyColor;
import gdxrpg.ecs.constants.Sounds;
import gdxrpg.utils.AnimationFactory;

public class AttackSystem extends IteratingSystem {

	private static final Logger log = LoggerFactory.getLogger(AttackSystem.class);

	private BaseGame game;

	public AttackSystem(Integer priority, BaseGame game)
	{
		super(Family.all(Attack.class).get(), priority);
		this.game = game;
	}

	public void meleeAttack(Entity source, Entity target)
	{
		Entity attackEntity = new Entity();
		attackEntity.add(new Attack(AttackType.MELEE, source, target));
		game.addEntity(attackEntity);
	}

	public void rangedAttack(Entity source, Entity target)
	{
		Entity attackEntity = new Entity();
		attackEntity.add(new Attack(AttackType.RANGED, source, target));
		game.addEntity(attackEntity);
	}

	public void killall()
	{
		Entity[] npcs = game.getEntitiesFor(Family.all(NPC.class).get()).toArray(Entity.class);
		// List<Entity> toKill = new ArrayList<>();
		for (int i = 0; i < npcs.length; i++)
		{
			// toKill.add(npcs[i]);
			kill(npcs[i]);
		}
		// for (Entity npc : toKill) {
		// // log.debug("Killing NPC #" + i);
		// kill(npc);
		// }
	}

	private void kill(Entity target)
	{
		// Make it a corpse
//		game.setProcessing(false);
		Health targetHealth = CM.get(target, Health.class);
		Renderable rend = CM.get(target, Renderable.class);
		Passable pass = CM.get(target, Passable.class);
		Position pos = CM.get(target, Position.class);
		game.playSound(Sounds.DEATH);
		// target.add(new Animation(Animations.DEATH_ANIMATION_1,
		// "DDDDDDEEEEEEAAAAAATTTTTTHHHHHH", false));
		targetHealth.dead = true;
		target.remove(Movement.class);
		target.remove(NPC.class);
		target.remove(Behavior.class);
		target.add(new Corpse());
		rend.fgColor = MyColor.GRAY;
		pass.passable = true;
		Entity blood = EntityFactory.item(pos.roundX(), pos.roundY(), ";", MyColor.RED, true);
		blood.add(new Description("blood spatter"));
		game.addEntity(blood);
		if (CM.has(target, Player.class))
		{
			// TODO game over
//			game.setProcessing(false);
		}
		// game.engine.removeEntity(target);
		// game.engine.addEntity(animationEntity);
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		Attack attack = CM.get(entity, Attack.class);
		switch (attack.type) {
			case MELEE:
				handleMeleeAtack(attack.source, attack.target);
				break;
			case RANGED:
				handleRangedAttack(attack.source, attack.target);
				break;
		}
		game.removeEntity(entity);
	}

	private void handleRangedAttack(Entity source, Entity target)
	{
		// TODO Auto-generated method stub
		Health targetHealth = CM.get(target, Health.class);
		RpgAttributes sourceStats = CM.get(source, RpgAttributes.class);
		int hit = sourceStats.str;

		targetHealth.health -= hit;
		game.playSound(Sounds.HIT);
		game.addMessage(String.format("Hit %s for %s damage", CM.get(target, Description.class).name, hit));
		AnimationFactory.createShootingAnimation(source, target);
		AnimationFactory.createBlinkingAnimation(40, target);
	}

	private void handleMeleeAtack(Entity source, Entity target)
	{
		Health targetHealth = CM.get(target, Health.class);
		// int sourceLevel = CM.get(source, ExperienceLevel.class).playLevel;
		RpgAttributes sourceStats = CM.get(source, RpgAttributes.class);

		int hit = sourceStats.str;

		targetHealth.health -= hit;
		game.playSound(Sounds.HIT);
		game.addMessage(String.format("Hit %s for %s damage", CM.get(target, Description.class).name, hit));
		// System.out.println(targetHealth.health);
		AnimationFactory.createBlinkingAnimation(40, target);
		AnimationFactory.createHitAnimation(source, target, hit);
		if (targetHealth.health <= 0)
		{
			kill(target);
			// HACK to test
			// } else if (!endTurn) {
			// attack(target, source, true);
		}
	}

}
