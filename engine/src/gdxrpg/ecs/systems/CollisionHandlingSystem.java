package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Collision;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.position.TargetPosition;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.constants.Sounds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

public class CollisionHandlingSystem extends IteratingSystem {

	private static final Logger log = LoggerFactory.getLogger(CollisionHandlingSystem.class);

	private BaseGame game;

	public CollisionHandlingSystem(Integer priority, BaseGame game)
	{
		super(Family.all(Collision.class).get(), priority);
		this.game = game;
	}

	@Override
	protected void processEntity(Entity collisionEntity, float deltaTime)
	{

		Collision coll = CM.get(collisionEntity, Collision.class);
		if (!game.get(RuleSystem.class).trigger(coll))
		{
			return;
		}
//		Entity player = game.get(PlayerSystem.class).get();
//		if (coll.source == player || coll.target == player)
//			game.setOnAutoPilot(false);
		// log.debug("coll: {}", coll);
		switch (coll.type) {
			case OUT_OF_BOUNDS:
				coll.source.add(Movement.fromDirection(DirectionHorizontal.HERE));
				break;
			case WALK_INTO_NONPASSABLE:
				coll.source.add(Movement.fromDirection(DirectionHorizontal.HERE));
				break;
			case WALK_INTO_MOVING_ENTITY:
				coll.source.add(Movement.fromDirection(DirectionHorizontal.HERE));
				if (CM.has(coll.source, Player.class))
				{
					// TODO handle collisions where the outcome isn't attack but dialogue or similar
//					MessageManager.getInstance().dispatchMessage(MessageType.MELEE_ATTACK.ordinal());
					game.get(AttackSystem.class).meleeAttack(coll.source, coll.target);
				}
				else if (CM.has(coll.source, NPC.class))
				{
					// NPCs only attack the player not each other
					// TODO use AI for this
//					CM.get(coll.source, AIComponent.class).strategy.ai.handleCollision(game, coll.source, coll.target);
				}
				break;
			case ENTER_PORTAL:
				coll.source.add(Movement.fromDirection(DirectionHorizontal.HERE));
				TargetPosition targetPos = CM.get(coll.target, TargetPosition.class);
				game.playSound(Sounds.PORTAL);
				coll.source.add(Position.fromPosition(targetPos));
				break;

		}
		game.removeEntity(collisionEntity);
	}

}
