package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.Pickup;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.Weight;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Item;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.constants.Sounds;
import gdxrpg.ecs.texttable.TextTable;
import gdxrpg.ecs.texttable.TextTable.TextTableAlign;
import gdxrpg.ecs.texttable.TextTable.TextTableBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;

public class ItemSystem extends EntitySystem {

	private static final Logger log = LoggerFactory.getLogger(ItemSystem.class);
	private BaseGame game;

	public ItemSystem(Integer priority, BaseGame game)
	{
		super(priority);
		this.game = game;
	}

	public void pickup(Entity actor, DirectionHorizontal dir)
	{
		AbstractPosition actorPos = CM.get(actor, Position.class);
		Position needlePos = new Position(actorPos.x + dir.dx, actorPos.y + dir.dy);
		for (Entity item : game.getEntitiesFor(Family.all(Item.class, Position.class).get()))
		{
			Position itemPos = CM.get(item, Position.class);
			if (itemPos.samePositionAs(needlePos) && this.canPickup(actor, item))
			{
				putIn(actor, item);
				game.addMessage("Picked up " + CM.get(item, Description.class).name);
			}
		}
	}

	public void drop(Entity actor, Entity item)
	{
		throw new RuntimeException("NOT IMPLEMNTED");
	}

	private boolean canPickup(Entity actor, Entity item)
	{
		boolean ret = false;
		if (CM.has(item, Item.class) && CM.has(actor, Player.class))
		{
			// TODO further checks (weight, volume etc.)
			if (CM.has(item, Pickup.class) && CM.get(item, Pickup.class).pickup)
			{
				ret = true;
			}
		}
		log.debug("Can pick up {} : {}", item, ret);
		return ret;
	}

	public void putIn(Entity itemContainerEntity, Entity item)
	{
		ItemContainer container = CM.get(itemContainerEntity, ItemContainer.class);
		container.contents.add(item);
		game.playSound(Sounds.PICKUP);
		item.remove(Position.class);
	}

	public void takeFrom(Entity itemContainerEntity, Entity item)
	{
		ItemContainer container = CM.get(itemContainerEntity, ItemContainer.class);
		container.contents.remove(item);
		game.playSound(Sounds.PICKUP);
		item.remove(Position.class);
	}

	public TextTable itemAsTable(Entity item)
	{
		try
		{
			CM.ensure(item, Item.class);
		} catch (Exception e)
		{
			log.error("{}", e);
			return TextTable.EMPTY;
		}
		TextTableBuilder ret = TextTable.builder();
		for (Component comp : item.getComponents())
		{
			if (comp.getClass().isAssignableFrom(Description.class)) {
				ret.row();
				ret.cell(((Description)comp).description);
				
			} else {
				ret.row();
				ret.cell(comp.getClass().getSimpleName());
				ret.cell(comp.toString()).align(TextTableAlign.Right).fgColor("ff0000");
			}
		}
		return ret.build();
	}

	public TextTable itemContainerAsTable(Entity itemContainerEntity)
	{
		try
		{
			CM.ensure(itemContainerEntity, ItemContainer.class);
		} catch (MissingComponentsException e)
		{
			return TextTable.EMPTY;
		}
		ItemContainer inv = CM.get(itemContainerEntity, ItemContainer.class);
		TextTableBuilder tb = TextTable.builder();
		for (Entity itemEntity : inv.contents) {
			Description desc = CM.get(itemEntity, Description.class);
			Renderable rend = CM.get(itemEntity, Renderable.class);
			tb.row();
			tb.cell(rend.fgChar + " ").fgColor(rend.fgColor);
			tb.cell(desc.name);
		}
		return tb.build();
	}

	public int getTotalWeight(ItemContainer itemContainer)
	{
		int ret = 0;
		for (Entity itemEntity : itemContainer.contents) {
//			log.debug("{}", EntitySerializer.dump(itemEntity));
			ret += CM.get(itemEntity, Weight.class).weight;
		}
		return ret;
	}

}
