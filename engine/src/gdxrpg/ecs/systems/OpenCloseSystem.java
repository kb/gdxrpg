package gdxrpg.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Openable;

public class OpenCloseSystem extends IteratingSystem {

	private BaseGame game;

	public OpenCloseSystem(Integer priority, BaseGame game)
	{
		super(Family.all(Openable.class).get(), priority);
		this.game = game;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		Openable openable = CM.get(entity, Openable.class);
		openable.setState(entity);
		// Renderable rend = CM.get(entity, Renderable.class);
		// Passable pass = CM.get(entity, Passable.class);
		// if (open.getCurrentState().equals("OPEN"))
		// {
		// rend.fgChar = open.openChar;
		// if (null != pass)
		// pass.passable = true;
		// }
		// else
		// {
		// rend.fgChar = open.closedChar;
		// if (null != pass)
		// pass.passable = false;
		// }
	}

	public void openAll()
	{
		for (Entity entity : game.getEntitiesFor(Family.all(Openable.class).get()))
		{
			open(entity);
		}
	}

	public void open(Entity entity)
	{
		CM.get(entity, Openable.class).setCurrentState("OPEN");
//		game.get(PlayerSystem.class).get(Busy.class).inc(1);
	}

	public void closeAll()
	{
		for (Entity entity : game.getEntitiesFor(Family.all(Openable.class).get()))
		{
			close(entity);
		}

	}

	public void close(Entity entity)
	{
		CM.get(entity, Openable.class).setCurrentState("CLOSED");
//		game.get(PlayerSystem.class).get(Busy.class).inc(1);
	}

}
