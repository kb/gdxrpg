package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.utils.FpsCounter;

import com.badlogic.ashley.core.EntitySystem;

public class FpsCounterSystem extends EntitySystem {

	private final FpsCounter fpsCounter = new FpsCounter();
	private BaseGame game;

	public FpsCounterSystem(Integer priority, BaseGame game)
	{
		super(priority);
		this.game = game;
	}

	@Override
	public void update(float delta)
	{
		fpsCounter.tick();
	}

	public int getFPS()
	{
		return fpsCounter.fps;
	}
}