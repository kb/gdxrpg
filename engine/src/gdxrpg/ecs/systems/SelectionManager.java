package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.EntityFinder;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.DirectionHorizontal;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;

public class SelectionManager extends EntitySystem {

	private BaseGame game;

	public SelectionManager(Integer priority, BaseGame game)
	{
		super(priority);
		this.game = game;
	}
	private EntityFinder entityFinder;

	private static final Logger log = LoggerFactory.getLogger(SelectionManager.class);

	private final LinkedList<Entity> selection = new LinkedList<Entity>();

	public void selectEntity(Entity e)
	{
		this.getSelection().add(e);
	}

	public void clearSelection()
	{
		this.selection.clear();
	}

	public Entity getSelectedEntity()
	{
		if (getSelection().isEmpty()) {
			return null;
		}
		return this.getSelection().getLast();
	}

	public LinkedList<Entity> getSelection()
	{
		return selection;
	}

	// TODO proper implementation
	public void selectClosestEntity(Entity referenceEntity)
	{
		Position referencePosition = CM.get(referenceEntity, Position.class);
		double shortestDistance = Float.MAX_VALUE;
		for (Entity entity : entityFinder.find(referencePosition, game)) {
			Position entityPosition = CM.get(entity, Position.class);
			double distance = referencePosition.roundDistance(entityPosition);
			if (distance < shortestDistance) {
				shortestDistance = distance;
				selection.clear();
				selection.add(entity);
			}
		}
	}

	// TODO proper implementation
	public void selectNextEntity(DirectionHorizontal dir, int jumpsize)
	{
		for (int i = 0; i < jumpsize; i++) {
			Entity refEntity = selection.isEmpty() ? game.get(PlayerSystem.class).get() : selection.getLast();
			Position referencePosition = CM.get(refEntity, Position.class);
			double shortestDistance = Float.MAX_VALUE;
			for (Entity entity : entityFinder.find(referencePosition, game)) {
				if (entity == refEntity) {
					continue;
				}
				Position entityPosition = CM.get(entity, Position.class);
				switch (dir) {
					case E:
						if (entityPosition.x <= referencePosition.x && !selection.contains(entity))
							continue;
						break;
					case N:
						if (entityPosition.y >= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case NE:
						if (entityPosition.y >= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case NW:
						if (entityPosition.y >= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case S:
						if (entityPosition.y <= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case SE:
						if (entityPosition.y <= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case SW:
						if (entityPosition.y <= referencePosition.y && !selection.contains(entity))
							continue;
						break;
					case W:
						if (entityPosition.x >= referencePosition.x && !selection.contains(entity))
							continue;
						break;
					default:
						continue;

				}
				double distance = referencePosition.distanceTo(entityPosition);
				if (distance < shortestDistance) {
					shortestDistance = distance;
					selection.clear();
					selection.add(entity);
				}
			}
		}
	}

	public void reset(EntityFinder entityFinder)
	{
		this.clearSelection();
		this.entityFinder = entityFinder;
	}

	public void reset()
	{
		this.clearSelection();
		this.entityFinder = new EntityFinder();
	}

}
