package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Utterance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

public class DialogSystem extends IteratingSystem {
	
	private static final Logger log = LoggerFactory.getLogger(DialogSystem.class);
	
	private static final Family family = Family.all(Utterance.class).get();
	private final BaseGame game;

	public DialogSystem(int priority, BaseGame game)
	{
		super(family, priority);
		this.game = game;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		Utterance utter = CM.get(entity, Utterance.class);
		if (utter.timeToShow <= 0) {
			game.removeEntity(entity);
		}
		utter.timeToShow -= 1 * deltaTime;
//		log.debug("{}", utter);
	}

}
