package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.command.Command;
import gdxrpg.ecs.texttable.TextTable;
import gdxrpg.ecs.texttable.TextTable.TextTableAlign;
import gdxrpg.ecs.texttable.TextTable.TextTableBuilder;

import com.badlogic.ashley.core.EntitySystem;

public class HelpSystem extends EntitySystem {
	
	private BaseGame game;

	public HelpSystem(Integer priority, BaseGame game)
	{
		super(priority);
		this.game = game;
	}
	
	public TextTable commandHelp() {
		TextTableBuilder buildr = TextTable.builder();
		for (Command cmd : ShellSystem.registeredCommands.values()) {
			buildr.row();
			buildr.cell(cmd.getParser().getUsage());
			buildr.cell(cmd.getSynopsis()).align(TextTableAlign.Right).fgColor("ccffcc");
		}
		return buildr.build();
	}

}
