package gdxrpg.ecs.systems;

import gdxrpg.PartialEngine;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions;
import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.components.types.TypeComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

public class SingletonEntitySystem<T extends TypeComponent> extends EntitySystem {
	
	private static final Logger log = LoggerFactory.getLogger(SingletonEntitySystem.class);
	

	private Entity singletonEntity = null;
	private final Class<T> typeComponent;
	private final PartialEngine engine;
	private boolean failed;

	public SingletonEntitySystem(int priority, PartialEngine engine, Class<T> typeComponent)
	{
		super(priority);
		this.typeComponent = typeComponent;
		this.engine = engine;
	}

	public Entity get()
	{
		if (null == singletonEntity && !failed)
		{
			try
			{
				reset();
			} catch (MissingEntityException | TooManyEntitiesException e)
			{
				throw new RuntimeException(e);
			}
		} else {
			try
			{
				CM.validate(singletonEntity, typeComponent);
			} catch (MissingComponentsException e)
			{
				throw new RuntimeException(e);
			}
		}
		return singletonEntity;
	}

	protected void set(Entity singletonEntity)
	{
		this.singletonEntity = singletonEntity;
	}

	public boolean is(Entity needle)
	{
		return get().equals(needle);

	}

	protected void reset() throws MissingEntityException, TooManyEntitiesException
	{
		ImmutableArray<Entity> entitiesFor = engine.getEntitiesFor(Family.one(typeComponent).get());
		if (entitiesFor.size() == 0)
		{
			throw Exceptions.missingEntity(typeComponent);
		}
		else if (entitiesFor.size() > 1)
		{
			throw Exceptions.tooManyEntities(typeComponent);
		}
		else
		{
			set(entitiesFor.first());
		}
	}

	public <C extends Component> C get(Class<C> class1)
	{
		return CM.get(get(), class1);
	}

}
