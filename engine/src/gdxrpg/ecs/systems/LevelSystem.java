package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntityFactory;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.ai.PositionNode;
import gdxrpg.ecs.ai.PositionNodeGraph;
import gdxrpg.ecs.ai.PositionNodeManhattanHeuristic;
import gdxrpg.ecs.components.BelongsToLevel;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.DefaultPlayerSpawnPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Level;
import gdxrpg.ecs.components.types.LevelPortal;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.components.types.MapScenery;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.ecs.scripting.CoffeeLoader;
import gdxrpg.utils.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;

public class LevelSystem extends SingletonEntitySystem<Level> {

	private static final Logger log = LoggerFactory.getLogger(LevelSystem.class);
	private final Map<Position, Set<Entity>> positionIndex = new HashMap<>();
	private static final PositionNodeManhattanHeuristic HEURISTIC = new PositionNodeManhattanHeuristic();
	public PositionNodeGraph positionGraph;
	private IndexedAStarPathFinder<PositionNode> pathFinder;
	private final BaseGame game;

	public LevelSystem(Integer priority, BaseGame game)
	{
		super(priority, game, Level.class);
		this.game = game;
	}

	@Override
	public void set(Entity singletonEntity)
	{
		super.set(singletonEntity);
	}

	@Override
	public void update(float deltaTime)
	{
		rebuildPositionIndex();

		// // TODO Collisiondetection etc.
		ImmutableArray<Entity> levelPortalEntities = game.getEntitiesFor(Family.all(LevelPortal.class).get());
		for (int i = 0; i < levelPortalEntities.size(); i++)
		{
			// LevelPortal levelPortal = CM.get(levelPortalEntities.get(i),
			// LevelPortal.class);
			UniqueLabel label = CM.get(levelPortalEntities.get(i), UniqueLabel.class);
			Position portalPos = CM.get(levelPortalEntities.get(i), Position.class);
			Position playerPos = CM.get(game.get(PlayerSystem.class).get(), Position.class);
			if (portalPos.samePositionAs(playerPos))
			{
				game.addMessage("ENTER POrtaALALLALA");
				loadLevel(label.uniqueLabel);
			}
		}
		super.update(deltaTime);
	}

	@Override
	public void reset()
	{
		try
		{
			super.reset();
		} catch (MissingEntityException e)
		{
			return;
		} catch (TooManyEntitiesException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		rebuildPositionIndex();
		rebuildPassableGraph();
	}

	public Set<Entity> getEntitiesAt(int x, int y)
	{
		return getEntitiesAt(new Position(x, y));
	}

	public Set<Entity> getEntitiesAt(AbstractPosition position)
	{
		return getEntitiesAt(position, Family.all().get());
	}

	final public Set<Entity> getEntitiesAt(AbstractPosition position, final Family family)
	{
		Set<Entity> candidates = positionIndex.get(position);
		Set<Entity> ret = new HashSet<>();
		if (null == candidates)
			return ret;
		for (Entity e : candidates)
		{
			if (family.matches(e))
			{
				ret.add(e);
			}
		}
		return ret;
	}

	public void spawnPlayer()
	{
		Entity level = get();
		DefaultPlayerSpawnPosition spawnPos = CM.get(level, DefaultPlayerSpawnPosition.class);
		Entity player;
		try
		{
			player = game.get(PlayerSystem.class).get();
		} catch (Exception e)
		{
			player = EntityFactory.createPlayer(0, 0);
			log.error("{}", EntitySerializer.dump(player));
			game.addEntity(player);
		}

		player.add(new Position(spawnPos.roundX(), spawnPos.roundY()));

		// player.add(new AnimationBuilder()
		// .speed(5f)
		// .content("*")
		// .color("ff0000")
		// .sourcePositions(CM.get(player, Position.class))
		// .targetPositions(new Position(1, 9))
		// .frameLag(1)
		// .looping(true)
		// .build(AnimationType.LINE_TO_POSITION));

		// CM.get(player, Animatable.class).add(new AnimationBuilder()
		// .speed(0.3f)
		// .looping(true)
		// .frameLag(0)
		// .build(AnimationType.CIRCLE_AROUND_ENTITY));

		// CM.get(player, Animatable.class).add(new AnimationBuilder()
		// .speed(1.3f)
		// .looping(true)
		// .content("Me oh my what a waste of time")
		// .build(AnimationType.SPEECH_BUBBLE));

		// CM.get(player, Animatable.class).add(new AnimationBuilder()
		// .speed(1.3f)
		// .looping(true)
		// .content("100")
		// .frameLag(0)
		// .build(AnimationType.RISE_AND_FADE_TEXT));
	}

	private List<Entity> getEntitiesAt(Position needle, Family family, boolean onlyFirst)
	{
		List<Entity> ret = new ArrayList<>();
		for (Entity entity : game.getEntitiesFor(family))
		{
			log.debug("{}", EntitySerializer.dump(entity));
			Position pos = CM.get(entity, Position.class);
			if (pos.samePositionAs(needle))
			{
				ret.add(entity);
				if (onlyFirst)
					return ret;
			}
		}
		return ret;
	}

	private Runnable getLoadLevelTask(final String levelName, final Map<EntitySystem, Boolean> map)
	{
		log.debug("Enqueuing load level task {}", levelName);
		return new Runnable() {
			@Override
			public void run()
			{
				log.debug("Loading level {}", levelName);
				// engine.getSystem(LevelSystem.class).loadLevelFromCoffeeScript("/level-1.coffee");
				// Load the new level
				if (game.getInactiveEntities().containsKey(levelName))
				{
					log.debug("Loading level from cache: {}", levelName);
					List<Entity> list = game.getInactiveEntities().get(levelName);
					log.debug("Loading {} entities from cache", list.size());
					for (Entity e : list)
					{
						// if (CM.has(e, Level.class)) {
						// log.debug("Cached LEVEL: {}",
						// EntitySerializer.dump(e));
						// }
						game.addEntity(e);
					}
				}
				else
				{
					CoffeeLoader.evalLevel(levelName, game);
				}

				game.get(LevelSystem.class).spawnPlayer();

				// log.debug("Add viewport");
				// engine.getSystem(ViewportSystem.class).setProcessing(false);
				log.debug("DONE Loading level {}", levelName);
				reset();
				game.update();
				reset();
				game.applyStates(map);
				// for (GameSaveListener l : gameSaveListeners) {
				// l.onLoad();
				// }
			}
		};
	}

	private Runnable getUnloadLevelTask(final Map<EntitySystem, Boolean> map)
	{
		return new Runnable() {
			@Override
			public void run()
			{
				final Entity currentLevel = game.get(LevelSystem.class).get();
				final String currentLevelName =
						currentLevel != null
								? CM.get(currentLevel, UniqueLabel.class).uniqueLabel
								: null;
				if (currentLevelName == null)
				{
					return;
				}
				log.debug("Unloading level {} ", currentLevelName);
				log.debug("No of entities before unload: {}", game.getEntities().size());
				// log.debug("Unloading level {}",
				// EntitySerializer.dump(get()));
				if (!game.getInactiveEntities().containsKey(currentLevelName))
				{
					game.getInactiveEntities().put(currentLevelName, new ArrayList<Entity>());
				}
				ImmutableArray<Entity> entitiesFor = game.getEntitiesFor(Family.all(BelongsToLevel.class).get());
				log.debug("Caching {} entities in level {}", entitiesFor.size(), currentLevelName);
				game.getInactiveEntities().get(currentLevelName).clear();
				for (int i = 0; i < entitiesFor.size(); i++)
				{
					Entity e = entitiesFor.get(i);
					// log.debug("REMOVE {}",
					// EntitySerializer.dump(e));
					game.getInactiveEntities().get(currentLevelName).add(e);
				}
				// game.entityCache.get(currentLevelName).add(currentLevel);
				for (Entity e : game.getInactiveEntities().get(currentLevelName))
				{
					game.removeEntity(e);
				}
				log.debug("No of entities after unload: {}", game.getEntities().size());
				// for (Entity e : game.getEntities())
				// {
				// // log.debug("game.engine contains {}",
				// EntitySerializer.dump(e));
				// }
				log.debug("No of cached entities: {}", game.getInactiveEntities().get(currentLevelName).size());
				game.applyStates(map);
			}
		};
	}

	public void loadLevel(final String levelName)
	{
		Map<EntitySystem, Boolean> oldStates = game.getSystemStates();
		game.addBeforeUpdateTask(getUnloadLevelTask(oldStates));
		game.addBeforeUpdateTask(getLoadLevelTask(levelName, oldStates));
		game.stopAllSystems();
	}

	public List<Position> pathFromTo(Entity source, Entity target)
	{
		Position pos = CM.get(source, Position.class);
		Position targetPos = CM.get(target, Position.class);
		return MathUtils.line(pos.roundX(), pos.roundY(), targetPos.roundX(), targetPos.roundY());
	}

	public String entitiesWithPath(Entity referenceEntity, List<Entity> entities)
	{
		StringBuilder sb = new StringBuilder();
		Position referencePos = CM.get(referenceEntity, Position.class);
		for (Entity entity : entities)
		{
			Position pos = CM.get(entity, Position.class);
			sb.append(pos.pathTo(referencePos));
			sb.append("\t");
			sb.append(CM.getTypeComponent(entity).getSimpleName());
			sb.append("\t");
			sb.append(CM.get(entity, Description.class).name);
			sb.append("\t");
			sb.append(pos.distanceTo(referencePos));
			sb.append(" [").append(entity.getId()).append("]");
			sb.append("\n");
		}
		return sb.toString();
	}

	private void rebuildPositionIndex()
	{
		Extent levelExtent = get(Extent.class);
		
		// rebuild index
//		log.debug("{}: {}", get(UniqueLabel.class), levelExtent);
		for (int y = 0; y < levelExtent.height; y++)
		{
			for (int x = 0; x < levelExtent.width; x++)
			{
				Position pos = new Position(x, y);
				if (!positionIndex.containsKey(pos))
				{
					positionIndex.put(pos, new HashSet<Entity>());
				}
				positionIndex.get(pos).clear();
			}
		}
		ImmutableArray<Entity> withPosition = game.getEntitiesFor(Family.all(Position.class).get());
		log.trace("Reindexing {} entities by position", withPosition.size());
		for (int i = 0; i < withPosition.size(); i++)
		{
			Entity e = withPosition.get(i);
//			 log.debug("INDEX {}", EntitySerializer.dump(e));
			Position pos = CM.get(e, Position.class);
//			log.debug("POS {}", pos);
			positionIndex.get(pos).add(e);
		}
	}

	public List<Position> findPath(Position fromPos, Position toPos)
	{
		GraphPath<PositionNode> path = new DefaultGraphPath<>();
		pathFinder.searchNodePath(
				positionGraph.getNode(fromPos),
				positionGraph.getNode(toPos),
				HEURISTIC,
				path);
		List<Position> ret = new ArrayList<>();
		for (PositionNode posNode : path)
		{
			ret.add(posNode.position);
		}
		return ret;

	}

	private void rebuildPassableGraph()
	{
		Extent levelExtent = get(Extent.class);
		log.info("Rebuilding graph");
		this.positionGraph = new PositionNodeGraph(levelExtent);
		for (int y = 0; y < levelExtent.height; y++)
		{
			for (int x = 0; x < levelExtent.width; x++)
			{
				Position fromPos = new Position(x, y);
				PositionNode fromPosNode = positionGraph.getOrAddNode(fromPos);
				for (DirectionHorizontal dir : DirectionHorizontal.values())
				{
					if (dir == DirectionHorizontal.HERE)
						continue;
					Position toPos = new Position(x + dir.dx, y + dir.dy);
					if (toPos.roundX() < 0
                        || toPos.roundY() < 0
                        || toPos.roundX() >= levelExtent.width
                        || toPos.roundY() >= levelExtent.height)
					{
						continue;
					}

					PositionNode toPosNode = positionGraph.getOrAddNode(toPos);
					Set<Entity> neighborSet = getEntitiesAt(toPos, Family.one(MapGround.class, MapScenery.class).get());
//					log.debug("NEIGHBORS: {}", neighborSet.size());
//					log.debug("INDEX: {}", positionIndex.get(toPos));
					boolean skip = false;
					for (Entity neighbor : neighborSet)
					{
						if (! CM.get(neighbor, Passable.class).passable)
						{
							skip = true;
							break;
						}
					}
					if (!skip)
						fromPosNode.addConnection(toPosNode, 1);
				}
			}
		}
		this.pathFinder = new IndexedAStarPathFinder<>(positionGraph);
	}
}
