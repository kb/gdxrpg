package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.Command;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.CommandParseException;
import gdxrpg.command.CommandParser;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.components.types.Shell;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

public class ShellSystem extends SingletonEntitySystem<Shell> {

	static Logger log = LoggerFactory.getLogger(ShellSystem.class);
	public static final Map<String, Command> registeredCommands = new HashMap<>();
	static
	{
		Set<Class<? extends AbstractCommand>> allTypes = new Reflections("gdxrpg.command.builtin").getSubTypesOf(AbstractCommand.class);
		for (Class<? extends Command> clazz : allTypes)
		{
			log.debug("Registering builtin command {}", clazz);
			registerCommand(clazz);
		}
	}

	public static void registerCommand(Class<? extends Command> clazz)
	{
		try
		{
			Command cmd = clazz.newInstance();
			if (registeredCommands.containsKey(cmd.getName()))
			{
				throw new RuntimeException(String.format("Command '%s' already associated with %s",
						cmd.getName(), registeredCommands.get(cmd.getName())));
			}
			registeredCommands.put(cmd.getName().toLowerCase(), cmd);
		} catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private final BaseGame game;

	public ShellSystem(Integer priority, BaseGame game)
	{
		super(priority, game, Shell.class);
		this.game = game;
	}

	@Override
	protected void reset() throws MissingEntityException, TooManyEntitiesException
	{
		ImmutableArray<Entity> entitiesFor = game.getEntitiesFor(Family.one(Shell.class).get());
		if (entitiesFor.size() == 0)
		{
			Entity shellEntity = new Entity();
			shellEntity.add(new Shell());
			game.addEntity(shellEntity);
			set(shellEntity);
		}
	}

	private Shell getTopShell()
	{
		Shell shell = CM.get(get(), Shell.class);
		return shell;
	}

	Shell getShell()
	{
		Shell shell = getTopShell();
		if (!shell.subshells.isEmpty())
		{
			shell = shell.subshells.getLast();
		}
		return shell;
	}

	private boolean historyActive(Shell shell)
	{
		return shell.historyIndex > 0;
	}

	private void replaceInputWith(Shell shell, String str)
	{
		shell.input.replace(0, shell.input.length(), str);
	}

	private void resetHistory(Shell shell)
	{
		shell.historyIndex = -1;
	}

	public void addAlias(String k, String v)
	{
		getShell().aliases.put(k, v);
	}

	public void backspace()
	{
		Shell shell = getShell();
		if (shell.cursor > 0)
		{
			shell.input.delete(shell.cursor - 1, shell.cursor);
			shell.cursor--;
		}
	}

	public void cancel()
	{
		Shell shell = getShell();
		if (historyActive(shell))
		{
			shell.input.replace(0, shell.input.length(), shell.oldContent);
			resetHistory(shell);
		}
		else
		{
			Shell topShell = getTopShell();
			topShell.subshells.clear();
			game.exitMode();
		}
	}

	public void delete()
	{
		Shell shell = getShell();
		if (shell.cursor < shell.input.length())
		{
			shell.input.delete(shell.cursor, shell.cursor + 1);
		}
	}

	public void deleteToEOL()
	{
		Shell shell = getShell();
		shell.input.delete(shell.cursor, shell.input.length());
	}

	public void end()
	{
		Shell shell = getShell();
		shell.cursor = shell.input.length();
	}

	public void enter()
	{
		Shell topShell = getTopShell();
		if (!topShell.subshells.isEmpty())
		{
			Shell subShell = topShell.subshells.removeLast();
			topShell.input.append(" ");
			topShell.input.append(subShell.input);
			if (topShell.subshells.isEmpty())
			{
				enter();
				return;
			}
		}
		else
		{
			String inputStr = topShell.input.toString();
			boolean parsedOkay = parseCmdline(inputStr);
			if (parsedOkay)
			{
				topShell.history.add(0, inputStr);
			}
			pos1();
			deleteToEOL();
			game.exitMode();
		}
	}

	public void enterSubshell(String shellName)
	{
		Shell topShell = getTopShell();
		Shell subshell = new Shell();
		subshell.shellName = shellName;
		topShell.subshells.addFirst(subshell);

	}

	public int getCursor()
	{
		Shell shell = getShell();
		return shell.cursor;
	}

	public StringBuilder getInput()
	{
		Shell shell = getShell();
		return shell.input;
	}

	public String getShellName()
	{
		return getShell().shellName;
	}

	public void historyNext()
	{
		Shell shell = getShell();
		shell.historyIndex = shell.historyIndex - 1;
		if (shell.historyIndex <= 0)
		{
			shell.historyIndex = 0;
			replaceInputWith(shell, shell.oldContent);
			resetHistory(shell);
		}
		else
		{
			shell.oldContent = shell.input.toString();
			replaceInputWith(shell, shell.history.get(shell.historyIndex));
		}
	}

	public void historyPrev()
	{
		Shell shell = getShell();
		shell.historyIndex = Math.min(shell.historyIndex + 1, shell.history.size() - 1);
		shell.oldContent = shell.input.toString();
		if (shell.history.size() > 0)
			shell.input.replace(0, shell.input.length(), shell.history.get(shell.historyIndex));
	}

	public void insert(Character key)
	{
		Shell shell = getShell();
		shell.input.insert(shell.cursor, key);
		shell.cursor++;
	}

	public boolean isInteractive()
	{
		return getShell().isInteractive;
	}

	public void left()
	{
		Shell shell = getShell();
		resetHistory(shell);
		shell.cursor = Math.max(shell.cursor - 1, 0);
	}

	public boolean parseCmdline(String cmdline)
	{
		boolean ret = false;
		log.debug("Handling input: '{}'", cmdline);

		if (cmdline.length() == 0)
		{
			cancel();
		}
		String firstToken = cmdline.split("\\s")[0];
		// Expand aliases
		log.trace("Start expanding aliases");
		for (Entry<String, String> kv : getShell().aliases.entrySet())
		{
			String alias = kv.getKey();
			if (firstToken.equals(alias))
			{
				String replace = kv.getValue();
				log.trace("Expanding alias '{}' to '{}'", alias, replace);
				cmdline = replace + cmdline.substring(firstToken.length());
				firstToken = cmdline.split("\\s")[0];
			}
		}
		log.trace("Done expanding aliases");

		// Try commands
		Command cmd = registeredCommands.get(firstToken.toLowerCase());
		if (null == cmd)
			return false;
		log.trace("For command: {}", cmd);
		try
		{
			CommandParser parser = cmd.getParser();
			List<String> tokenized = parser.tokenize(cmdline);
			if (parser.getNumberOfArguments() > 0 && tokenized.size() == 1 && isInteractive())
			{
				// List<String> interactiveTokenized = new ArrayList<>();
				for (int i = 0; i < parser.getNumberOfArguments(); i++)
				{
					parser.askForInput(game, i);
					enterSubshell(parser.getArgumentPrompt(i) + "_" + i);
				}
				return false;
			}
			CommandInvocation invoc = parser.parse(game, cmdline);
			log.trace("INVOC : {}", invoc);
			game.getInputHandler().handleInputCommand(invoc);
			ret = true;
		} catch (CommandParseException e)
		{
			game.addMessage(e.getMessage());
			e.printStackTrace();
			// throw new RuntimeException(e);
		}
		// if (ret)
		// game.getInputHandler().handleInputCommand(CommandInvocations.focus_map);
		return ret;
	}

	public void pasteFromClipboard()
	{
		String clipboardText = game.getClipboardContent();
		for (char ch : clipboardText.toCharArray())
		{
			insert(ch);
		}
	}

	public void pos1()
	{
		Shell shell = getShell();
		shell.cursor = 0;
	}

	public void readCmdFile(InputStream is)
	{
		try
		{
			String asString = IOUtils.toString(is);
			boolean wasInteractive = getShell().isInteractive;
			getShell().isInteractive = false;
			for (String line : asString.split("\n"))
			{
				if (line.matches("^\\s*#") || line.matches("^\\s*$"))
					continue;
				log.debug("Parsing init command: {}", line);
				parseCmdline(line);
			}
			getShell().isInteractive = wasInteractive;
		} catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void right()
	{
		Shell shell = getShell();
		shell.cursor = Math.min(shell.cursor + 1, shell.input.length());
	}

	public void updateAutoComplete()
	{
		Shell shell = getShell();
		String lowerCase = shell.input.toString().toLowerCase();
		List<String> matches = new ArrayList<>();
		for (Entry<String, Command> kv : registeredCommands.entrySet())
		{
			if (kv.getKey().startsWith(lowerCase))
			{
				matches.add(kv.getKey());
			}
		}
		game.addMessage(String.format("%s", matches));
	}

	public interface SubshellExitListener {
		void onExit(String response);
	}

}