package gdxrpg.ecs.systems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.action.AbstractAction;
import gdxrpg.ecs.components.types.GameTime;


/**
 * This system processes {@link AbstractAction Actions}.
 * 
 */
public class ActionSystem extends SingletonEntitySystem<GameTime>
{
	
	private static final Logger log = LoggerFactory.getLogger(ActionSystem.class);

	private BaseGame game;

	public ActionSystem(Integer priority, BaseGame game)
	{
		super(priority, game, GameTime.class);
		this.game = game;
	};

	@Override
	protected void reset() throws MissingEntityException, TooManyEntitiesException
	{
		ImmutableArray<Entity> entitiesFor = game.getEntitiesFor(Family.one(GameTime.class).get());
		if (entitiesFor.size() == 0)
		{
			Entity timeEntity = new Entity();
			GameTime time = new GameTime();
			time.dateTime = game.getConfig().startDate();
			timeEntity.add(time);
			game.addEntity(timeEntity);
			set(timeEntity);
		} else {
			super.reset();
		}
	}
	
	@Override
	public void update(float deltaTime)
	{
		Entity player = game.getPlayer();
		Busy playerBusy = CM.get(player, Busy.class);
		boolean turnFinished = false;
		int timePassed = (int)deltaTime;
		log.debug("Time passed {}", timePassed);
		
		// Player has nothing to do
		if (playerBusy.queue.isEmpty()) {
			log.trace("Player has nothing to do.");
			return;
		}
		
		// Increase game time
		get(GameTime.class).dateTime = get(GameTime.class).dateTime.plusNanos(game.getConfig().nanosPerTick() * timePassed);
		get(GameTime.class).turns += 1;
		
		// TODO normalize timePassed to player
		// Reduce the passed time
		for (Entity entity : game.getEntitiesFor(Family.all(Busy.class).get())) {
			CM.get(entity, Busy.class).tick(timePassed);
		}

		// Player has a current action and it should be executed now
		if (playerBusy.finished() && ! playerBusy.queue.isEmpty()) {
			playerBusy.execute(game, player);
			turnFinished = true;
		}
		
		// If the player finished his move, execute
		if (turnFinished) {
			for (Entity entity : game.getEntitiesFor(Family.all(Busy.class).get())) {
				if (entity.getId() == player.getId()) {
					continue;
				}
				Busy busy = CM.get(entity, Busy.class);
				// TODO bonus
				if (busy.finished() && ! busy.queue.isEmpty()) {
					busy.execute(game, entity);
				}
			}
			game.addAfterUpdateTask(new Runnable() {
				
				@Override
				public void run()
				{
					game.setProcessing(false);
				}
			});
		}
	}

}
