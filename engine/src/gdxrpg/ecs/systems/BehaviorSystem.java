package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Behavior;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeParser;

public class BehaviorSystem extends IteratingSystem {
	
	private static final Logger log = LoggerFactory.getLogger(BehaviorSystem.class);

	private BaseGame game;
	private final BehaviorTreeParser<Entity> parser;

	public BehaviorSystem(Integer priority, BaseGame game)
	{
		super(Family.all(Behavior.class).get(), priority);
		this.game = game;
		parser = new BehaviorTreeParser<>(BehaviorTreeParser.DEBUG_NONE);
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		Behavior behaviorComponent = CM.get(entity, Behavior.class);
		if (null == behaviorComponent.btree)
		{
			loadBehaviorTree(entity);
		}
		log.info("Running behavior '{}' for entity {}", behaviorComponent.behavior, EntitySerializer.dumpTerse(entity));
		behaviorComponent.btree.step();
	}

	private void loadBehaviorTree(Entity entity)
	{
		try
		{
			Behavior behaviorComponent = CM.get(entity, Behavior.class);
			Path treePath = game.getGameDirectory().resolve("behaviors/" + behaviorComponent.behavior);
			behaviorComponent.btree = parser.parse(Files.newBufferedReader(treePath), entity);
		} catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
