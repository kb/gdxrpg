package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.Animation;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

public class AnimationSystem extends IteratingSystem {

	private static final Logger log = LoggerFactory.getLogger(AnimationSystem.class);

	private BaseGame game;
	private static final Family family = Family.all(Animatable.class).get();

	public AnimationSystem(Integer priority, BaseGame game)
	{
		super(family, priority);
		this.game = game;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		Set<Animation> animations = CM.get(entity, Animatable.class).animations;
		Set<Animation> delete = new HashSet<>();
		for (Animation animation : animations)
		{
			// log.trace("Animation curFrame {}", animation.currentFrame);
			animation.incCurrentFrame(animation.fps / game.getConfig().targetFPS());
			if (animation.isFinished())
			{
				// // TODO this is buggy if the entity is not an animation
				// // TODO reap orphaned animations
				// // this.game.engine.removeEntity(entity);
				delete.add(animation);
			}
		}
//		log.debug("{} toDelete: ", delete.size());
//		log.debug("BEFORE {}", animations.size());
		boolean removeAll = animations.removeAll(delete);
//		log.debug("AFTER {}", animations.size());
		if ( delete.size() > 0 && ! removeAll) {
			throw new RuntimeException();
		}
		delete.clear();
	}
}
