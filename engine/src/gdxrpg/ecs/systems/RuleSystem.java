package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.builtin.Eval;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Collision;
import gdxrpg.ecs.components.Script;
import gdxrpg.ecs.scripting.CoffeeLoader;
import gdxrpg.ecs.scripting.CollisionScript;
import gdxrpg.ecs.scripting.CommandScript;
import gdxrpg.ecs.scripting.IScript;
import gdxrpg.utils.Logging;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.script.Invocable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;

public class RuleSystem extends EntitySystem {

	private static final Logger log = LoggerFactory.getLogger(RuleSystem.class);
	private BaseGame game;
	// private Map<String, Invocable> rules = new HashMap<>();
	private final Set<Entity> evaledEntity = new HashSet<>();
	private Map<Class<? extends IScript>, Map<Entity, IScript>> rules = new HashMap<>();
	{
		registerInterface(CommandScript.class);
		registerInterface(CollisionScript.class);
	}

	public void registerInterface(Class<? extends IScript> iface)
	{
		rules.put(iface, new HashMap<>());
	}

	public RuleSystem(Integer priority, BaseGame game)
	{
		super(priority);
		this.game = game;
	}

	@Override
	public void update(float deltaTime)
	{
		for (Entity ruleEntity : game.getEntitiesFor(Family.all(Script.class).get()))
		{
			Script coffeeScriptRule = CM.get(ruleEntity, Script.class);
			if (!evaledEntity.contains(ruleEntity))
			{
				for (Class<? extends IScript> iface : rules.keySet())
				{
					String logName = CM.getTypeComponent(ruleEntity).getSimpleName() + "@" + ruleEntity.getId();
					Invocable invoc = (Invocable) CoffeeLoader.evalJavascript(game, coffeeScriptRule.code, logName);
					IScript actualIface = invoc.getInterface(iface);
					if (null != actualIface)
					{
						log.debug(Logging.HL1, "Adding {} for entity {}", iface.getSimpleName(), EntitySerializer.dumpTerse(ruleEntity));
						rules.get(iface).put(ruleEntity, actualIface);
					}
				}
				evaledEntity.add(ruleEntity);
			}
		}
	}

	public Boolean trigger(Collision collision)
	{
		for (Entry<Entity, CollisionScript> kv : getRulesByClass(CollisionScript.class).entrySet())
		{
			Entity self = kv.getKey();
			if (collision.target != null && collision.target.equals(self))
			{
				CollisionScript collisionScript = kv.getValue();
				Boolean scriptReturn = collisionScript.onCollision(game, self, collision.source);
				if (null != scriptReturn && !scriptReturn)
					return false;
			}
		}
		return true;
	}

	public Boolean trigger(CommandInvocation cmdInvoc)
	{
		if (cmdInvoc.getCommand().getClass() == Eval.class
				||
				cmdInvoc.getCommand().getClass().getSimpleName().startsWith("Shell"))
		{
			return true;
		}
		for (Entry<Entity, CommandScript> kv : getRulesByClass(CommandScript.class).entrySet())
		{
			CommandScript commandInvocationScript = kv.getValue();
			Entity self = kv.getKey();
			Boolean scriptReturn = commandInvocationScript.onCommand(game, self, cmdInvoc.getCommand(), cmdInvoc.arguments);
			if (null != scriptReturn && !scriptReturn)
				return false;
		}
		return true;
	}

	private <T extends IScript> Map<Entity, T> getRulesByClass(Class<T> clazz)
	{
		Map<Entity, T> ret = new HashMap<>();
		for (Entry<Entity, IScript> kv : rules.get(clazz).entrySet())
		{
			ret.put(kv.getKey(), clazz.cast(kv.getValue()));
		}
		return ret;
	}

}
