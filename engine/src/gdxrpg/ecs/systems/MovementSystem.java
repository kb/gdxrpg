package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.action.MoveAction;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.DirectionHorizontal;
import gdxrpg.utils.MovementUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

public class MovementSystem extends IteratingSystem {

	private final static Logger log = LoggerFactory.getLogger(MovementSystem.class);

	private BaseGame game;
	private static final Family family = Family.all(Position.class, Movement.class, Busy.class).get();

	public MovementSystem(Integer priority, BaseGame game)
	{
		super(family, priority);
		this.game = game;
	}

	// public boolean isMoving(Entity entity)
	// {
	// return CM.has(entity, Movement.class) && !CM.get(entity,
	// Movement.class).is(DirectionHorizontal.HERE);
	// }

	@Override
	public void update(float deltaTime)
	{
		long now = System.nanoTime();
		super.update(deltaTime);
		if (log.isTraceEnabled())
			log.trace("Updating {} took {} ms", getClass().getSimpleName(), (System.nanoTime() - now) / 1_000_000f);
	}

	public void move(Entity entity, DirectionHorizontal moveDir)
	{
		move(entity, Movement.fromDirection(moveDir));
	}

	public void move(Entity entity, int dx, int dy)
	{
		Position curPos = CM.get(entity, Position.class);
		Position newPos = curPos.plusOffset(new Offset(dx, dy));
		CM.get(entity, Busy.class).queue.addLast(new MoveAction(100, newPos));
//		Busy busy = CM.get(entity, Busy.class);
//		if (! busy.finished()) {
//			log.error("Not moving because still busy for {}", busy.getTimeLeft());
//			return;
//		}
//		// log.debug("before: {}", CM.get(player, Movement.class));
//		CM.get(entity, Movement.class).dx = dx;
//		CM.get(entity, Movement.class).dy = dy;
//
//		int speed = 100;
//		if (CM.has(entity, Speed.class))
//		{
//			speed = CM.get(entity, Speed.class).speed;
//		}
//
//		int time = (int) (100 / Math.ceil(Math.max(1, speed)/100f));
//		log.debug(Xterm256Constants.HIGHLIGHT, "Adding busy time of {} to {}", time, CM.getTypeComponent(entity));
//		busy.inc(time);
//		// log.debug("after: {} (Pos: {})", CM.get(player, Movement.class),
//		// CM.get(player, Position.class));
//		// game.setProcessing(true);
//		// log.debug("{}", game.isProcessing());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime)
	{
		// Position oldPos = CM.get(entity, Position.class);
		// log.debug("Should I move entity {}?", EntitySerializer.dump(entity));
		log.trace("Should I move entity {}?", CM.getTypeComponent(entity));
		if (!CM.get(entity, Movement.class).isMoving())
		{
			log.trace("Don't move {}: Not moving", EntitySerializer.dumpTerse(entity));
			return;
		}
		else if (!CM.get(entity, Busy.class).finished())
		{
			log.trace("Don't move {}: Still busy for {}", EntitySerializer.dumpTerse(entity), CM.get(entity, Busy.class));
			return;
		}

		log.trace("Moving {}", EntitySerializer.dumpTerse(entity));
		Position newPos = MovementUtils.calculateNextPosition(game, entity);
		entity.add(Position.fromPosition(newPos));
		CM.get(entity, Movement.class).stopMoving();
		// game.get(GameEventSystem.class).trigger(new GameEvent(GameEventType.PLAYER_MOVED));
		// if (game.get(PlayerSystem.class).is(entity) && !isMoving(entity)) {
		// game.setProcessing(false);
		// }
	}

	public void move(Entity entity, Movement determineMovement)
	{
		move(entity, (int)determineMovement.dx, (int) determineMovement.dy);
		
	}
}