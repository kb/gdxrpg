package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntityFactory;
import gdxrpg.ecs.Exceptions.MissingEntityException;
import gdxrpg.ecs.Exceptions.TooManyEntitiesException;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Viewport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public class ViewportSystem extends SingletonEntitySystem<Viewport> {

	private static final Logger log = LoggerFactory
			.getLogger(ViewportSystem.class);

	private BaseGame game;

	public ViewportSystem(Integer priority, BaseGame game)
	{
		super(priority, game, Viewport.class);
		this.game = game;
	}

	@Override
	protected void reset()
	{
		try
		{
			super.reset();
		} catch (MissingEntityException e)
		{
			Entity currentLevel = game.get(LevelSystem.class).get();
			set(EntityFactory.createViewPort(CM.get(currentLevel, UniqueLabel.class).uniqueLabel));
		} catch (TooManyEntitiesException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void update(float deltaTime)
	{
		Entity viewport = get();
//		log.debug("Viewport size in tiles: {}", CM.get(entity, Extent.class));
//		 log.debug("Level size in tiles: {}", CM.get(currentLevel, Extent.class));
		Entity currentLevel = game.get(LevelSystem.class).get();
		Extent levelExtent = CM.get(currentLevel, Extent.class);
		Entity player = game.get(PlayerSystem.class).get();
		Position playerPos = CM.get(player, Position.class);
		Offset viewportOffset = CM.get(viewport, Offset.class);
		Extent viewportExtent = CM.get(viewport, Extent.class);
		// log.debug("{}", MainGame.dumpEntityInfo(currentLevel));
		// log.debug("{}", MainGame.dumpEntityInfo(viewport));

		/*
		 * Ideally: the viewport has the player bang in the center If player
		 * reaches the edges of the level: Fix the width / height Otherwise: If
		 * the player reaches the edges of the viewport: change the offset to
		 * show more of the level
		 */

		int edgeMargin = 3;
		int shiftHeight = 2 * edgeMargin;
		int shiftWidth = 2 * edgeMargin;

		// assert playerPos != null : this;
		// assert extent != null : this;

		// // Hard Limits: Level
		if (playerPos.x - viewportExtent.height + edgeMargin <= 0)
		{
			viewportOffset.dx = 0;
		}
		else if (playerPos.x + viewportExtent.height >= levelExtent.width - edgeMargin)
		{
			viewportOffset.dx = levelExtent.width - viewportExtent.width;
		}
		else if (playerPos.x + edgeMargin >= viewportOffset.dx + viewportExtent.width)
		{
			viewportOffset.dx = Math.max(0, Math.min(viewportOffset.dx + viewportExtent.width - shiftWidth, levelExtent.width - viewportExtent.width));
		}
		else if (playerPos.x - edgeMargin < viewportOffset.dx)
		{
			viewportOffset.dx = Math.max(viewportOffset.dx - viewportExtent.width + shiftWidth, 0);
		}
		// Hard Limits: Level
		if (playerPos.y - viewportExtent.height + edgeMargin <= 0)
		{
			viewportOffset.dy = 0;
		}
		else if (playerPos.y + viewportExtent.height >= levelExtent.height)
		{
			viewportOffset.dy = levelExtent.height - viewportExtent.height;
			// Soft Limits: Viewport
		}
		else
		{

			// TODO buggy
			
			// moving to bottom of viewport
			if (playerPos.y >= viewportOffset.dy + viewportExtent.height - edgeMargin)
			{
				int oldDy = viewportOffset.dy;
				viewportOffset.dy = Math.max(0, 
						Math.min(viewportOffset.dy + viewportExtent.height - shiftHeight,
								levelExtent.height - viewportExtent.height));
				log.debug("Changing from {} to {}", oldDy, viewportOffset.dy);
				log.debug("ViewportOffset: {}", viewportOffset);
				log.debug("viewportExtent: {}", viewportExtent);
			}
			
			// movign to top of viewport
			else if (playerPos.y < viewportOffset.dy + edgeMargin )
			{
				viewportOffset.dy = Math.max(0, 
						viewportOffset.dy - viewportExtent.height + shiftHeight);
			}
		}
		// int foo = 2;
		// if (true) {
		// foo++;
		// }

		// log.debug(EntitySerializer.dumpEntityInfo(viewport));

		// TODO
		// if (playerPos.x > viewportExtent.width - 5) {
		// viewportOffset.dx += 5;
		// };
		// if (playerPos.y > viewportExtent.height - 5) {
		// viewportOffset.dy += 5;
		// };
	}

	public boolean isWithinCurrentViewport(AbstractPosition pos)
	{
		return isWithin(get(), pos);
	}

	public boolean isWithinCurrentViewport(Entity entity)
	{
		Position position = CM.get(entity, Position.class);
		return (null != position && isWithin(get(), position));
	}

	public boolean isWithin(Entity vp, AbstractPosition pos)
	{
		Offset offset = CM.get(vp, Offset.class);
		Extent extent = CM.get(vp, Extent.class);
		if (pos.x < offset.dx || pos.y < offset.dy
				|| pos.x >= offset.dx + extent.width
				|| pos.y >= offset.dy + extent.height)
		{
			return false;
		}
		return true;
	}

}
