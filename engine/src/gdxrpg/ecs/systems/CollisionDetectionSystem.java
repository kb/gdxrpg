package gdxrpg.ecs.systems;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntityFactory;
import gdxrpg.ecs.components.Collision;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.Speed;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.components.types.MapScenery;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.components.types.Portal;
import gdxrpg.ecs.constants.CollisionType;
import gdxrpg.utils.MovementUtils;

public class CollisionDetectionSystem extends SortedIteratingSystem {

	private static final Logger log = LoggerFactory.getLogger(CollisionDetectionSystem.class);

	private static final Family family = Family.all(Movement.class, Position.class, Speed.class).get();

	/**
	 * Compare entities so that the player is the first entity in the iteration.
	 * @return
	 */
	private final static Comparator<Entity> getComparator() {
		return new Comparator<Entity>() {

			@Override
			public int compare(Entity o1, Entity o2) {
				if (o1.equals(o2)) {
					return 0;
				}
				if (CM.has(o1, Player.class))
					return -1;
				if (! CM.has(o2, Player.class))
					return +1;
				return 0;
			}
		};
	}

	private final BaseGame game;

	private Map<AbstractPosition, Entity> positionOccupied = new HashMap<>();

	public CollisionDetectionSystem(Integer priority, BaseGame game) {
		super(family, getComparator(), priority);
		this.game = game;
	}

	private void calculateOccupied()
	{
		for (Entity entity : game.getEntitiesFor(Family.all(Position.class, Passable.class).get())) {
			Position pos = CM.get(entity, Position.class);
			Passable passable = CM.get(entity, Passable.class);
			Movement mov = CM.get(entity, Movement.class);
			if (null != mov && !mov.isMoving() && !passable.passable) {
				this.setOccupied(pos, entity);
				continue;
			}
			if (passable != null && !passable.passable) {
				this.setOccupied(pos, entity);
			}
//			 log.debug("OCCUPADO: {}", positionOccupied);
		}
	}

	private synchronized void clearOccupied() {
		this.positionOccupied.clear();
	}

	private synchronized Entity getOccupied(AbstractPosition pos) {
		return this.positionOccupied.get(pos);
	}

	private synchronized void setOccupied(AbstractPosition pos, Entity entity) {
		this.positionOccupied.put(pos, entity);
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		Position newPos = MovementUtils.calculateNextPosition(game, entity);
		log.trace("{} --> {}  [{}] ???", CM.get(entity, Position.class), newPos, CM.get(entity, Movement.class));

		Entity collision = checkForCollision(entity, newPos);
		if (null != collision) {
			game.addEntity(collision);
		}

		// log.debug("OCCUPADO: {}", positionOccupied);
	}

	/**
	 * Collision detection
	 * @param entity 
	 * @param targetPos 
	 * @return Entity with EntityType {@link Collision} or null if no collision
	 */
	public Entity checkForCollision(Entity entity, Position targetPos)
	{
		/*
		 * Map bounds
		 */
		Entity currentLevel = game.get(LevelSystem.class).get();
		Extent extent = CM.get(currentLevel, Extent.class);
		if (targetPos.roundX() < 0 || targetPos.roundY() < 0 || targetPos.roundX() >= extent.width || targetPos.roundY() >= extent.height) {
			return EntityFactory.createCollision(CollisionType.OUT_OF_BOUNDS, entity, targetPos);
		} else {
			/*
			 * Walls and such
			 */
			for (Entity tile : game.get(LevelSystem.class).getEntitiesAt(targetPos, Family.one(MapGround.class, MapScenery.class).get())) 
			{
				if (tile != null && ( !CM.has(tile, Passable.class) || !CM.get(tile, Passable.class).passable)) {
					return EntityFactory.createCollision(CollisionType.WALK_INTO_NONPASSABLE, entity, tile, targetPos);
				}
			}
		}

		/*
		 * NPCs or Player about to move to the position in question
		 */
		Entity target = getOccupied(targetPos);
		if (target != null && target != entity) {
			// log.debug("NEXT COLLISION between {} and {}", curPos, newPos);
			return EntityFactory.createCollision(CollisionType.WALK_INTO_MOVING_ENTITY, entity, target, targetPos);
		} else {
			this.setOccupied(targetPos, entity);
		}

		/*
		 * Portals
		 */
		if (CM.get(entity, Player.class) != null) {
			for (Entity portalEntity : game.getEntitiesFor(Family.all(Portal.class).get())) {
				Position pos = CM.get(portalEntity, Position.class);
				if (targetPos.samePositionAs(pos)) {
					return EntityFactory.createCollision(CollisionType.ENTER_PORTAL, entity, portalEntity, targetPos);
				}
			}
		}
		return null;
	}

	@Override
	public void update(float deltaTime) {
		this.clearOccupied();
		long now = System.nanoTime();
		calculateOccupied();
//		super.update(deltaTime);
		if (log.isTraceEnabled())
			log.trace("Updating {} took {} ms", getClass().getSimpleName(), (System.nanoTime() - now) / 1_000_000f);
	}
}