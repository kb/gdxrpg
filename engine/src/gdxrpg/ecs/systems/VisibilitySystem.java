package gdxrpg.ecs.systems;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.EyeSight;
import gdxrpg.ecs.components.Opacity;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.UndirectedLightSource;
import gdxrpg.ecs.components.position.AbstractPosition;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.MapScenery;
import gdxrpg.utils.MathUtils;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;

public class VisibilitySystem extends IteratingSystem {

	private static final Logger log = LoggerFactory.getLogger(VisibilitySystem.class);

	private BaseGame game;
	private final Family familyToCheck = Family.all(Position.class, Renderable.class).get();

	public VisibilitySystem(Integer priority, BaseGame game)
	{
		super(Family.all(UndirectedLightSource.class, Position.class).get(), priority);

		this.game = game;
	}

	@Override
	public void update(float deltaTime)
	{
		// TODO
//		ImmutableArray<Entity> entities = game.getEntitiesFor(familyToCheck);
//		for (int i = 0; i < entities.size(); i++)
//		{
//			CM.get(entities.get(i), Renderable.class).visibility = 0;
//		}
//		super.update(deltaTime);
	}

	@Override
	protected void processEntity(Entity lightSourceEntity, float deltaTime)
	{
		AbstractPosition lightPos = CM.get(lightSourceEntity, Position.class);
		// Entity player = game.get(PlayerSystem.class).get();
		// if (entity == player)
		// CM.get(player, null)
		// return;
		ImmutableArray<Entity> entities = game.getEntitiesFor(familyToCheck);
		for (int i = 0; i < entities.size(); i++)
		{
			Entity toCheck = entities.get(i);
			Position pos = CM.get(toCheck, Position.class);
			UndirectedLightSource lightSource = CM.get(lightSourceEntity, UndirectedLightSource.class);
			double distance = lightPos.distanceTo(pos);
			if (distance + 1 > lightSource.radius)
			{
				continue;
			}
			Renderable rend = CM.get(toCheck, Renderable.class);
			// rend.visibility = (int) Math.max(0, 255 - Math.round(distance) *
			// Math.rint(radius));
			rend.visibility += 100 * (1 - (distance / lightSource.radius));
			if (log.isTraceEnabled())
				log.trace("New visibility: {}", rend.visibility);
			List<Position> ray = MathUtils.line(lightPos.x, lightPos.y, pos.x, pos.y);
			int previousOpacity = 0;
			for (int j = 0; j < ray.size() - 1; j++)
			{
				Position rayPos = ray.get(j);
				// log.debug("{}", rayPos);
				Set<Entity> entitiesAt = game.get(LevelSystem.class).getEntitiesAt(rayPos, Family.all(Opacity.class, Renderable.class).get());
				int totalOpacityHere = previousOpacity;
				for (Entity rayEntity : entitiesAt)
				{
					totalOpacityHere += CM.get(rayEntity, Opacity.class).opacity;
					CM.get(rayEntity, Renderable.class).visibility -= previousOpacity;
				}
				rend.visibility -= totalOpacityHere;
				previousOpacity = totalOpacityHere;
			}
		}
	}

	// TODO
	public boolean canSee(Entity source, Position to)
	{
		Position from = CM.get(source, Position.class);
		int eyeSightRemaining = CM.get(source, EyeSight.class).eyeSight;
		List<Position> lineTo = from.lineTo(to);
		for (Position pos : lineTo)
		{
			log.debug("POSITION: {}", pos);
			for (Entity ent : game.get(LevelSystem.class).getEntitiesAt(pos,
					Family.one(MapScenery.class).get()))
			{
				int opacity = CM.get(ent, Opacity.class).opacity;
				log.debug("OPACITY: {} : {}", EntitySerializer.dumpTerse(ent), opacity);
//				if (opacity > 0)
//				{
					eyeSightRemaining -= opacity;
//				}
			}
			eyeSightRemaining -= 1;
		}
		return eyeSightRemaining > 0;
	}
}
