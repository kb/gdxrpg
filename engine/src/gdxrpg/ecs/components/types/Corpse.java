package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Renderable;

import java.util.Arrays;

public class Corpse extends TypeComponent {

	public Corpse()
	{
		super(Arrays.asList(
				Corpse.class,
				Renderable.class
				));
	}

}
