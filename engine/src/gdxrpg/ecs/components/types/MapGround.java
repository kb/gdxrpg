package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.BelongsToLevel;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;

import java.util.Arrays;

public class MapGround extends TypeComponent {

	public MapGround()
	{
		super(Arrays.asList(
				MapGround.class,
				Renderable.class,
				Position.class,
				Passable.class,
				BelongsToLevel.class));
	}

}
