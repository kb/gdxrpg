package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Opacity;
import gdxrpg.ecs.components.Renderable;

import java.util.Arrays;


public class Item extends TypeComponent {
	
	public Item()
	{
		super(Arrays.asList(Item.class, Renderable.class, Opacity.class));
	}


}
