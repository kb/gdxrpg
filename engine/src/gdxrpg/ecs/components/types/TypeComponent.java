package gdxrpg.ecs.components.types;

import java.util.List;

import com.badlogic.ashley.core.Component;

public abstract class TypeComponent extends Component {
	
	private final List<Class<? extends Component>> requiredComponents;

	public TypeComponent(List<Class<? extends Component>> requiredComponents)
	{
		this.requiredComponents = requiredComponents;
	}
	
	@Override
	public String toString()
	{
		return "is a " + getClass().getSimpleName();
	}

	public List<Class<? extends Component>> getRequiredComponents()
	{
		return requiredComponents;
	}

}
