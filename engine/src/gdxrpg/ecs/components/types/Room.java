package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.position.Position;

import java.util.Arrays;

/**
 * {@link Extent}
 *
 */
public class Room extends TypeComponent {

	public Room()
	{
		super(Arrays.asList(Room.class, Position.class, Description.class));
	}

}
