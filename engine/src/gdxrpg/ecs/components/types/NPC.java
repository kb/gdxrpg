package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.Behavior;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.EyeSight;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.ExperienceLevel;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.RpgAttributes;
import gdxrpg.ecs.components.Speed;

import java.util.Arrays;

public class NPC extends TypeComponent {

	public NPC()
	{
		super(Arrays.asList(
				NPC.class,
				Animatable.class,
				Behavior.class,
				Busy.class,
				EyeSight.class,
				Description.class,
				ExperienceLevel.class,
				Health.class,
				ItemContainer.class,
				Passable.class,
				Renderable.class,
				RpgAttributes.class,
				Speed.class
				));
	}

}
