package gdxrpg.ecs.components.types;

import java.time.LocalDateTime;
import java.util.Arrays;

public class GameTime extends TypeComponent{

	public GameTime()
	{
		super(Arrays.asList(GameTime.class));
	}
	
	public int turns = 0;
	public int ticks = 0;
	public LocalDateTime dateTime = null;

}
