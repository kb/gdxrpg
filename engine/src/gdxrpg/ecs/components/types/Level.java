package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.components.position.DefaultPlayerSpawnPosition;

import java.util.Arrays;

public class Level extends TypeComponent {

	public Level()
	{
		super(Arrays.asList(
				Level.class,
				UniqueLabel.class,
				DefaultPlayerSpawnPosition.class,
				Extent.class));
	}

}
