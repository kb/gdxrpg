package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;

import java.util.Arrays;

public class Portal extends TypeComponent {

	public Portal()
	{
		super(Arrays.asList(Player.class, Position.class, Renderable.class));
	}
}
