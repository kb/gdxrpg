package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Extent;

import java.util.Arrays;

public class Viewport extends TypeComponent {
	
	public Viewport()
	{
		super(Arrays.asList(
				Viewport.class,
				Extent.class
				));
	}

}
