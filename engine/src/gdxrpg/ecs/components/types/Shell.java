package gdxrpg.ecs.components.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.owlike.genson.annotation.JsonIgnore;

public class Shell extends TypeComponent {
	
	public Shell()
	{
		super(Arrays.asList(Shell.class));
	}
	public String shellName = "";
	public int historyIndex = -1;
	public int cursor = 0;
	public String oldContent = "";
	public final Map<String,String> aliases = new HashMap<>();
	public final List<String> history = new ArrayList<>();
	public final StringBuilder input = new StringBuilder();
	public boolean isInteractive = true;
	@JsonIgnore
	public final LinkedList<Shell> subshells = new LinkedList<>();
}
