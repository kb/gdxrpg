package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.components.position.Position;

import java.util.Arrays;

public class LevelPortal extends TypeComponent{
	
	public LevelPortal()
	{
		super(Arrays.asList(
				LevelPortal.class,
				Position.class,
				UniqueLabel.class,
				Renderable.class
				));
	}

}
