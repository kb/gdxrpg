package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.EyeSight;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.ExperienceLevel;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.RpgAttributes;
import gdxrpg.ecs.components.Speed;
import gdxrpg.ecs.components.position.Position;

import java.util.Arrays;

public class Player extends TypeComponent {

	public Player()
	{
		super(Arrays.asList(Player.class,
				Animatable.class,
				ExperienceLevel.class,
				EyeSight.class,
				RpgAttributes.class,
				Health.class,
				Busy.class,
				Speed.class,
				Position.class,
				Renderable.class,
				ItemContainer.class));
	}

}
