package gdxrpg.ecs.components.types;

import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.Renderable;

import java.util.Arrays;

public class AnimationTile extends TypeComponent{

	public AnimationTile()
	{
		super(Arrays.asList(Renderable.class, Offset.class));
	}
}
