package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class Pickup extends Component {

	public boolean pickup = true;

	public Pickup(boolean pickup)
	{
		this.pickup = pickup;
	}

	@Override
	public String toString()
	{
		return "Pickupable!" + pickup;
	}

}
