package gdxrpg.ecs.components;

import gdxrpg.BaseGame;
import gdxrpg.ecs.components.action.IAction;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class Busy extends Component {

	public final LinkedList<IAction> queue = new LinkedList<>();

	private static final Logger log = LoggerFactory.getLogger(Busy.class);

	private int timeBonus = 0;

	public Busy()
	{
	}

	@Override
	public String toString()
	{
		return "NIH";
//		return String.valueOf(timeLeft) + " moves";
	}


	public boolean finished()
	{
		return queue.isEmpty() || queue.getFirst().isFinished();
	}

	public void tick(int timePassed)
	{
		if (! this.queue.isEmpty()) {
			this.queue.getFirst().incCompleted(timePassed);
		} else {
			this.timeBonus += timePassed;
			
		}
	}

	// TODO
	public void execute(BaseGame game, Entity entity)
	{
		if (! queue.isEmpty()) {
			queue.getFirst().execute(game, entity);
			queue.removeFirst();
		}
	}

	public int getTimeLeft()
	{
		if (! queue.isEmpty()) {
			return queue.getFirst().getCost() - queue.getFirst().getCompleted();
		}
		return 0;
	}


}
