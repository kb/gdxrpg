package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class Utterance extends Component {
	
	public int timeToShow = 5;
	public final String text;
	public final long sourceId;

	public Utterance(Entity source, String text, int timeToShow)
	{
		this.sourceId = source.getId();
		this.text = text;
		this.timeToShow = timeToShow;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s: '%s' [%s]", sourceId, text, timeToShow);
	}

}
