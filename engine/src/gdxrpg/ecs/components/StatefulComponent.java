package gdxrpg.ecs.components;

import java.util.List;
import java.util.Map;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public interface StatefulComponent {

	// Set<String> getStates();

	Map<String, List<Component>> getStateMap();

	String getOldState();

	String getCurrentState();

	void setCurrentState(String newState);

	void setState(Entity parent);

}
