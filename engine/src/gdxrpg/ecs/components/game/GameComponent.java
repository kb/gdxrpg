package gdxrpg.ecs.components.game;

import gdxrpg.BaseGame;

import com.badlogic.ashley.core.Component;
import com.owlike.genson.annotation.JsonIgnore;

// This feels very very hackish
public class GameComponent extends Component {

	@JsonIgnore
	public final BaseGame game;

	@JsonIgnore
	public GameComponent(BaseGame game)
	{
		this.game = game;
	}

}
