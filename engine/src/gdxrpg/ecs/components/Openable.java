package gdxrpg.ecs.components;

public class Openable extends AbstractStatefulComponent {

	public Openable(String currentState)
	{
		createState("OPEN");
		createState("CLOSED");
		setCurrentState(currentState);
	}
}
