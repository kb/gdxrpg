package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class EyeSight extends Component {
	
	public int eyeSight = 10;

	public EyeSight(int eyeSight)
	{
		this.eyeSight = eyeSight;
	}

	@Override
	public String toString()
	{
		return "" + eyeSight;
	}


}
