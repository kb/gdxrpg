package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class TimeToMove extends Component {
	
	private final float timeToMove;
	public float remaining;
	
	public TimeToMove(float timeToMove)
	{
		this.timeToMove = timeToMove;
		this.remaining = timeToMove;
	}

}
