package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class LabelComponent extends Component {

	public final String label;
	
	public LabelComponent(String label)
	{
		this.label = label;
	}
}
