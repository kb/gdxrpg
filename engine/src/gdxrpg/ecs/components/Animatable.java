package gdxrpg.ecs.components;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.ashley.core.Component;

public class Animatable extends Component {
	
	public final Set<Animation> animations = new HashSet<>();

	public void add(Animation build)
	{
		this.animations.add(build);
	}

}
