package gdxrpg.ecs.components;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.components.types.AnimationTile;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class Animation extends Component {

	private final UUID uuid = UUID.randomUUID();
	private static final Logger log = LoggerFactory.getLogger(Animation.class);
	public final List<List<Entity>> frameList;
	private double currentFrame = 0f;
	public final boolean looping;
	public final double fps;

	public Animation(List<List<Entity>> frameList, boolean looping, double fps)
	{
		this.looping = looping;
		this.frameList = frameList;
		this.fps = fps;
		for (List<Entity> frame : frameList)
		{
			for (Entity tile : frame)
			{
				try
				{
					CM.validate(tile, AnimationTile.class);
				} catch (MissingComponentsException e)
				{
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
	}

	public List<Entity> getCurrentFrame()
	{
		double idx = Math.min(frameList.size() - 1, currentFrame);
//		log.debug("curFrame: {}", (int)idx);
		List<Entity> list = frameList.get((int)idx);
		return list;
	}
	public void incCurrentFrame(double d)
	{
//		log.debug("{}", this);
		this.currentFrame += d;
		if (currentFrame >= frameList.size()) {
			if (looping) {
				currentFrame = 0f;
			} else {
				currentFrame = -1;
			}
		}
	}
	public boolean isFinished() {
		return currentFrame < 0;
	}
	
	@Override
	public String toString()
	{
		return "Animation [frameList=" + frameList + ", currentFrame=" + currentFrame + ", looping=" + looping + ", fps=" + fps + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animation other = (Animation) obj;
		if (uuid == null)
		{
			if (other.uuid != null)
				return false;
		}
		else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
