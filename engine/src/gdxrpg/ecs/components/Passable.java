package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class Passable extends Component {

	public boolean passable = false;

	public Passable(boolean passable)
	{
		this.passable = passable;
	}

	@Override
	public String toString()
	{
		return passable ? "YES_PASS": "NO_PASS";
	}

}
