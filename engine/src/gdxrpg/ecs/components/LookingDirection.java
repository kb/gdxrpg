package gdxrpg.ecs.components;

import gdxrpg.ecs.constants.DirectionHorizontal;

import com.badlogic.ashley.core.Component;

public class LookingDirection extends Component {
	
	public DirectionHorizontal direction;
	
	public LookingDirection(DirectionHorizontal direction)
	{
		this.direction = direction;
	}

}
