package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class BelongsToLevel extends Component {
	
	private final String belongsToLevel;

	public BelongsToLevel(String belongsToLevel)
	{
		this.belongsToLevel = belongsToLevel;
	}

	@Override
	public String toString()
	{
		return "part of level " + belongsToLevel;
	}
}
