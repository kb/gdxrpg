package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.owlike.genson.annotation.JsonIgnore;

public class Behavior extends Component {
	
	public final String behavior;
	
	@JsonIgnore
	public BehaviorTree<Entity> btree = null;
	
	public Behavior(String behavior)
	{
		this.behavior = behavior;
	}

}
