package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class ExperienceLevel extends Component {
	
	public ExperienceLevel(int i)
	{
		this.playLevel = i;
	}
	
	public ExperienceLevel()
	{
		playLevel = 1;
	}

	public int playLevel;

}
