package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class RpgAttributes extends Component {
	public int str = 1;
	public int dex = 1;
	public int con = 1;
	public int intel = 1;
	public int wis = 1;
	public int cha = 1;
	
	public RpgAttributes()
	{
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("str=");
		builder.append(str);
		builder.append(", dex=");
		builder.append(dex);
		builder.append(", con=");
		builder.append(con);
		builder.append(", intel=");
		builder.append(intel);
		builder.append(", wis=");
		builder.append(wis);
		builder.append(", cha=");
		builder.append(cha);
		return builder.toString();
	}
}