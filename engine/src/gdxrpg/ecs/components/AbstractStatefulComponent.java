package gdxrpg.ecs.components;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.owlike.genson.annotation.JsonIgnore;

public abstract class AbstractStatefulComponent extends Component implements StatefulComponent {

	private static final Logger log = LoggerFactory.getLogger(AbstractStatefulComponent.class);

	public Map<String, List<Component>> stateMap;

	public String currentState;

	@JsonIgnore
	public String oldState = "";

	@Override
	public String getCurrentState()
	{
		return currentState;
	}

	@Override
	public String getOldState()
	{
		return oldState;
	}

	@Override
	public void setCurrentState(String newState)
	{
		this.oldState = currentState;
		this.currentState = newState;
	}

	@Override
	public void setState(Entity parent)
	{
//		log.debug(EntitySerializer.genson.serialize(this));
		if (oldState != null && !getCurrentState().equals(getOldState()))
		{
			for (Component comp : getStateMap().get(getOldState()))
			{
				parent.remove(comp.getClass());
			}
		}
		for (Component comp : getStateMap().get(getCurrentState()))
		{
			parent.add(comp);
		}
		oldState = currentState;
	}

	protected void createState(String state)
	{

	}

	@Override
	public Map<String, List<Component>> getStateMap()
	{
		return stateMap;
	}
}
