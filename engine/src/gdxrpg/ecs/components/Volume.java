package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

/**
 * Volume in cubic centimeters.
 * 
 * @author kba
 *
 */
public class Volume extends Component {

	public int volume;
	
	public Volume(int volume)
	{
		this.volume = volume;
	}
	
	@Override
	public String toString()
	{
		return volume + "ccm^3";
	}
}
