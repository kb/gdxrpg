package gdxrpg.ecs.components;

import gdxrpg.ecs.CM;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class ItemContainer extends Component {
	
	public int weightCapacity = 1000;
	public int volumeCapactiy = 1000;
	public final List<Entity> contents = new ArrayList<>();
	
	public ItemContainer()
	{
	}

	public ItemContainer(int weightCapacity, int volumeCapactiy)
	{
		this.weightCapacity = weightCapacity;
		this.volumeCapactiy = volumeCapactiy;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Entity e  : contents)
		{
			sb.append(CM.getTypeComponent(e).toString());
			sb.append("\n");
		}
		sb.append("]");
		return sb.toString();
	}
	
	
}
