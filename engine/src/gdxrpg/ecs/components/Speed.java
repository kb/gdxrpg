package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

// TODO
public class Speed extends Component {

//	public final int speed = 100;
	public final int speed;

	public Speed(int speed)
	{
		this.speed = speed;
	}
	
	@Override
		public String toString()
		{
			return "SPEED: " + speed;
		}

}
