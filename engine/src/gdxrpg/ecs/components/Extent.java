package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class Extent extends Component {

	public int height;
	public int width;

	public Extent(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	
	@Override
	public String toString()
	{
		return String.format("<%sx%s>", width, height);
	}

}
