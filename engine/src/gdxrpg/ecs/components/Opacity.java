package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class Opacity extends Component {
	public static final int MAX_VALUE = 100;
	public static final int TRANSPARENT = 0;
	public static final int OPAQUE = 100;
	public int opacity = 0;
	
	@Override
	public String toString()
	{
		return "Opacity: " + opacity;
	}

}
