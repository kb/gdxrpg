package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class UndirectedLightSource extends Component{

	public int radius;

	public UndirectedLightSource(int radius)
	{
		this.radius = radius;
	}

	
	@Override
	public String toString()
	{
		return "Light: r="+radius;
	}


}
