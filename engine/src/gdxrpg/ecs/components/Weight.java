package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

/**
 * Weight in grams.
 * 
 * @author kba
 *
 */
public class Weight extends Component {
	
	public int weight;
	
	public Weight(int weight)
	{
		this.weight = weight;
	}
	
	@Override
	public String toString()
	{
		return weight + "g";
	}

}
