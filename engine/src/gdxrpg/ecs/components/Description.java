package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class Description extends Component {
	
	public String name;
	public String description = "Nothing special about it";
	
	public Description(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return "Description [name=" + name + "]";
	}

}
