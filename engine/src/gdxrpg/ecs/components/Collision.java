package gdxrpg.ecs.components;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.CollisionType;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

// TODO remove nesting of entities
public class Collision extends Component {

	public final Entity source;
	public final Entity target;
	public final CollisionType type;
	private final Position pos;

	public Collision(Entity source, CollisionType type, Position pos)
	{
		this.source = source;
		this.type = type;
		this.pos = pos;
		this.target = null;
	}

	public Collision(Entity source, CollisionType type, Entity target, Position pos)
	{
		this.source = source;
		this.target = target;
		this.type = type;
		this.pos = pos;
	}

	@Override
	public String toString()
	{
		if (target != null)
			return String.format("%s@%s between ??@%s and ??@%s",
					type,
					pos,
					CM.get(source, Position.class),
					CM.get(target, Position.class)
					);
		else
			return String.format("%s@%s for ??@%s",
					type,
					pos,
					CM.get(source, Position.class)
					);
	}

}
