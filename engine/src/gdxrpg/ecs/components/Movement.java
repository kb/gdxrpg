package gdxrpg.ecs.components;

import gdxrpg.ecs.constants.DirectionHorizontal;

//
import com.badlogic.ashley.core.Component;

public class Movement extends Component {
    public double dx;
    public double dy;
    public Movement(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }
	public static Movement fromDirection(DirectionHorizontal dir)
	{
		return new Movement(dir.dx, dir.dy);
	}
	
	@Override
	public String toString()
	{
		return String.format("^%s>%s", dy, dx);
	}
	public boolean is(DirectionHorizontal dir)
	{
		return this.dx == dir.dx && this.dy == dir.dy;
	}
	public boolean is(float dx, float dy)
	{
		return roundDX() == dx && roundDY() == dy;
	}
	public boolean is(int dx, int dy)
	{
		return this.dx == dx && this.dy == dy;
	}
	private int roundDX()
	{
		return (int) Math.round(dx);
	}
	private int roundDY()
	{
		return (int) Math.round(dy);
	}
	public boolean isMoving()
	{
		return ! is(DirectionHorizontal.HERE);
	}
	public void stopMoving()
	{
		this.dx = 0;
		this.dy = 0;
	}
	public void set(Movement movement)
	{
		this.dx = movement.dx;
		this.dy = movement.dy;
		
	}
}