package gdxrpg.ecs.components.position;

import gdxrpg.ecs.components.Offset;
import gdxrpg.utils.MathUtils;

import java.util.List;

import com.badlogic.ashley.core.Component;

public abstract class AbstractPosition extends Component {

	public final double x;
	public final double y;

    public AbstractPosition(double x, double y) {
    	this.x = x;
    	this.y = y;
    }
    
    public AbstractPosition(AbstractPosition oldPos) {
    	this.x = oldPos.x;
    	this.y = oldPos.y;
    }

	public double roundDistance(AbstractPosition other)
	{
		double dx = Math.abs(this.x - other.x);
		double dy = Math.abs(this.y - other.y);
		return (int) Math.sqrt(dx * dx + dy * dy);
	}
	public double distanceTo(AbstractPosition other)
	{
		double dx = Math.abs(this.x - other.x);
		double dy = Math.abs(this.y - other.y);
		return Math.sqrt(dx * dx + dy * dy);
	}
	public String pathTo(AbstractPosition other)
	{
		int dx = this.roundX() - other.roundX();
		int dy = this.roundY() - other.roundY();
		StringBuilder sb = new StringBuilder();
		if (dx == 0 && dy == 0)
			sb.append("HERE");
		if (dy != 0) {
			sb.append(Math.abs(dy));
			sb.append(dy > 0 ? "S" : "N");
			sb.append(" ");
		}
		if (dx != 0) {
			sb.append(Math.abs(dx));
			sb.append(dx > 0 ? "W" : "E");
		}
		return sb.toString();
	}

	public List<Position> lineTo(AbstractPosition to)
	{
		List<Position> ret = MathUtils.line(x, y, to.x, to.y);
		if (ret.size() > 0)
			ret.remove(0);
		if (ret.size() > 0)
			ret.remove(ret.size() - 1);
		return ret;
	}

	public int roundY()
	{
		return (int) Math.round(y);
	}

	public int roundX()
	{
		return (int) Math.round(x);
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + roundX();
		result = prime * result + roundY();
		return result;
	}

	public boolean samePositionAs(AbstractPosition obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
//		if (getClass() != obj.getClass())
//			return false;
		AbstractPosition other = (AbstractPosition) obj;
		if (roundX() != other.roundX())
			return false;
		if (roundY() != other.roundY())
			return false;
		return true;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return samePositionAs((AbstractPosition) obj);
	}
	
	public Position minusOffset(Offset offset) {
		return new Position(x - offset.dx, y - offset.dy);
	}
	public Position plusOffset(Offset offset) {
		return new Position(x + offset.dx, y + offset.dy);
	}

	@Override
    public String toString() {
//		return String.format("%s<%s/%s>", getClass().getSimpleName(), roundX(), roundY());
		return String.format("%s<%s/%s>", "", roundX(), roundY());
    }
	
	public int getIndex(int levelWidth)
	{
		return roundX() + roundY() * levelWidth;
	}

//	public float x()
//	{
//		return x;
//	}
//	public float y()
//	{
//		return y;
//	}

}