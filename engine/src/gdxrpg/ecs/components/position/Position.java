package gdxrpg.ecs.components.position;

import com.badlogic.ashley.core.Component;

public final class Position extends AbstractPosition {

	public Position(double x, double y)
	{
		super(x, y);
	}

	public static Component fromPosition(AbstractPosition fromPos)
	{
		return new Position(fromPos.x, fromPos.y);
	}

}
