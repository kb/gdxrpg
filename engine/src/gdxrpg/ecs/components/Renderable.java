package gdxrpg.ecs.components;

import gdxrpg.ecs.constants.MyColor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
//import com.badlogic.gdx.graphics.Pixmap;
//import com.badlogic.gdx.graphics.Pixmap.Format;
//import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
//import com.badlogic.gdx.graphics.g2d.BitmapFont.Glyph;
//import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Renderable extends Component {

	private static final String YELLOW = "#ffff0000";
	// public final TextureRegion texture;
	Logger log = LoggerFactory.getLogger(Renderable.class);

	public String fgChar;
	// public final int byteLength = 1;
	public String fgColor;
	public int visibility = 255;
	public String bgColor = "000000";
	public String filteredColor;
	private String filteredCharacter;

	public Renderable(String ch)
	{
		this(ch, MyColor.YELLOW);
	}

	public Renderable(String ch, String color)
	{
		this.fgChar = ch;
		this.fgColor = color;
		this.filteredColor = color;
		this.filteredCharacter = ch;
	}
	public Renderable(String ch, String color, String bgColor)
	{
		this.fgChar = ch;
		this.fgColor = color;
		this.bgColor = bgColor;
		this.filteredColor = color;
		this.filteredCharacter = ch;
	}
	
	public static Renderable fromRenderable(Renderable toClone)
	{
		Renderable rend = new Renderable(toClone.fgChar, toClone.fgColor);
		rend.bgColor = toClone.bgColor;
		rend.visibility = toClone.visibility;
		return rend;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Renderable other = (Renderable) obj;
		if (visibility != other.visibility)
			return false;
		if (fgChar == null)
		{
			if (other.fgChar != null)
				return false;
		}
		else if (!fgChar.equals(other.fgChar))
			return false;
		if (fgColor == null)
		{
			if (other.fgColor != null)
				return false;
		}
		else if (!fgColor.equals(other.fgColor))
			return false;
		return true;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + visibility;
		result = prime * result + ((fgChar == null) ? 0 : fgChar.hashCode());
		result = prime * result + ((fgColor == null) ? 0 : fgColor.hashCode());
		return result;
	}

	@Override
	public String toString()
	{
		return String.format("'%s' #%s", fgChar, fgColor);
	}

}