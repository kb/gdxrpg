package gdxrpg.ecs.components;

import gdxrpg.ecs.components.position.Position;

import com.badlogic.ashley.core.Component;

public class Offset extends Component {
	
	public int dx = 0;
	public int dy = 0;
	
	public Offset(int dx, int dy)
	{
		this.dx = dx;
		this.dy = dy;
	}

	public static Offset fromOffset(Offset toClone)
	{
		return new Offset(toClone.dx, toClone.dy);
	}
	public static Offset fromPositions(Position source, Position target)
	{
		return new Offset(target.roundX() - source.roundX(), target.roundY() - source.roundY());
	}

	@Override
	public String toString() {
		return String.format("+%s+%s", dx, dy);
	}

}
