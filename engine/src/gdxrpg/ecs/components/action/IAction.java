package gdxrpg.ecs.components.action;

import gdxrpg.BaseGame;

import com.badlogic.ashley.core.Entity;

public interface IAction {

	public int getCost();
	
	public int getCompleted();

	void execute(BaseGame game, Entity entity);

	public void incCompleted(int timePassed);

	public boolean isFinished();

}