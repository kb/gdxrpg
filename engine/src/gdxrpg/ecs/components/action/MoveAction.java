package gdxrpg.ecs.components.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Collision;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.systems.CollisionDetectionSystem;

public class MoveAction extends AbstractAction {
	
	private static final Logger log = LoggerFactory.getLogger(MoveAction.class);

	public final Position targetPosition;

	public MoveAction(int cost, Position targetPosition)
	{
		super(cost);
		this.targetPosition = targetPosition;
	}

	@Override
	public void execute(BaseGame game, Entity entity)
	{
		log.debug("Adding target Position!");
		// Collision detection
		Entity collision = game.get(CollisionDetectionSystem.class).checkForCollision(entity, targetPosition);
		if (null != collision) {
			log.debug("Collision occured: {}", CM.get(collision, Collision.class));
			game.addEntity(collision);
		} else {
			entity.add(targetPosition);
		}
	}

	@Override
	public void incCompleted(int timePassed)
	{
		this.completed += timePassed;
	}

	@Override
	public boolean isFinished()
	{
		return completed >= cost;
	}

}
