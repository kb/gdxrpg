package gdxrpg.ecs.components.action;

import com.badlogic.ashley.core.Component;

public abstract class AbstractAction extends Component implements IAction {

	public final int cost;
	public int completed;

	public AbstractAction(int cost)
	{
		this.cost = cost;
	}
	
	@Override
	public int getCompleted()
	{
		return completed;
	}

	@Override
	public int getCost()
	{
		return cost;
	}

}
