package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;

public class UniqueLabel extends Component {
	
	public final String uniqueLabel;

	public UniqueLabel(String uniqueLabel)
	{
		this.uniqueLabel = uniqueLabel;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uniqueLabel == null) ? 0 : uniqueLabel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniqueLabel other = (UniqueLabel) obj;
		if (uniqueLabel == null) {
			if (other.uniqueLabel != null)
				return false;
		} else if (!uniqueLabel.equals(other.uniqueLabel))
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return String.format("LABEL:'%s'", uniqueLabel);
	}
	
}
