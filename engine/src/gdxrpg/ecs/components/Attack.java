package gdxrpg.ecs.components;

import gdxrpg.ecs.constants.AttackType;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class Attack extends Component {

	public final AttackType type;
	public final Entity source;
	public final Entity target;

	public Attack(AttackType type, Entity source, Entity target)
	{
		this.type = type;
		this.source = source;
		this.target = target;
	}

}