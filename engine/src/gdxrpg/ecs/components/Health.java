package gdxrpg.ecs.components;

import com.badlogic.ashley.core.Component;


public class Health extends Component {
	
	public int maxHealth = 100;
	public int health;
	public boolean dead;
	
	public Health(int health)
	{
		this.health = health;
	}

	public Health(int health, int maxHealth)
	{
		this.health = health;
		this.maxHealth = maxHealth;
	}

	public float percentage()
	{
		return ((float)health) / maxHealth;
	}
	
	@Override
	public String toString()
	{
		return String.format("HEALTH: %s/%s", health, maxHealth);
	}

}
