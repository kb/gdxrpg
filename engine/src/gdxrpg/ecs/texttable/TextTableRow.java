package gdxrpg.ecs.texttable;

import gdxrpg.ecs.texttable.TextTable.TextTableAlign;
import gdxrpg.ecs.texttable.TextTableCell.TextTableCellBuilder;

import java.util.ArrayList;
import java.util.List;

public class TextTableRow {
	private final List<TextTableCell> cells;
	private final TextTableAlign rowExpand;

	public TextTableRow(List<TextTableCell> cells, TextTableAlign rowExpand)
	{
		this.cells = cells;
		this.rowExpand = rowExpand;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (TextTableCell cell : getCells())
		{
			sb.append(cell.toString());
		}
		return sb.toString();
	}

	public List<TextTableCell> getCells()
	{
		return cells;
	}

	public TextTableAlign getRowExpand()
	{
		return rowExpand;
	}

	protected static class TextTableRowBuilder {
		private final List<TextTableCellBuilder> cells = new ArrayList<>();
		private TextTableAlign expand;

		public TextTableCellBuilder cell(String contents)
		{
			TextTableCellBuilder cellBuilder = TextTableCell.builder();
			cellBuilder.contents(contents);
			this.cells.add(cellBuilder);
			return cellBuilder;
		}

		public TextTableRowBuilder expand(TextTableAlign expand)
		{
			this.expand = expand;
			return this;
		}

		public TextTableRow build()
		{
			List<TextTableCell> ret = new ArrayList<>();
			for (TextTableCellBuilder cellBuilder : this.cells) {
				ret.add(cellBuilder.build());
			}
			 return new TextTableRow(ret, expand);
		}
	}
}
