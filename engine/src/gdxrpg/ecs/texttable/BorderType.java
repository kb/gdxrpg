package gdxrpg.ecs.texttable;

public enum BorderType {
	ASCII(new String[] { "+", "-", "+", "|", "+", "-", "+", "|" }),
	SINGLE(new String[] { "\u250c", "\u2500", "\u2510", "\u2502", "\u2518", "\u2500", "\u2514", "\u2502" });
	public final String nw;
	public final String n;
	public final String ne;
	public final String e;
	public final String se;
	public final String s;
	public final String sw;
	public final String w;

	private BorderType(String[] chars)
	{
		nw = chars[0];
		n = chars[1];
		ne = chars[2];
		e = chars[3];
		se = chars[4];
		s = chars[5];
		sw = chars[6];
		w = chars[7];
	}
}