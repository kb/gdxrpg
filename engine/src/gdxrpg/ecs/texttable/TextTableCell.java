package gdxrpg.ecs.texttable;

import gdxrpg.ecs.texttable.TextTable.TextTableAlign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextTableCell {
	
	private static final Logger log = LoggerFactory.getLogger(TextTableCell.class);

	public final String contents;
	public final TextTableAlign align;
	public final String fgColor;
	public final String bgColor;
	
	public static TextTableCellBuilder builder() {
		return new TextTableCellBuilder();
	}

	private TextTableCell(String contents, TextTableAlign align, String fgColor, String bgColor)
	{
		this.contents = contents;
		this.align = align;
		this.fgColor = fgColor;
		this.bgColor = bgColor;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(contents);
		sb.append("[");
		sb.append(align).append(",");
		sb.append(fgColor).append(",");
		sb.append(bgColor);
		sb.append("]");
		sb.append("\t");
		return sb.toString();
	}

	public static class TextTableCellBuilder {

		private String contents;
		private TextTableAlign align = TextTableAlign.Left;
		private String fgColor = "ffffff";
		private String bgColor = null;
		
		public TextTableCellBuilder contents(String contents)
		{
			this.contents = contents;
			return this;
		}

		public TextTableCellBuilder align(TextTableAlign align)
		{
			this.align = align;
			return this;
		}

		public TextTableCellBuilder fgColor(String fgColor)
		{
			this.fgColor = fgColor;
			return this;
		}

		public TextTableCellBuilder bgColor(String bgColor)
		{
			this.bgColor = bgColor;
			return this;
		}

		public TextTableCell build()
		{
			return new TextTableCell(contents, align, fgColor, bgColor);
		}

	}

}
