package gdxrpg.ecs.texttable;

import gdxrpg.ecs.texttable.TextTableCell.TextTableCellBuilder;
import gdxrpg.ecs.texttable.TextTableRow.TextTableRowBuilder;

import java.util.ArrayList;
import java.util.List;

public class TextTable {
	
	public enum TextTableAlign {
		None,
		Left,
		Right,
		Center,
		Fill,
	}
	
	public final static TextTable EMPTY = builder().build();
	
	private int selectedRow;
	private final List<TextTableRow> rows;
	
	private TextTable(List<TextTableRow> table)
	{
		this.rows = table;
	}
	public List<TextTableRow> getRows()
	{
		return rows;
	}

	public static TextTable.TextTableBuilder builder() {
		return new TextTableBuilder();
	}
	
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (TextTableRow row : rows) {
			sb.append(row.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
	public int getSelectedRow()
	{
		return selectedRow;
	}
	public void setSelectedRow(int selectedRow)
	{
		this.selectedRow = selectedRow;
	}
	public static class TextTableBuilder {
		
		private final List<TextTableRowBuilder> rowBuilders = new ArrayList<>();
		private TextTableRowBuilder currentRowBuilder = null;

		public TextTableRowBuilder row()
		{
			this.currentRowBuilder = new TextTableRowBuilder(); 
			this.rowBuilders.add(currentRowBuilder);
			return currentRowBuilder;
		}

		public TextTableCellBuilder cell(String contents) {
			return currentRowBuilder.cell(contents);
		}
		
		public TextTable build() {
			List<TextTableRow> ret = new ArrayList<>();
			for (TextTableRowBuilder rowBuilder : rowBuilders) {
				ret.add(rowBuilder.build());
			}
			return new TextTable(ret);
		}
	}
}
