package gdxrpg.ecs;

import gdxrpg.ecs.Exceptions.MissingComponentsException;
import gdxrpg.ecs.components.types.TypeComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;

public class CM {

	private static final Logger log = LoggerFactory.getLogger(CM.class);
	private final static Map<Class<? extends TypeComponent>, TypeComponent> typeSingletonCache = new HashMap<>();

	public final static List<Class<? extends TypeComponent>> typeComponents = new ArrayList<>();
	public final static Map<String, Class<? extends Component>> strToComponentClass = new HashMap<>();
	static
	{
		for (Class<? extends TypeComponent> clazz : new Reflections("gdxrpg.ecs.components.types").getSubTypesOf(TypeComponent.class))
		{
			CM.typeComponents.add(clazz);
			CM.strToComponentClass.put(clazz.getSimpleName(), clazz);
		}
		for (Class<? extends Component> clazz : new Reflections("gdxrpg.ecs.components").getSubTypesOf(Component.class))
		{
			CM.strToComponentClass.put(clazz.getSimpleName(), clazz);
		}
	}

	public static final void ensure(Entity entity, Class<? extends Component> type) throws MissingComponentsException
	{
		ensure(entity, Arrays.<Class<? extends Component>> asList(type));
	}

	public static final void ensure(Entity entity, List<Class<? extends Component>> types) throws MissingComponentsException
	{
		List<String> missing = new ArrayList<>();
		for (Class<? extends Component> type : types)
		{
			if (!ComponentMapper.getFor(type).has(entity))
			{
				missing.add(type.getSimpleName());
			}
		}
		if (missing.size() > 0)
		{
			throw Exceptions.missingComponents(entity, missing);
		}
	}


	public static Class<? extends Component> fromSimpleName(String typeString) throws ClassNotFoundException
	{
		typeString = typeString.substring(0, 1).toUpperCase() + typeString.substring(1);
		if (strToComponentClass.containsKey(typeString)) {
			return strToComponentClass.get(typeString);
		} else {
			throw new ClassNotFoundException(typeString);
		}
	}

	public static final <T extends Component> T get(Entity entity, Class<T> type)
	{
		return ComponentMapper.getFor(type).get(entity);
	}

	public static final Class<? extends TypeComponent> getTypeComponent(Entity entity)
	{
		for (Class<? extends TypeComponent> typeClass : CM.typeComponents)
		{
			if (has(entity, typeClass))
			{
				return typeClass;
			};
		}
		return TypeComponent.class;
	}

	@SafeVarargs
	public static final boolean has(Entity entity, Class<? extends Component>... types)
	{
		for (Class<? extends Component> type : types)
		{
			if (!ComponentMapper.getFor(type).has(entity))
			{
				return false;
			}
		}
		return true;
	}

	//
	public static void validate(Entity entity, Class<? extends TypeComponent> type) throws MissingComponentsException
	{
		if (!typeSingletonCache.containsKey(type))
		{
			try
			{
				typeSingletonCache.put(type, type.newInstance());
			} catch (InstantiationException | IllegalAccessException e)
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		ensure(entity, typeSingletonCache.get(type).getRequiredComponents());
	}

}
