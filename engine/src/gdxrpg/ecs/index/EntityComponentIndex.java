package gdxrpg.ecs.index;

import java.util.Collection;
import java.util.Set;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;

@Deprecated
public interface EntityComponentIndex<K extends Component, T> {

	T get(K key);
	
	Set<Entity> getFor(Family family);

	Set<Entity> getWith(K key, Family family);

	boolean has(K key);

	void put(K key, Entity entity);

	int size();

	Collection<T> values();

}