package gdxrpg.ecs;

import gdxrpg.BaseGame;
import gdxrpg.ecs.components.Description;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.GameMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.owlike.genson.Context;
import com.owlike.genson.Converter;
import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.owlike.genson.stream.ObjectReader;
import com.owlike.genson.stream.ObjectWriter;

public class EntitySerializer {

	private static final Logger log = LoggerFactory.getLogger(EntitySerializer.class);

	public static final Genson genson = new GensonBuilder()
			.exclude("log")
			.exclude("requiredComponents")
			.withConverters(new EntityConverter())
			.withConverters(new LocalDateTimeConverter())
			.useClassMetadata(true)
			.useRuntimeType(true)
			.useIndentation(true)
			.useConstructorWithArguments(true)
			.create();

	private static Entity componentListToEntity(List<Component> compList)
	{
		Entity entity = new Entity();
		for (Component comp : compList)
		{
			entity.add(comp);
		}
		return entity;
	}

	public static String dump(Entity e, Set<Class<? extends Component>> filtered)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Entity #").append(e.getId()).append("\n");
		for (Component comp : e.getComponents())
		{
			if (filtered == null || filtered.contains(comp.getClass()))
			{
				sb.append("\t").append(comp.getClass().getSimpleName());
				sb.append(":\t").append(comp.toString()).append("\n");
			}
		}
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public static String dump(Entity e, Class<? extends Component>...classes)
	{
		HashSet<Class<? extends Component>> filtered = new HashSet<Class<? extends Component>>();
		for (Class<? extends Component> clazz : classes) {
			filtered.add(clazz);
		}
		return dump(e, filtered);
	}

	public static String dump(Entity e)
	{
		return dump(e, (Set<Class<? extends Component>>)null);
	}
	
	public static String dumpTerse(Entity e)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(CM.getTypeComponent(e).getSimpleName());
		sb.append("@");
		sb.append(e.getId());
		if (CM.has(e,  Description.class)) {
			sb.append(" ");
			sb.append("\"");
			sb.append(CM.get(e, Description.class).name);
			sb.append("\"");
		}
		if (CM.has(e, Position.class))
		{
			sb.append(" ");
			sb.append(CM.get(e, Position.class));
		}
		return sb.toString();
	}

	private static List<Component> entityToComponentList(Entity e)
	{
		Component[] array = e.getComponents().toArray(Component.class);
		List<Component> decompiledEntity = Arrays.asList(array);
		return decompiledEntity;
	}

	public static Entity fromJSON(String json)
	{
		Entity ret = new Entity();
		List<Component> components = EntitySerializer.genson.deserialize(json, new GenericType<List<Component>>() {
		});
		for (Component component : components)
		{
			ret.add(component);
		}
		// log.debug("{}", dumpEntityInfo(ret));
		return ret;
	}

	public static void loadGameFromFile(BaseGame game, String loadPath)
	{
		try (InputStream inputStream = Files.newInputStream(Paths.get(loadPath));
				GZIPInputStream in = new GZIPInputStream(inputStream))
		{
			SaveGame saveGame = EntitySerializer.genson.deserialize(in, SaveGame.class);
			in.close();
			inputStream.close();

			game.removeAllEntities();
			for (List<Component> compList : saveGame.activeEntities)
			{
				game.addEntity(componentListToEntity(compList));
			}

			game.getInactiveEntities().clear();
			for (Entry<String, List<List<Component>>> kv : saveGame.entityCache.entrySet())
			{
				game.getInactiveEntities().put(kv.getKey(), new ArrayList<Entity>());
				for (List<Component> compList : kv.getValue())
				{
					game.getInactiveEntities().get(kv.getKey()).add(componentListToEntity(compList));
				}
			}

			game.getMessages().clear();
			game.getMessages().addAll(saveGame.messages);

			game.modeStack.clear();
			game.modeStack.addAll(saveGame.modeStack);

			game.setProcessing(false);
		} catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static String saveGame(BaseGame game)
	{
		Stack<GameMode> modeStack = game.modeStack;
		boolean isProcessing = game.isProcessing();
		List<String> messages = new ArrayList<>(game.getMessages());
		List<List<Component>> activeEntityList = new ArrayList<>();
		for (Entity e : game.getEntities())
		{
			activeEntityList.add(entityToComponentList(e));
		}
		Map<String, List<List<Component>>> entityCache = new HashMap<>();
		for (Entry<String, List<Entity>> kv : game.getInactiveEntities().entrySet())
		{
			List<List<Component>> cachedEntityList = new ArrayList<>();
			for (Entity e : kv.getValue())
			{
				cachedEntityList.add(entityToComponentList(e));
			}
			entityCache.put(kv.getKey(), cachedEntityList);
		}

		SaveGame saveGame = new SaveGame(isProcessing, modeStack, activeEntityList, entityCache, messages);
		try
		{
			return genson.serialize(saveGame);
		} catch (Exception e1)
		{
			// e1.printStackTrace();
			throw new RuntimeException(e1);
		}
	}

	public static void saveGameToFile(BaseGame game, String savePath)
	{
		try (
				OutputStream outputStream = Files.newOutputStream(Paths.get(savePath));
				GZIPOutputStream out = new GZIPOutputStream(outputStream))
		{
			String str = saveGame(game);
			IOUtils.write(str, out);
		} catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	static String toJSON(Component[] array)
	{
		return EntitySerializer.genson.serialize(array);
	}

	public static String toJSON(Entity entity)
	{
		return toJSON(entity.getComponents().toArray());
	}
	
	static class LocalDateTimeConverter implements Converter<LocalDateTime>
	{

		@Override
		public void serialize(LocalDateTime object, ObjectWriter writer, Context ctx) throws Exception
		{
			writer.beginObject();
			writer.writeName("localDateTime");
			writer.writeString(object.toString());
			writer.endObject();
		}

		@Override
		public LocalDateTime deserialize(ObjectReader reader, Context ctx) throws Exception
		{
			LocalDateTime ret = null;
			reader.beginObject();
			while (reader.hasNext()) {
				reader.next();
				if (reader.name().equals("localDateTime")) {
					ret = LocalDateTime.parse(reader.valueAsString(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
					log.error("xxx\nxxxx\n{} parsed as {}", reader.valueAsString(), ret);
				}
				else
				{
					reader.skipValue();
				}
			}
			reader.endObject();
			return ret;
		}
		
	}

	static class EntityConverter implements Converter<Entity>
	{

		@Override
		public void serialize(Entity object, ObjectWriter writer, Context ctx) throws Exception
		{
		    writer.beginObject();
		    writer.writeName("components");
		    writer.beginArray();
		    for (Component component: object.getComponents()) {
		    	ctx.genson.serialize(component, writer, ctx);
		    }
		    writer.endArray();
		    writer.endObject();
		}

		@Override
		public Entity deserialize(ObjectReader reader, Context ctx) throws Exception
		{
			Entity object = new Entity();
		    reader.beginObject();
		    while (reader.hasNext()) {
		    	reader.next();
		    	if ("components".equals(reader.name())) {
		    		List<Component> listOfComponents = ctx.genson.deserialize(new GenericType<List<Component>>() {}, reader,ctx);
		    		for (Component component : listOfComponents) {
		    			object.add(component);
					}
		    	} else {
		    		reader.skipValue();
		    	}
		    }
		    reader.endObject();
			return object;
		}
		
	}
}
