package gdxrpg;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

public interface PartialEngine {
	
	ImmutableArray<Entity> getEntities();

	ImmutableArray<Entity> getEntitiesFor(Family family);
	
	void update();
	
	Entity addEntity(Entity entity);
	
	void removeAllEntities();
	
	<K extends EntitySystem> K get(Class<K> clazz);

	void removeEntity(Entity e);

	Entity getEntity(long sourceId);

}
