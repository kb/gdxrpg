package gdxrpg;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Queue;
import java.util.Stack;

import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.utils.Array;
import com.google.common.collect.EvictingQueue;

import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.game.GameComponent;
import gdxrpg.ecs.constants.GameMode;
import gdxrpg.ecs.constants.Sounds;
import gdxrpg.ecs.event.GameModeChangeListener;
import gdxrpg.ecs.event.GameSaveListener;
import gdxrpg.ecs.systems.ActionSystem;
import gdxrpg.ecs.systems.AnimationSystem;
import gdxrpg.ecs.systems.AttackSystem;
import gdxrpg.ecs.systems.BehaviorSystem;
import gdxrpg.ecs.systems.CollisionDetectionSystem;
import gdxrpg.ecs.systems.CollisionHandlingSystem;
import gdxrpg.ecs.systems.DialogManager;
import gdxrpg.ecs.systems.FpsCounterSystem;
import gdxrpg.ecs.systems.HelpSystem;
import gdxrpg.ecs.systems.ItemSystem;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.MovementSystem;
import gdxrpg.ecs.systems.OpenCloseSystem;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.systems.RuleSystem;
import gdxrpg.ecs.systems.SelectionManager;
import gdxrpg.ecs.systems.ShellSystem;
import gdxrpg.ecs.systems.ViewportSystem;
import gdxrpg.ecs.systems.VisibilitySystem;
import gdxrpg.utils.GameConfig;
import gdxrpg.utils.GameMetadata;

public abstract class BaseGame implements PartialEngine {

	private static final Logger log = LoggerFactory.getLogger(BaseGame.class);

	private final Path gameDirectory;
	private final GameMetadata metadata;
	private final GameConfig config;
	private final Map<String, List<Entity>> inactiveEntities = new HashMap<>();
	private Queue<String> messages = EvictingQueue.create(100);
	private boolean processing = true;
	private MainInputHandler inputCommandProcessor;
	private boolean playerTurn = false;
	private final InterruptableEngine engine = new InterruptableEngine();

	private List<GameModeChangeListener> gameModeChangeListeners = new ArrayList<>();
	private List<GameSaveListener> gameSaveListeners = new ArrayList<>();
	private Queue<Runnable> beforeUpdateTasks = new LinkedList<>();
	private Queue<Runnable> afterUpdateTasks = new LinkedList<>();

	public final Stack<GameMode> modeStack = new Stack<>();
	private List<Class<? extends EntitySystem>> toggleableSystems = Arrays.asList(
			BehaviorSystem.class,
			CollisionDetectionSystem.class,
			CollisionHandlingSystem.class,
			ActionSystem.class,
			MovementSystem.class
			);
	private List<Class<? extends EntitySystem>> systems = Arrays.asList(
			OpenCloseSystem.class,
			LevelSystem.class,
			CollisionDetectionSystem.class,
			CollisionHandlingSystem.class,
			MovementSystem.class,
			ViewportSystem.class,
			ItemSystem.class,
			VisibilitySystem.class,
			BehaviorSystem.class,
			ActionSystem.class,

			AnimationSystem.class,
			PlayerSystem.class,
			FpsCounterSystem.class,
			AttackSystem.class,
			DialogManager.class,
			SelectionManager.class,
			ShellSystem.class,
			RuleSystem.class,
			HelpSystem.class
			);

	public BaseGame(Path gameDir)
	{
		if (!Files.isDirectory(gameDir))
		{
			throw new RuntimeException("gameDir must be a directory");
		}
		gameDir = gameDir.toAbsolutePath();
		this.gameDirectory = gameDir;
		try
		{
			// load metadata
			Properties metadataProps = new Properties();
			metadataProps.load(Files.newInputStream(Paths.get(this.getGameDirectory().toString(), "metadata.properties")));
			this.metadata = ConfigFactory.create(GameMetadata.class, metadataProps);
			log.info("Metadata: {}", metadata);

			// load config
			ConfigFactory.setProperty("metadata.id", metadata.id());
			ConfigFactory.setProperty("metadata.dir", getGameDirectory().toString());
			this.config = ConfigFactory.create(GameConfig.class);
			log.info("Config: {}", config);
		} catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void addAfterUpdateTask(Runnable r)
	{
		this.afterUpdateTasks.add(r);
	}

	public void addBeforeUpdateTask(Runnable r)
	{
		this.beforeUpdateTasks.add(r);
	}

	public void addGameModeChangeListener(GameModeChangeListener listener)
	{
		this.gameModeChangeListeners.add(listener);
	}

	public void addGameSaveListener(GameSaveListener listener)
	{
		this.gameSaveListeners.add(listener);
	}

	public GameConfig getConfig()
	{
		return config;
	}

	public GameMetadata getMetadata()
	{
		return metadata;
	}

	public Path getGameDirectory()
	{
		return gameDirectory;
	}

	public void start()
	{
		log.debug("Adding iterating systems");
		for (int priority = 0; priority < systems.size(); priority++)
		{
			Class<? extends EntitySystem> systemClass = systems.get(priority);
			try
			{
				EntitySystem system = systemClass.getDeclaredConstructor(Integer.class, BaseGame.class).newInstance(priority * priority, this);
				log.debug("{} priority: {}", systemClass, system.priority);
				engine.addSystem(system);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		log.debug("Add input handler");
		this.inputCommandProcessor = new MainInputHandler(this);
		this.enterMode(GameMode.DEFAULT_MODE);

		log.debug("Loading init file");
		get(ShellSystem.class).readCmdFile(getClass().getResourceAsStream("/default.cmd"));

		get(ViewportSystem.class).setProcessing(true);

		log.debug("Loading start level");
		get(LevelSystem.class).loadLevel(getConfig().startLevel());
		// setProcessing(false);
		update();
		get(LevelSystem.class).loadLevel(getConfig().startLevel());
	}

	public void quit()
	{
		System.exit(1);
	}

	public void addMessage(String msg)
	{
		log.trace(MarkerFactory.getMarker("XTERM:248-1"), "addMessage: {}", msg);
		this.getMessages().add(msg);
	}

	@SuppressWarnings("all")
	public synchronized void enterMode(GameMode newMode, Entity... args)
	{
		if (!modeStack.isEmpty() && modeStack.peek() == newMode)
		{
			return;
		}
		modeStack.push(newMode);
		log.debug("Notifying of enter {}", newMode);
		for (GameModeChangeListener l : this.gameModeChangeListeners)
		{
			l.onEnterMode(newMode, args);
		}
	}

	public synchronized void exitMode()
	{
		GameMode currentMode = modeStack.pop();
		log.debug("Notifying of exit {}", currentMode);
		for (GameModeChangeListener l : this.gameModeChangeListeners)
		{
			l.onExitMode(currentMode);
		}
	}

	public <K extends EntitySystem> K get(Class<K> clazz)
	{
		return engine.getSystem(clazz);
	}

	public synchronized GameMode getCurrentMode()
	{
		return modeStack.peek();
	}

	public MainInputHandler getInputHandler()
	{
		return inputCommandProcessor;
	}

	public List<String> getMessageList()
	{
		ArrayList<String> ret = new ArrayList<>();
		for (String msg : getMessages())
		{
			ret.add(0, msg);
		}
		return ret;
	}

	public Queue<String> getMessages()
	{
		return messages;
	}

	public Entity getPlayer()
	{
		return get(PlayerSystem.class).get();
	}

	public <T extends Component> T getPlayer(Class<T> componentClass)
	{
		return get(PlayerSystem.class).get(componentClass);
	}

	public boolean isProcessing()
	{
		// if (processing)
		// return
		// get(MovementSystem.class).isMoving(get(PlayerSystem.class).getPlayer());
		return processing;
	}
	
	public boolean isProcessing(Class<? extends EntitySystem> clazz)
	{
		return this.get(clazz).checkProcessing();
	}

	// XXX
	public void setProcessing(boolean processing)
	{
		this.processing = processing;
		for (Class<? extends EntitySystem> systemClass : toggleableSystems)
		{
			engine.getSystem(systemClass).setProcessing(processing);
		}
	}

	public void loadGame(String arg)
	{
		boolean wasProcessing = isProcessing();
		setProcessing(false);
		EntitySerializer.loadGameFromFile(this, arg);
		setProcessing(wasProcessing);
		for (GameSaveListener l : gameSaveListeners)
		{
			l.onLoad();
		}
	}

	public void saveGame(String fileName)
	{
		EntitySerializer.saveGameToFile(this, fileName);
	}

	public void stopAllSystems()
	{
		for (EntitySystem system : engine.getSystems())
		{
			system.setProcessing(false);
		}
	}

	/*
	 * Abstract methods
	 */

	public abstract void setClipboardContent(String content);

	public abstract void playSound(Sounds sound);

	public abstract String getClipboardContent();

	/*
	 * 
	 * Methods implementing {@link PartialEngine}
	 */

	@Override
	public ImmutableArray<Entity> getEntities()
	{
		return engine.getEntities();
	}

	public Map<String, List<Entity>> getInactiveEntities()
	{
		return inactiveEntities;
	}

	@Override
	public Entity addEntity(Entity entity)
	{
		entity.add(new GameComponent(this));
		engine.addEntity(entity);
		return entity;
	}

	@Override
	public ImmutableArray<Entity> getEntitiesFor(Family family)
	{
		return engine.getEntitiesFor(family);
	}

	public ImmutableArray<Entity> getEntitiesFor(Family family, Comparator<Entity> comparator)
	{
		Entity[] entities = engine.getEntitiesFor(family).toArray(Entity.class);
		Array<Entity> arr = new Array<>(false, entities.length);
		arr.addAll(entities);
		arr.sort(comparator);
		return new ImmutableArray<Entity>(arr);
	}

	@Override
	public Entity getEntity(long id)
	{
		return engine.getEntity(id);
	}

	@Override
	public void removeEntity(Entity e)
	{
		engine.removeEntity(e);
	}

	@Override
	public void removeAllEntities()
	{
		engine.removeAllEntities();
	}

	public Map<EntitySystem, Boolean> getSystemStates()
	{
		Map<EntitySystem, Boolean> states = new HashMap<>();
		for (EntitySystem system : engine.getSystems())
		{
			states.put(system, system.checkProcessing());
		}
		return states;
	}

	public void applyStates(Map<EntitySystem, Boolean> map)
	{
		for (Entry<EntitySystem, Boolean> kv : map.entrySet())
		{
			kv.getKey().setProcessing(kv.getValue());
		}
	}

	@Override
	public void update()
	{
		while (!beforeUpdateTasks.isEmpty())
		{
			log.trace("Running 'before' tasks");
			beforeUpdateTasks.remove().run();
		}
//		log.trace("Updating engine");
		// TODO
		int delta = get(PlayerSystem.class).get(Busy.class).getTimeLeft();
		engine.update(delta);
		while (!afterUpdateTasks.isEmpty())
		{
			log.trace("Running 'after' tasks");
			afterUpdateTasks.remove().run();
		}
	}

}
