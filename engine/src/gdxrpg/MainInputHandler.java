package gdxrpg;

import gdxrpg.command.CommandInvocation;
import gdxrpg.command.builtin.Eval;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.constants.GameMode;
import gdxrpg.ecs.event.GameModeChangeListener;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.github.kba.logback.xterm256.Xterm256Constants;

public class MainInputHandler implements InputHandler {
	private static final Logger log = LoggerFactory.getLogger(MainInputHandler.class);

	public final static Map<GameMode, Map<KeyPress, CommandInvocation>> mappings = new HashMap<>();
	static
	{
		for (GameMode mode : GameMode.values())
		{
			mappings.put(mode, new HashMap<KeyPress, CommandInvocation>());
		}
	}

	private final Set<InputCommandListener> inputCommandListeners = new HashSet<>();
	private final Map<GameMode, GameModeHandler> gameModes = new HashMap<>();;
	private final BaseGame game;
	public MainInputHandler(final BaseGame game)
	{
		this.game = game;
		log.debug("Setup game mode handlerss");
		for (GameMode mode : GameMode.values())
		{
			gameModes.put(mode, mode.createHandler(game));
		}
		this.game.addGameModeChangeListener(new GameModeChangeListener() {
			@Override
			public void onEnterMode(GameMode mode, Object... args)
			{
				gameModes.get(mode).onEnter();
			}

			@Override
			public void onExitMode(GameMode mode, Object... args)
			{
				gameModes.get(mode).onExit();
			}
		});
	}
	public synchronized void addInputCommandListener(InputCommandListener listener)
	{
		this.inputCommandListeners.add(listener);
	}

//	public void handleInputCommand(Enumommand cmd, Object... arguments)
//	{
//		handleInputCommand(new CommandInvocation(cmd, arguments));
//
//	}

	@Override
	public boolean handleInputCommand(CommandInvocation invoc)
	{
		if (null == invoc)
			return false;
		for (InputCommandListener l : inputCommandListeners)
		{
			l.onReceivedCommandInvocation(invoc);
		}
		if (invoc.command.getClass() != Eval.class)
			log.debug(Xterm256Constants.HIGHLIGHT, "Received input command {}", invoc.command);
		boolean handled = gameModes.get(game.getCurrentMode()).handleInputCommand(invoc);
		if (handled) {
			game.setProcessing(true);
		}
//		if (!handled)
//		{
//			switch (invoc.getCommand().getClass().getName())) {
//				case QuitCommand.class.getName():
//					game.quit();
//					break;
//				default:
//					break;
//			}
//		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gdxrpg.InputHandler#handleKey(java.lang.String)
	 */
	@Override
	public boolean handleKey(KeyPress key)
	{
		if (null == key)
		{
			return false;
		}
		log.trace("Handling key {}", key);
		if (key.equalsVimString("1"))
		{
			for (Entity entity : game.getEntitiesFor(Family.all(Busy.class).get())) {
				log.debug("{} : [{}]", entity, CM.get(entity, Busy.class).queue);
			}
			return true;
		}
		Map<KeyPress, CommandInvocation> mapping = mappings.get(game.getCurrentMode());
		if (mapping.containsKey(key))
		{
			return game.getInputHandler().handleInputCommand(mapping.get(key));
		} else 
			return gameModes.get(game.getCurrentMode()).handleKey(key);
	}

	public interface InputCommandListener {
		public void onReceivedCommandInvocation(CommandInvocation invoc);
	}

}
