package gdxrpg;

import gdxrpg.command.CommandInvocation;

import org.bitbucket.kb.keypress.KeyPress;

public interface InputHandler {

	public abstract boolean handleInputCommand(CommandInvocation invoc);

	public abstract boolean handleKey(KeyPress key);

}