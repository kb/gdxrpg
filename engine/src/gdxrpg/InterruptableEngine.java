package gdxrpg;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.Engine;

public class InterruptableEngine extends Engine {

	List<Runnable> beforeUpdateTasks = new ArrayList<>();
	List<Runnable> afterUpdateTasks = new ArrayList<>();

	public void addBeforeUpdateTask(Runnable r)
	{
		this.beforeUpdateTasks.add(r);
	}

	public void addAfterUpdateTask(Runnable r)
	{
		this.afterUpdateTasks.add(r);
	}

	@Override
	public void update(float deltaTime)
	{
		if (!beforeUpdateTasks.isEmpty())
		{
			for (Runnable r : beforeUpdateTasks)
			{
				r.run();
			}
			beforeUpdateTasks.clear();
		}
		super.update(deltaTime);
		if (!afterUpdateTasks.isEmpty())
		{
			for (Runnable r : afterUpdateTasks)
			{
				r.run();
			}
			afterUpdateTasks.clear();
		}
	}

}
