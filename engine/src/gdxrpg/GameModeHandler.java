package gdxrpg;



public interface GameModeHandler extends InputHandler {
	
	void onExit();

	void onEnter();

}
