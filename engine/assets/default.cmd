# vim: ft=sh

#
# Aliases
# 

alias a move-dir
alias nw move-dir nw
alias x examine
alias gg move-dir e
alias c close-xy

# 
# Bindings
# 

bind DEFAULT_MODE g pickup-first-dir here
bind DEFAULT_MODE <F1> help
bind DEFAULT_MODE : cmdline
bind DEFAULT_MODE <c-g> gg
bind DEFAULT_MODE h move-dir w
bind DEFAULT_MODE j move-dir s
bind DEFAULT_MODE k move-dir n
bind DEFAULT_MODE l move-dir e
bind DEFAULT_MODE y move-dir nw
bind DEFAULT_MODE u move-dir ne
bind DEFAULT_MODE b move-dir sw
bind DEFAULT_MODE n move-dir se
bind DEFAULT_MODE i inv
bind DEFAULT_MODE s save-game
bind DEFAULT_MODE S load-game
bind DEFAULT_MODE z wait

bind SHELL_MODE <left> shell-left
bind SHELL_MODE <right> shell-right
bind SHELL_MODE <del> shell-delete
bind SHELL_MODE <bs> shell-backspace
bind SHELL_MODE <up> shell-history-prev
bind SHELL_MODE <down> shell-history-next
bind SHELL_MODE <cr> shell-enter
bind SHELL_MODE <c-k> shell-delete-to-eol
bind SHELL_MODE <insert> shell-paste
bind SHELL_MODE <esc> shell-cancel
bind SHELL_MODE <home> shell-pos1
bind SHELL_MODE <end> shell-end

bind SHELL_MODE <c-b> shell-left
bind SHELL_MODE <c-f> shell-right
bind SHELL_MODE <c-h> shell-backspace
bind SHELL_MODE <c-d> shell-delete
bind SHELL_MODE <c-j> shell-enter
bind SHELL_MODE <c-a> shell-pos1
bind SHELL_MODE <c-e> shell-end
