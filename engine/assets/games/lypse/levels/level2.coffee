###
# Private
###
levelBuilder = new LevelBuilder(
	name: 'level2',
	width: 50,
	height: 50,
	defaultGround:
		renderable:
			fgChar: '1'
	defaultPlayerSpawnPosition: x: 1, y: 3
)

levelBuilder.draw 'scenery', 0, 0,
	"""
	########
	#      #######0
	#      #      |
	#      #      #
	#      |      #
	#      #      #
	#      #      #
	####  #########
	#      #      #
	#      #      #
	#      #      #
	#      |      #
	0#######      #
	       #      #
	       ########
	""",
	'0':
		renderable: fgChar: '#', fgColor: '88aa22'
	# '|':
		# openable: openChar: '.', openable: yes, open: no
levelBuilder.draw 'room', 0, 0,
	"""
	        
	 111111        
	 111111 222222 
	 111111 222222 
	 111111 222222 
	 111111 222222 
	 111111 222222 
	               
	 333333 444444 
	 333333 444444 
	 333333 444444 
	 333333 444444 
	        444444 
	        444444 
	               
	""",
	'1':
		description: name: 'room-1'
		passable: true
	'2':
		description: name: 'room-2'
		passable: true
	'3':
		description: name: 'room-3'
		passable: true
	'4':
		description: name: 'room-4'
		passable: true

levelBuilder.addEntity LevelPortal,
	uniqueLabel: 'level1'
	position: x: 3, y: 2
	renderable: fgChar: '*', fgColor: 'ff0fff'

levelBuilder.addItem {
	description: name: 'up-down-stairs'
	position: x: 1, y: 1
	renderable: fgChar: "ͳ", fgColor: '0f0fff'
}

levelBuilder.addItem {
	description: name: 'down-stairs'
	position: x: 1, y: 4
	renderable: fgChar: '>', fgColor: 'f0ffff'
}
levelBuilder.addNPC {
	position: x: 6, y: 7
	speed: 100
	description: name: 'foo'
}
