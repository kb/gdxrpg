###
# Private
###
levelBuilder = new LevelBuilder(
	name: 'level1',
	width: 20,
	height: 30,
	defaultGround:
		renderable:
			fgChar: '1'
			fgColor: '006600'
	defaultPlayerSpawnPosition: x: 4, y: 9
)

levelBuilder.draw 'scenery', 0, 0,
	"""
	012345678901234567890123456789012345678901234567890
	1  ########                                       1
	2  #      #######0                                2
	3  #      #      |                                3
	4  #      #      #                                4
	5  #      |      #                                5
	6  #      #      #                                6
	7  #      #      #                                7
	8  ####  #########                                8
	9  #      #      #                                9
	0  #      #      #                                0
	1  #      #      #                                1
	2  #      |      #                                2
	3  0#######      #                                3
	4         #      #                                4
	5         ########                                5
	6                                                 6
	7                                                 7
	8                                                 8
	9                                                 9
	012345678901234567890123456789012345678901234567890
	""",
	'0':
		renderable: fgChar: '#', fgColor: '88aa22'
	'#':
		opacity:  70
		passable: false
	'|':
		description:
			name: 'door'
		# passable: true
		openable:
			currentState: "OPEN"
			stateMap:
				OPEN:
					renderable: fgChar: '/', fgColor: '88aa22'
					passable: true
					opacity: 0
				CLOSED:
					renderable: fgChar: '|', fgColor: '00aa22'
					passable: false
					opacity: 100
		renderable: fgColor: '3333ff'
		script:
			name: 'door-code'
			code:
				onCommand: (game, self, args) ->
					for x,idx of args
						log.warn("ARG #{idx}: {}", x)

levelBuilder.draw 'room', 0, 0,
	"""
	        
	 111111        
	 111111 222222 
	 111111 222222 
	 111111 222222 
	 111111 222222 
	 111111 222222 
	               
	 333333 444444 
	 333333 444444 
	 333333 444444 
	 333333 444444 
	        444444 
	        444444 
	               
	""",
	'1':
		description: name: 'room-1'
		passable: true
	'2':
		description: name: 'room-2'
		passable: true
	'3':
		description: name: 'room-3'
		passable: true
	'4':
		description: name: 'room-4'
		passable: true

levelBuilder.addEntity LevelPortal,
	uniqueLabel: 'level2'
	position: x: 5, y: 10
	renderable: fgChar: '*', fgColor: 'ff0fff'

levelBuilder.addItem {
	description: name: 'up-down-stairs'
	position: x: 1, y: 1
	renderable: fgChar: "ͳ", fgColor: '0f0fff'
}

levelBuilder.addItem {
	description: name: 'down-stairs'
	weight: 4650
	pickup: true
	position: x: 1, y: 4
	renderable: fgChar: '>', fgColor: 'f0ffff'
}

levelBuilder.addItem {
	description: name: 'wooden bow', description: "A sturdy bow made of oak wood."
	weight: 4650
	pickup: true
	position: x: 5, y: 5
	renderable: fgChar: '%', fgColor: 'afffaa'
}

levelBuilder.addItem {
	description: name: 'backpack', description: "A backpack"
	itemContainer: weightCapacity: 10*1000, volumeCapacity: 60 *1000
	weight: 3000
	pickup: true
	position: x: 1, y: 8
	renderable: fgChar: '&', fgColor: 'afffaa'
}

levelBuilder.addItem {
	description: name: 'sword', description: "A sword"
	itemContainer: weightCapacity: 10*1000, volumeCapacity: 60 *1000
	weight: 3000
	pickup: true
	position: x: 1, y: 8
	renderable: fgChar: '&', fgColor: 'afffaa'
}

for i in [1 ... 3]
	levelBuilder.addNPC {
		position: x: 5, y: i
		movement: dx:0, dy: 0
		speed: 120 - Math.random() * 40
		description: name: 'foo'
	}
levelBuilder.addNPC {
	renderable: fgChar: 'D', fgColor: '00ff0f'
	position: x: 7, y: 8
	movement: dx:0, dy: 0
	speed: 180
	description: name: 'fokr'
	script:
		code:
			onCommand: (game, self, command, args) ->
				if command.getName() == 'ask-about'
					game.addMessage("I don't know anything about ")
					game.addMessage("I don't know anything about " + args[1])
			onCollision: (game, self, source) ->
				log.debug("I got hit by {}", source)
				game.get(AttackSystem.class).meleeAttack(source, self)
				return false
}
