###
# Private
###
levelBuilder = new LevelBuilder(
	name: 'test-level-1',
	width: 20,
	height: 20,
	defaultGround:
		renderable:
			fgChar: '1'
	defaultPlayerSpawnPosition: x: 1, y: 8
)

levelBuilder.addItem {
	description: name: 'up-down-stairs'
	weight: 999
	position: x: 1, y: 1
	renderable: fgChar: "ͳ", fgColor: '0f0fff'
}

levelBuilder.addItem {
	description: name: 'down-stairs'
	weight: 999
	volume: 10
	position: x: 1, y: 4
	renderable: fgChar: '>', fgColor: 'f0ffff'
}

levelBuilder.addEntity Item, {
	description: name: 'wooden bow', description: "A sturdy bow made of oak wood."
	weight: 4650
	opacity: 3
	pickup: true
	position: x: 5, y: 5
	renderable: fgChar: '%', fgColor: 'afffaa'
}

levelBuilder.addNPC {
	position: x: 2, y: 2
	description: name: 'foo'
	aIComponent:strategy:'HUNT_PLAYER'
}
