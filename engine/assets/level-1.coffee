LEVEL_ONE_NAME = "sample-1"
LEVEL_TWO_NAME = "sample-2"

WIDTH = 50
HEIGHT = 50
NUMBER_OF_NPCS = 0

# RNG = {
#     nextInt: (bound) -> Math.floor(Math.random() * bound)
# }

ret = new ArrayList()

###
# Add level
###
ret.add EntityFactory.level(LEVEL_ONE_NAME, WIDTH, HEIGHT)

###
# Add tiles
###
log.debug "Loading map"
map = EntityBuilder.mapBuilder(LEVEL_ONE_NAME, WIDTH, HEIGHT)
toDraw = """xxxxxxxx
|      x
|      x
x      x
xxxxxxxx
"""
legend = new HashMap()
legend.put '|', EntityBuilder.empty().renderable('|').passable(false).openable(true, true, false)
legend.put 'x', EntityBuilder.empty().renderable('#').passable(false)
map.draw(MapScenery.class, 7, 12, toDraw, legend)
# log.debug("{}", legend)

# map.add tile for tile in EntityFactory.createMapEmpty(LEVEL_ONE_NAME, WIDTH, HEIGHT, '\u2593')
# for cell, idx in map
	# row = parseInt(idx / WIDTH)
	# if row % 2 == 0
		# CM.get(cell, Renderable.class).fgColor = '123456'

for e in map.getEntities()
	log.debug "{}", CM.get(e, Position.class)
ret.addAll map.getEntities()
log.debug("Entities " + map.getEntities().size())
log.debug "Loaded map"


###
# Add NPCS
###
createNPCs = () ->
	for i in [0 .. NUMBER_OF_NPCS]
		strategy = AIStrategy.values()[Math.ceil(Math.random() * (AIStrategy.values().length - 1))]
		x = Math.ceil(RNG.nextInt WIDTH)
		y = Math.ceil(RNG.nextInt HEIGHT)
		# while not CM.get(map[y * WIDTH + x], "passable").passable
		#     x = Math.ceil(RNG.nextInt WIDTH)
		#     y = Math.ceil(RNG.nextInt HEIGHT)
			# log.debug("hangs here")
		# ret.add EntityFactory.createNPC(x, y, 'foo', strategy, false)
		# log.debug("Strategy {}", strategy)
		ret.add entity NPC, {
			position:
				x: x
				y: y
			renderable:
				fgChar: strategy.name().substr(0, 1)
				color: "abcdef"
			description:
				name:  NamingUtils.getRandomName()
			aIComponent:
				strategy: strategy.name()
			passable: false
			speed:
				speed: RNG.nextInt 80, 100
			health:
				health:80
				maxHealth: 100
			movement:
				dx: 0
				dy: 0
		}

###
# Add items
###
createItems = () ->
	ret.add EntityFactory.item(2, 2, "Г", MyColor.RED, true)
	ret.add EntityFactory.item(2, 2, "ℝ", MyColor.BLUE, true)
	ret.add EntityFactory.item(2, 6, "\ue26e", MyColor.RED, true)
	ret.add EntityFactory.item(3, 6, "ℝ", MyColor.BLUE, true)
	ret.add EntityFactory.item(2, 7, "D", MyColor.BLUE, true)
	ret.add EntityFactory.item(2, 8, "ø", MyColor.BLUE, true)
	ret.add EntityFactory.item(3, 8, "Ø", MyColor.BLUE, true)
	ret.add EntityFactory.item(20, 8, "©", MyColor.BLUE, true)
	ret.add EntityFactory.item(2, 9, "ϟ", MyColor.BLUE, true)
	ret.add EntityFactory.item(2, 10, "♥", MyColor.RED, true)
	log.debug "adding shit"
	ret.add entity Item, {
		passable: false
		uniqueLabel: 'foo'
		pickup: true
		openable: true
		passable: false
		description:
			name: 'fork'
		renderable:
			fgChar: "X"
			color:"ff0000"
		position:
			x: 1
			y: 1
	}
	y = buildEntity(Item)
			.position 10, 10 
			.renderable '9', 'f0f0cb'
			.description 'xubbl'
			.build()
	ret.add y
	log.debug "added shit"

ret.add(EntityFactory.portal(8, 8, 8, 3))
ret.add(EntityFactory.portal(4, 4, 9, 7))

createNPCs()
createItems()
ret.add entity NPC, {
	description: "insert funny name here"
	passable: false
	renderable:
		fgChar: ''
		color: "f0f000"
	position:
		x: 3
		y: 3
	movement:
		dx: 0
		dy: 0
	health: 100
	speed: RNG.nextInt 80, 100
}

log.debug("Adding all {} entities", ret.size())
engine.addEntity e for e in ret
log.debug("Done Adding all entities")
