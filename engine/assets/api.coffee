# CM = Java.type("gdxrpg.ecs.CM")
# LoggerFactory = Java.type("org.slf4j.LoggerFactory")
# log = LoggerFactory.getLogger('js/api')

# print("XXXXXXXXXXXXXXXXXXXXXXXXX")
# print("X     START OF API      X")
# print("XXXXXXXXXXXXXXXXXXXXXXXXX")

extend = (object, properties) ->
	for key, val of properties
		object[key] = val
	object

deepmerge = (target, src) ->
	array = Array.isArray(src)
	dst = array and [] or {}
	if array
		target = target or []
		dst = dst.concat(target)
		src.forEach (e, i) ->
			if typeof dst[i] == 'undefined'
				dst[i] = e
			else if typeof e == 'object'
				dst[i] = deepmerge(target[i], e)
			else
				if target.indexOf(e) == -1
					dst.push e
			return
	else
		if target and typeof target == 'object'
			Object.keys(target).forEach (key) ->
				dst[key] = target[key]
				return
		Object.keys(src).forEach (key) ->
			if typeof src[key] != 'object' or !src[key]
				dst[key] = src[key]
			else
				if !target[key]
					dst[key] = src[key]
				else
					dst[key] = deepmerge(target[key], src[key])
			return
	dst


###
#
###
class ApiUtilsClass

	###
	# Make the first letter of a string uppercase
	###
	ucfirst: (str) ->
		str.substr(0,1).toUpperCase() + str.substr(1)

	###
	# Make the first letter of a string lowercase
	###
	lcfirst: (str) ->
		str.substr(0,1).toLowerCase() + str.substr(1)
	isRealObject: (varVal) ->
		return typeof varVal is 'object' and Object.prototype.toString.call(varVal) is '[object Object]'
	strToClass: (type) ->
		if typeof type is 'string'
			# print "TypeStr: #{@ucfirst type}, api.types: #{api.types}"
			type = api.types[@ucfirst type]
		return type
	getSimpleName: (type) ->
		type = ApiUtils.strToClass type
		type.class.getSimpleName()
	getCanonicalName: (typeStr) ->
		type = ApiUtils.strToClass typeStr
		if not type or not type.class
			log.debug "No type for #{typeStr}"
			throw "No type for #{typeStr}"
		type.class.getCanonicalName()

_entityJSON = (typeCompName, desc) ->
	ret = []
	if not desc
		desc = typeCompName
		typeCompName = null
	if typeCompName
		ret.push {
			'@class': ApiUtils.getCanonicalName(typeCompName)
		}
	# log.debug "DESC: {}", JSON.stringify desc
	for compName, compValues of desc
		# log.debug ApiUtils.strToClass compName
		# log.debug typeof compName
		compMap = {}
		compMap["@class"] = ApiUtils.getCanonicalName compName
		if typeof compValues in ['string', 'boolean', 'number']
			x = {}
			x[ApiUtils.lcfirst compName] = compValues
			compValues = x
		# nested components
		logit = false
		for varName, varVal of compValues
			# log.debug "{} => {}", varName, Object.prototype.toString.call(varVal)
			if ApiUtils.isRealObject(varVal)
				# log.debug "VARVAL: {}", JSON.stringify varVal
				states = {}
				isCode = false
				for objKey, objValue of varVal
					if ApiUtils.isRealObject(objValue)
						states[objKey] = _entityJSON objValue
					else if typeof objValue is 'function'
						isCode = true
						compMap[varName] or= ""
						compMap[varName] += "#{objKey} = #{objValue.toString()}\n"
						# logit = true
					else
						log.error("Unhandled object type {} in {}", objKey, varVal)
				unless isCode
					compMap[varName] = states
			else
				compMap[varName] = varVal
		if logit
			log.error "{}", JSON.stringify compMap
		ret.push compMap
	return ret

_entity = (type, desc) ->
	asJSON = JSON.stringify _entityJSON type, desc
	# log.debug "JSON: {}", ret
	# log.debug "JSON: {}", asJSON
	e = EntitySerializer.fromJSON asJSON
	if type
		# log.debug "VALIDATE {}", type
		CM.validate e, ApiUtils.strToClass(type).class
	return e

ApiUtils = new ApiUtilsClass(this)
# buildEntity = (type) ->
#     EntityBuilder.builder(EntityType.valueOf(type))

class LevelBuilder

	strLayerMap : {
		ground: MapGround
		scenery: MapScenery
		room: Room
	}

	@defaultItem : {
		opacity: 0
		passable: true
		description: name: 'define-me'
		renderable: fgChar: '?', fgColor: "ffffff"
	}

	@defaultNPC : {
		animatable: animations: []
		renderable: fgChar: 'N', fgColor: "ffffff"
		health: health: 100, maxHealth: 100
		experienceLevel: 1
		eyeSight: 10
		speed: 100
		rpgAttributes:
			str: 1
			dex: 1
			con: 1
			wis: 1
			intel: 1
			cha: 1
		busy: 0
		movement:
			dx: 0
			dy: 0
		passable: false
		description: name: 'john'
		itemContainer: weightCapacity: 20 * 1000, volumeCapacity: 60 * 1000
		behavior: 'hunt.tree'
	}
	levelEntities: []

	constructor : (@opts) ->
		@level = {
			belongsToLevel: @opts.name
			uniqueLabel: @opts.name
			extent:
				width: @opts.width
				height: @opts.height
			defaultPlayerSpawnPosition: @opts.defaultPlayerSpawnPosition
		}
		@map = {}
		for k of @strLayerMap
			@map[k] = {}
		@defaultGround = deepmerge {
			renderable:
				fgChar: '.'
				fgColor: '018858'
			passable: true
		}, @opts.defaultGround
		@initializeGroundLayer()

	initializeGroundLayer: () ->
		for x in [0 ... @level.extent.width]
			for y in [0 ... @level.extent.height]
				@map.ground[@pos2str(x, y)] = @defaultGround
	
	str2type : (str) -> @strLayerMap[str]
	pos2str : (x, y) -> "#{x},#{y}"
	str2pos : (str) -> 
		[x, y] = str.split(",")
		return {x: parseInt(x), y: parseInt(y)}
	
	numberOfTiles : () ->
		ret = 0
		for layerStr, tileMap of @map
			ret += tileMap.length
		return ret
	
	addEntity : (type, desc) ->
		log.debug("Called")
		@levelEntities.push _entity(type, desc)

	_buildMap: () ->
		ret = []
		log.debug("No of tiles:{}", @numberOfTiles())
		# Add cells
		for layerStr, tileMap of @map
			for posStr, cell of tileMap
				layerType = @str2type layerStr
				cell.belongsToLevel = {
					belongsToLevel: @level.uniqueLabel
				}
				cell.position = @str2pos posStr
				e = _entity layerType, cell
				ret.push e
		# Add level
		level = _entity Level, @level
		ret.push level
		return ret

	getEntities: () ->
		ret = new ArrayList()
		for e in @_buildMap()
			ret.add e 
		for e in @levelEntities
			ret.add e 
		return ret

	draw: (layer, xOffset, yOffset, toDraw, legend) ->
		for line, y in toDraw.split "\n" 
			continue if y >= @level.extent.height
			line = line.replace(/^\t*/, '')
			for cell, x in line.split ""
				continue if x >= @level.extent.width
				desc = legend[cell]
				if cell isnt ' ' and not desc
					desc = {}
				if (desc)
					@map[layer][@pos2str(x,y)] or= { }
					@map[layer][@pos2str(x,y)][k] = v for k,v of desc
					@map[layer][@pos2str(x,y)].renderable or= {}
					@map[layer][@pos2str(x,y)].renderable.fgChar or= cell
					@map[layer][@pos2str(x,y)].passable or= false
					@map[layer][@pos2str(x,y)].opacity or= 0

	addEntityWithDefault: (type, defaultDesc, desc) ->
		e = _entity type, deepmerge defaultDesc, desc
		@levelEntities.push e
		return e

	addNPC : (desc) -> @addEntityWithDefault NPC, LevelBuilder.defaultNPC, desc

	addItem : (desc) -> @addEntityWithDefault Item, LevelBuilder.defaultItem, desc

# print("XXXXXXXXXXXXXXXXXXXXXXXXX")
# print("X      END OF API       X")
# print("XXXXXXXXXXXXXXXXXXXXXXXXX")
