package gdxrpg.ecs.texttable;

import gdxrpg.ecs.texttable.TextTable.TextTableBuilder;
import gdxrpg.ecs.texttable.TextTable.TextTableAlign;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TextTableTest {
	
	private static final Logger log = LoggerFactory.getLogger(TextTableTest.class);
	
	@Test
	public void schmoo() {
		TextTableBuilder tb = TextTable.builder();
		tb.row();
		tb.cell("bar").align(TextTableAlign.Left).fgColor("ff0000");
		tb.cell("quux").align(TextTableAlign.Left).fgColor("ff0000");
		tb.row();
		log.debug("BUILT: {}", tb.build());
	}

}
