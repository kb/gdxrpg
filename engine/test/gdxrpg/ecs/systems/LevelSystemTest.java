package gdxrpg.ecs.systems;

import static org.junit.Assert.*;
import gdxrpg.BaseGame;
import gdxrpg.BaseTest;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.builtin.Wait;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Movement;
import gdxrpg.ecs.components.action.AbstractAction;
import gdxrpg.ecs.components.action.IAction;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.NPC;

import org.junit.Test;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeParser;
import com.owlike.genson.Deserializer;

public class LevelSystemTest extends BaseTest {

	@Test
	public void testGraph() throws Exception
	{
		BaseGame game = loadTestGame("lypse");
		LevelSystem levelSystem = game.get(LevelSystem.class);
		log.debug("Node @ 0,0 {}", levelSystem.positionGraph.getNode(new Position(3, 1)).getConnections());
		log.debug("Path {}", levelSystem.findPath(new Position(0, 0), new Position(19, 19)));
	}

	@Test
	public void testBTree() throws Exception
	{
		BaseGame game = loadTestGame("lypse");
//		Path treePath = game.getGameDirectory().resolve("behaviors/test.tree");
//		Reader reader = Files.newBufferedReader(treePath);
//		BehaviorTreeParser<Entity> parser = new BehaviorTreeParser<>(BehaviorTreeParser.DEBUG_NONE);
		Entity npc = game.getEntitiesFor(Family.all(NPC.class).get()).random();
//		BehaviorTree<Entity> btree = parser.parse(reader, npc);
//		log.debug("MovementSystem active? {}", game.isProcessing(MovementSystem.class));
		
		BehaviorTree<Entity> btree = parser.parse(reader, npc);
		// log.debug("MovementSystem active? {}", game.isProcessing(MovementSystem.class));

		game.setProcessing(true);
		for (int i = 0; i < 5; i++)
		{
			log.debug("MovementSystem active? {}", game.isProcessing(MovementSystem.class));
			log.debug("POSITION / MOVEMENT {} / {}", CM.get(npc, Position.class), CM.get(npc, Movement.class));
			game.getInputHandler().handleInputCommand(new CommandInvocation(new Wait()));
			game.update();
		}
	}

	@Test
	public void testSerializeRunnable() throws Exception
	{
	}

}
