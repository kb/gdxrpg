package gdxrpg;

import static org.junit.Assert.assertEquals;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.Volume;
import gdxrpg.ecs.components.Weight;
import gdxrpg.ecs.components.position.Position;

import java.util.List;

import org.junit.Test;

import com.badlogic.ashley.core.Entity;

public class EntityFinderTest extends BaseTest {

	@Test
	public void testFilterCandidates() throws Exception
	{
		BaseGame game = loadTestGame("test-game-1");
		log.debug("NO entities: {}", game.getEntities().size());
		EntityFinder entityFinder = new EntityFinder();
		entityFinder.radius(3);
		entityFinder.includeComponent(Renderable.class);
		entityFinder.excludeComponent(Volume.class);
		entityFinder.label("o");
		entityFinder.matchesComponent(Weight.class, w -> w.weight < 1000);
		List<Entity> find = entityFinder.find(new Position(1,1), game);
		assertEquals(1, find.size());
		dump(find);
	}

	@Test
	public void testFromSimpleName() throws ClassNotFoundException
	{
		BaseGame game = loadTestGame("test-game-1");
		EntityFinder entityFinder = new EntityFinder();
		entityFinder.radius(10);
		entityFinder.excludeComponent("Item");
		dump(entityFinder.find(new Position(1, 1), game));
	}

}
