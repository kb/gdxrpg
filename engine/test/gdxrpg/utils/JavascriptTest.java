package gdxrpg.utils;

import gdxrpg.ecs.scripting.CoffeeLoader;

import java.io.IOException;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class JavascriptTest {

	private void foo() {
//		List<Integer> layer1 = Arrays.asList(
//			0, 0, 0, 0, 0,
//			0, 0, 0, 0, 0,
//			0, 0, 0, 0, 0,
//			0, 0, 0, 0, 0,
//			0, 0, 0, 0, 0,
//			0, 0, 0, 0, 0
//        );
	}

	public static void main(String[] args) throws ScriptException, IOException, InterruptedException {
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
		Bindings bindings = nashorn.createBindings();
		String script = CoffeeLoader.compileCoffeeScriptFile("/tmp/test.coffee");
		nashorn.setBindings(bindings, ScriptContext.GLOBAL_SCOPE);
		nashorn.eval(script);
	}	

}
