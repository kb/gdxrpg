package gdxrpg;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;


public class BaseGameTest {
	
	Logger log = LoggerFactory.getLogger(BaseGameTest.class);
	
	@Test
	public void testEnitityId() {
		
		Engine engine = new Engine();
		Entity e2 = new Entity();
		Entity e = e2;
		log.debug("{}", e.getId());
		engine.addEntity(e);
		log.debug("{}", e.getId());
		engine.removeAllEntities();
		log.debug("{}", e.getId());
		engine.addEntity(e2);
		log.debug("{}", e2.getId());
		engine.addEntity(e);
		log.debug("{}", e.getId());
	}

	@Test
	public void testSetProcessing() throws Exception
	{
		throw new RuntimeException("not yet implemented");
	}

}
