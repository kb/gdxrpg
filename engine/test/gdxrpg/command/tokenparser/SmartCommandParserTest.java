package gdxrpg.command.tokenparser;

import static org.junit.Assert.assertEquals;
import gdxrpg.BaseGame;
import gdxrpg.BaseTest;
import gdxrpg.command.AbstractCommand;
import gdxrpg.command.CommandInvocation;
import gdxrpg.command.CommandParser;
import gdxrpg.ecs.constants.DirectionHorizontal;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartCommandParserTest extends BaseTest {

	private static final Logger log = LoggerFactory.getLogger(SmartCommandParserTest.class);

	@Test
	public void testParse() throws Exception
	{
		setLoggingLevel("TRACE");
		AbstractCommand testCommand = new AbstractCommand(
				"Just a test",
				SmartCommandParser.builder()
						.string("foo")
						.enumToken(DirectionHorizontal.class)) {

			@Override
			public void execute(BaseGame game, CommandInvocation invoc)
			{
				System.out.println("Was executed!");
			}
		};
		CommandParser parser = testCommand.getParser();
		List<String> tokenize = parser.tokenize("test foo");
		assertEquals("test", tokenize.get(0));
		assertEquals("foo", tokenize.get(1));

		BaseGame game = loadTestGame("test-game-1");
		CommandInvocation parse = parser.parse(game, "test foo N");
		log.debug("{}", parse);
//		log.debug(EntitySerializer.genson.serialize(parser));
//		System.exit(0);
	}
}
