package gdxrpg;

import gdxrpg.ecs.EntitySerializer;

import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;

public class BaseTest {

	protected static final Logger log = LoggerFactory.getLogger(EntityFinderTest.class);
	public static void setLoggingLevel(String level) {
	    ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
	    root.setLevel(ch.qos.logback.classic.Level.toLevel(level));
	}

	public BaseTest()
	{
		super();
	}

	protected BaseGame loadTestGame(String testGame)
	{
		TestBaseGame game = new TestBaseGame(Paths.get("assets/games/" + testGame));
		game.start();
		return game;
	}

	public String dump(Entity entity)
	{
		return EntitySerializer.dump(entity);
	}

	public void dump(List<Entity> find)
	{
		for (int i = 0; i < find.size(); i++)
		{
			Entity entity = find.get(i);
			log.debug("Result #{}: {}", i, dump(entity));
		}
	}

}