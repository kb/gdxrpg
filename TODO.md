# TODO
`vim: set noexpandtab sw=4 ts=4 :`

## Lanterna GUI

[ ] Possibility to load data into player's inventory +CoffeeScript
[ ] Scrollable text. Maybe Markup + CSS?
[ ] Widget for Autocompletion
[ ] Viewport scrolling right-aligns after threshold, fix that

## EntityFinder
[ ] doors aren't found by EntityFinder
	[ ] problem with radius in EntityFinder

## Scripting
[ ] Merge RuleSystem and GameEventSystem
[x] 'before' and 'after' for IScript

## Time and Calendar
[x] TimeSytem measuring time
[x] Measuring in-game time
[ ] Time is not saved / loaded :(

## Mechanisms for individual game products
	[x] revamp config
		[x] Drop Conf/Prop
		[x] Game metadata
		[x] Game config
	[ ] Separate into game asset dir and game user dir
		maybe use xdg? https://github.com/omajid/xdg-java
	[ ] shell history

## Dialog
[ ] Emotion
  [ ] Emotional stance towards player and other entities
[ ] Game log should contain references to entities
  * who said something to whom
[ ] Dialog tree system
  * If dialog is only in code that's unwieldy
  * Something like ashera 
    * https://github.com/Yuffster/asherah
    * https://github.com/scottbw/dialoguejs
