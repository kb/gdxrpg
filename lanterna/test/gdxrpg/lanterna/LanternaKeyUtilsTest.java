package gdxrpg.lanterna;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;


public class LanternaKeyUtilsTest {
	
	private static final Logger log = LoggerFactory.getLogger(LanternaKeyUtilsTest.class);
	
	@Test
	public void testKeyStrokeFromVim() {
		assertEquals("<s-s>", LanternaKeyUtils.keyStrokeToVim(new KeyStroke('S', false, false)));
		assertEquals("s", LanternaKeyUtils.keyStrokeToVim(new KeyStroke('s', false, false)));
		assertEquals("<c-s>", LanternaKeyUtils.keyStrokeToVim(new KeyStroke('s', true, false)));
		assertEquals("<a-s>", LanternaKeyUtils.keyStrokeToVim(new KeyStroke('s', false, true)));
		assertEquals("<c-a-s>", LanternaKeyUtils.keyStrokeToVim(new KeyStroke('s', true, true)));
		assertEquals("<c-a-down>", LanternaKeyUtils.keyStrokeToVim(new KeyStroke(KeyType.ArrowDown, true, true)));
	}

}
