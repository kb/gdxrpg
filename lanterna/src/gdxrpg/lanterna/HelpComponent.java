package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.systems.HelpSystem;
import gdxrpg.ecs.texttable.BorderType;
import gdxrpg.ecs.texttable.TextTable;

import com.googlecode.lanterna.TerminalSize;

public class HelpComponent extends BaseLanternaComponent {

	public HelpComponent(BaseGame game)
	{
		super(game, BorderType.SINGLE);
		setActive(false);
	}

	@Override
	public void draw()
	{
		graphics.fill(' ');
		graphics.putString(0, 0, "HELP");
		TextTable commandHelp = game.get(HelpSystem.class).commandHelp();
		LanternaTextTableDrawer.drawTable(graphics, commandHelp);
		
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
