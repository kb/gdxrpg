package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.constants.Sounds;
import gdxrpg.utils.AudioPlayer;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.nio.file.Path;

public class LanternaGame extends BaseGame {

	public LanternaGame(Path gameDir)
	{
		super(gameDir);
	}

	@Override
	public void playSound(Sounds sound)
	{
		AudioPlayer.playClip(LanternaGame.class.getResourceAsStream("/sounds/" + sound.soundPath));
	}

	@Override
	public String getClipboardContent()
	{
		Transferable clipboardContents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		try
		{
			return (String) clipboardContents.getTransferData(DataFlavor.stringFlavor);
		} catch (UnsupportedFlavorException | IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setClipboardContent(String content)
	{
		// TODO Auto-generated method stub
		
	}

}
