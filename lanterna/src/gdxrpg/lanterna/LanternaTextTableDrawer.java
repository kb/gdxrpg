package gdxrpg.lanterna;

import gdxrpg.ecs.texttable.TextTable;
import gdxrpg.ecs.texttable.TextTable.TextTableAlign;
import gdxrpg.ecs.texttable.TextTableCell;
import gdxrpg.ecs.texttable.TextTableRow;

import java.util.EnumSet;
import java.util.List;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;

public class LanternaTextTableDrawer {

	static void drawTable(TextGraphics textGraphics, TextTable table)
	{
		List<TextTableRow> rows = table.getRows();
		for (int rowIdx = 0; rowIdx < rows.size(); rowIdx++)
		{
			TextTableRow row = rows.get(rowIdx);
			if (rowIdx == table.getSelectedRow())
				textGraphics.setModifiers(EnumSet.of(SGR.BOLD));
			else
				textGraphics.setModifiers(EnumSet.noneOf(SGR.class));
			int offset = 0;
			List<TextTableCell> cells = row.getCells();
			for (int cellIdx = 0; cellIdx < cells.size(); cellIdx++)
			{
				TextTableCell cell = cells.get(cellIdx);
				if (cell.align == TextTableAlign.Right) {
					offset = textGraphics.getSize().getColumns() - cell.contents.length();
				}
				TextColor oldFgColor = textGraphics.getForegroundColor();
				TextColor oldBgColor = textGraphics.getBackgroundColor();
				if (null != cell.fgColor)
					textGraphics.setForegroundColor(TextColorUtils.str2Indexed(cell.fgColor));
				if (null != cell.bgColor)
					textGraphics.setBackgroundColor(TextColorUtils.str2Indexed(cell.bgColor));
				textGraphics.putString(offset, rowIdx, cell.contents);
				offset += cell.contents.length();
				textGraphics.setForegroundColor(oldFgColor);
				textGraphics.setBackgroundColor(oldBgColor);
			}
		}
	}

}
