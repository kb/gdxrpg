package gdxrpg.lanterna;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.TextColor;

public final class TextColorUtils {
	
	private static final Logger log = LoggerFactory.getLogger(TextColorUtils.class);

	static Map<String, TextColor.RGB> rgbCache = new HashMap<>();
	static Map<String, TextColor.Indexed> indexedCache = new HashMap<>();

	static TextColor str2Indexed(String colorStr)
	{
		if (indexedCache.containsKey(colorStr)) {
			return indexedCache.get(colorStr);
		} else {
			if (colorStr.startsWith("#")) {
				colorStr = colorStr.substring(1);
			}
			TextColor.Indexed color = TextColor.Indexed.fromRGB(
					Integer.valueOf(colorStr.substring(0, 2), 16),
					Integer.valueOf(colorStr.substring(2, 4), 16),
					Integer.valueOf(colorStr.substring(4, 6), 16));
			indexedCache.put(colorStr, color);
			indexedCache.put("#" + colorStr, color);
			return color;
		}
	}

	/**
	 * @param colorStr
	 * @param visibility
	 * @return
	 */
	static TextColor str2Indexed(String colorStr, int visibility)
	{
		int vis0 = 0;
		int vis1 = 20;
		int vis2 = 30;
		int vis3 = 45;
		if (visibility <= vis0) {
			return str2Indexed("000000");
		} else if(visibility > vis0 && visibility <= vis1){
			return str2Indexed("111111");
		} else if(visibility > vis1 && visibility <= vis2){
			return str2Indexed("333333");
		} else if(visibility > vis2 && visibility <= vis3){
			return str2Indexed("666666");
		}
		if (indexedCache.containsKey(colorStr)) {
			return indexedCache.get(colorStr);
		} else {
			if (colorStr.startsWith("#")) {
				colorStr = colorStr.substring(1);
			}
			int red = Integer.valueOf(colorStr.substring(0, 2), 16);
			int green = Integer.valueOf(colorStr.substring(2, 4), 16);
			int blue = Integer.valueOf(colorStr.substring(4, 6), 16);
			TextColor.Indexed color = TextColor.Indexed.fromRGB(
					red,
					green,
					blue);
			indexedCache.put(colorStr, color);
			indexedCache.put("#" + colorStr, color);
			return color;
		}
	}

	static TextColor str2RGB(String colorStr)
	{
		if (rgbCache.containsKey(colorStr)) {
			return rgbCache.get(colorStr);
		} else {
			if (colorStr.startsWith("#")) {
				colorStr = colorStr.substring(1);
			}
			TextColor.RGB color = new TextColor.RGB(
					Integer.valueOf(colorStr.substring(0, 2), 16),
					Integer.valueOf(colorStr.substring(2, 4), 16),
					Integer.valueOf(colorStr.substring(4, 6), 16));
			rgbCache.put(colorStr, color);
			rgbCache.put("#" + colorStr, color);
			return color;
		}
	}

}
