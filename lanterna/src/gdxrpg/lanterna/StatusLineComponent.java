package gdxrpg.lanterna;

import com.badlogic.gdx.utils.StringBuilder;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;

import gdxrpg.BaseGame;
import gdxrpg.ecs.components.types.GameTime;
import gdxrpg.ecs.systems.ActionSystem;
import gdxrpg.ecs.systems.FpsCounterSystem;
import gdxrpg.utils.FpsCounter;

import com.badlogic.gdx.utils.StringBuilder;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;

public class StatusLineComponent extends BaseLanternaComponent {

	private final FpsCounter fpsCounter;

	public StatusLineComponent(BaseGame game, FpsCounter fpsCounter)
	{
		super(game);
		this.fpsCounter = fpsCounter;
	}

	@Override
	public void draw()
	{
		graphics.setBackgroundColor(new TextColor.Indexed(233)).fill(' ');
		graphics.putString(0, 0, "MODE: ");
		graphics.setForegroundColor(new TextColor.Indexed(132));
		graphics.putString(7, 0, game.getCurrentMode().toString());
		StringBuilder right = new StringBuilder();
		right.append("| ");
		right.append("PROCESSING: ");
		right.append(game.isProcessing());
		right.append("Date: ");
		right.append(game.get(ActionSystem.class).get(GameTime.class).dateTime);
		right.append("| ");
		right.append("OPS: ");
		right.append(game.get(FpsCounterSystem.class).getFPS());
		right.append(" | ");
		right.append("FPS: ");
		right.append(fpsCounter.fps);
		right.append(" |");
		graphics.putString(graphics.getSize().getColumns() - right.length(), 0, right.toString());
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
