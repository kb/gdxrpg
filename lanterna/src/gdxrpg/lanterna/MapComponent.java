package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.components.Animatable;
import gdxrpg.ecs.components.Animation;
import gdxrpg.ecs.components.Busy;
import gdxrpg.ecs.components.Offset;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.components.types.Item;
import gdxrpg.ecs.components.types.LevelPortal;
import gdxrpg.ecs.components.types.MapGround;
import gdxrpg.ecs.components.types.MapScenery;
import gdxrpg.ecs.components.types.NPC;
import gdxrpg.ecs.components.types.Player;
import gdxrpg.ecs.components.types.Portal;
import gdxrpg.ecs.systems.SelectionManager;
import gdxrpg.ecs.systems.ViewportSystem;

import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;

public class MapComponent extends BaseLanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(MapComponent.class);

	private Family[] renderOrder = new Family[] {
			Family.all(MapGround.class).get(),
			Family.all(MapScenery.class).get(),
			Family.all(Item.class, Position.class).get(),
			Family.all(LevelPortal.class).get(),
			Family.all(Portal.class).get(),
			Family.all(LevelPortal.class).get(),
			Family.all(NPC.class).get(),
			Family.all(Player.class).get(),
	};

	public MapComponent(BaseGame game)
	{
		super(game);
	}

	@Override
	public void draw()
	{
		TextGraphics mapGraphics = this.graphics;
		mapGraphics.fill(' ');
//		int j = 0;
		for (Family family : renderOrder)
		{
			ImmutableArray<Entity> entities = game.getEntitiesFor(family);
			// log.debug("No of entities for {} : {}", j++, entities.size());
			for (int i = 0; i < entities.size(); i++)
			{
				// if (j == 2) {
				// log.debug("{}", EntitySerializer.dump(entities.get(i)));
				// }
				drawEntity(mapGraphics, entities.get(i));
			}
		}
		for (Entity entity : game.get(SelectionManager.class).getSelection())
		{
			drawHighlight(mapGraphics, entity);
		}
	}

	private void drawEntity(TextGraphics mapGraphics, Entity entity)
	{
		// Position pos = CM.get(entity, Position.class);
		if (!game.get(ViewportSystem.class).isWithinCurrentViewport(entity))
			return;

		Entity viewport = game.get(ViewportSystem.class).get();
		Position pos = CM.get(entity, Position.class).minusOffset(CM.get(viewport, Offset.class));
		Renderable rend = CM.get(entity, Renderable.class);
		if (null == pos)
		{
			log.error("{}", EntitySerializer.dump(entity));
			return;
		}
		// TextColor oldBG = mapGraphics.getBackgroundColor();
		// TextCharacter backCharacter = screen.getBackCharacter(pos.roundX(),
		// pos.roundY());
		// mapGraphics.setBackgroundColor(backCharacter.getForegroundColor());
		// log.debug("VISIBILITY {}", rend.visibility);
		drawRenderableAtPosition(mapGraphics, pos, rend, entity);
		if (CM.has(entity, Animatable.class))
		{
			for (Animation animation : CM.get(entity, Animatable.class).animations)
			{
				for (Entity tile : animation.getCurrentFrame())
				{
					Offset tileOffset = CM.get(tile, Offset.class);
					Renderable tileRenderable = CM.get(tile, Renderable.class);
					Position tilePos = pos.plusOffset(tileOffset);
					drawRenderableAtPosition(mapGraphics, tilePos, tileRenderable, tile);
				}
			}
			// return;
		}
		// mapGraphics.setBackgroundColor(oldBG);
	}

	private void drawRenderableAtPosition(TextGraphics mapGraphics, Position pos, Renderable rend, Entity entity)
	{
		String fgColor = rend.fgColor;
		String bgColor = rend.bgColor;
		String fgChar = rend.fgChar;
		TextColor bgTextColor = TextColorUtils.str2Indexed(bgColor, rend.visibility);
		TextColor fgTextColor = TextColorUtils.str2Indexed(fgColor, rend.visibility);
		mapGraphics.setBackgroundColor(bgTextColor);
		mapGraphics.setForegroundColor(fgTextColor);
		EnumSet<SGR> activeModifiers = mapGraphics.getActiveModifiers();
		if (CM.has(entity, Busy.class) && CM.get(entity, Busy.class).getTimeLeft() > 0)
			mapGraphics.enableModifiers(SGR.BLINK);
		// if ((fgChar.charAt(0) & 0xff00) == 0xff00)
		// fgChar += ' ';
//		mapGraphics.putString(pos.roundX() + 1, pos.roundY(), "\n");
//		log.debug("POS: {} / {} / [{}]", pos, rend, str2Indexed);
		mapGraphics.putString(pos.roundX(), pos.roundY(), fgChar);
		mapGraphics.setModifiers(activeModifiers);
	}

	private void drawHighlight(TextGraphics mapGraphics, Entity entity)
	{
		Entity viewport = game.get(ViewportSystem.class).get();
		Position pos = CM.get(entity, Position.class).minusOffset(CM.get(viewport, Offset.class));
		mapGraphics.setForegroundColor(TextColor.ANSI.RED);
		mapGraphics.putString(pos.roundX() - 1, pos.roundY() - 1, "\\");
		mapGraphics.putString(pos.roundX(), pos.roundY() - 1, "|");
		mapGraphics.putString(pos.roundX() + 1, pos.roundY() - 1, "/");
		mapGraphics.putString(pos.roundX() + 1, pos.roundY(), "-");
		mapGraphics.putString(pos.roundX() + 1, pos.roundY() + 1, "\\");
		mapGraphics.putString(pos.roundX(), pos.roundY() + 1, "|");
		mapGraphics.putString(pos.roundX() - 1, pos.roundY() + 1, "/");
		mapGraphics.putString(pos.roundX() - 1, pos.roundY(), "-");
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		return new TerminalSize(40, 30);
	}

}
