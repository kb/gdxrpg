package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.texttable.BorderType;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

public abstract class BaseLanternaComponent implements LanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(BaseLanternaComponent.class);

	protected TextGraphics graphics;
	protected final BaseGame game;
	protected boolean focussed;
	private BorderType borderType;
	private TextGraphics borderGraphics;
	private TerminalSize terminalSize;
	private boolean active;

	public BaseLanternaComponent(BaseGame game)
	{
		this(game, null);
	}

	public BaseLanternaComponent(BaseGame game, BorderType borderType)
	{
		this.game = game;
		this.borderType = borderType;
		this.active = true;
	}

	protected int height()
	{
		return terminalSize.getRows();
	}

	protected int width()
	{
		return terminalSize.getColumns();
	}

	public abstract void draw();

	@Override
	public boolean handleKey(KeyPress vimKey)
	{
		return game.getInputHandler().handleKey(vimKey);
	}

	@Override
	public boolean isFocussed()
	{
		return focussed;
	}

	@Override
	public final void repaint()
	{
		if (null != borderGraphics)
		{
			// borderGraphics.setBackgroundColor(TextColor.ANSI.RED);
			// borderGraphics.setForegroundColor(TextColor.ANSI.YELLOW);
			int maxY = borderGraphics.getSize().getColumns() - 1;
			int maxX = borderGraphics.getSize().getRows() - 1;

			borderGraphics.putString(0, 0, borderType.nw);
			for (int x = 1; x < maxY; x++)
			{
				borderGraphics.putString(x, 0, borderType.n);
			}
			borderGraphics.putString(maxY, 0, borderType.ne);
			for (int y = 1; y < maxX; y++)
			{
				borderGraphics.putString(maxY, y, borderType.w);
			}
			borderGraphics.putString(maxY, maxX, borderType.se);
			for (int x = 1; x < maxY; x++)
			{
				borderGraphics.putString(x, maxX, borderType.s);
			}
			borderGraphics.putString(0, maxX, borderType.sw);
			for (int y = 1; y < maxX; y++)
			{
				borderGraphics.putString(0, y, borderType.w);
			}
		}
		draw();
	}

	@Override
	public void setFocussed(boolean focussed)
	{
		this.focussed = focussed;
	}

	public void setGraphics(TextGraphics graphics)
	{
		if (null != borderType)
		{
			this.borderGraphics = graphics;
			int w = graphics.getSize().getColumns();
			int h = graphics.getSize().getRows();
			this.graphics = graphics.newTextGraphics(new TerminalPosition(1, 1), new TerminalSize(w - 2, h - 2));
		}
		else
		{
			this.graphics = graphics;
		}
		this.terminalSize = graphics.getSize();
	}

	@Override
	public void setActive(boolean active)
	{
		this.active = active;
	}

	@Override
	public boolean isActive()
	{
		return active;
	}
}
