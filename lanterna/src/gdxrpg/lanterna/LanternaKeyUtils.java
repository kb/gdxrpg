package gdxrpg.lanterna;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

public class LanternaKeyUtils {
	
	private static final Logger log = LoggerFactory.getLogger(LanternaKeyUtils.class);
	
	public static final String keyStrokeToVim(KeyStroke key)
	{
		StringBuilder sb = new StringBuilder();
		KeyType keyType = key.getKeyType();
		if (keyType == KeyType.Character) {
			String lowerCase = key.getCharacter().toString().toLowerCase();
			boolean isShiftDown = Character.toLowerCase(key.getCharacter()) != key.getCharacter();
			if (key.isAltDown() || key.isCtrlDown() || isShiftDown) {
				sb.append("<");
				if (isShiftDown) sb.append("S-");
				if (key.isCtrlDown()) sb.append("C-");
				if (key.isAltDown()) sb.append("A-");
				sb.append(lowerCase);
				sb.append(">");
			} else {
				sb.append(lowerCase);
			}
		} else {
			sb.append("<");
			if (key.isAltDown() || key.isCtrlDown()) {
				if (key.isCtrlDown()) sb.append("C-");
				if (key.isAltDown()) sb.append("A-");
			}
			switch (key.getKeyType()) {
				case ArrowDown: sb.append("Down");
					break;
				case ArrowLeft: sb.append("Left");
					break;
				case ArrowRight: sb.append("Right");
					break;
				case ArrowUp: sb.append("Up");
					break;
				case Backspace: sb.append("BS");
					break;
				case Delete: sb.append("Del");
					break;
				case End: sb.append("End");
					break;
				case Enter: sb.append("CR");
					break;
				case Escape: sb.append("Esc");
					break;
				case F1: sb.append("F1");
					break;
				case F2: sb.append("F2");
					break;
				case F3: sb.append("F3");
					break;
				case F4: sb.append("F4");
					break;
				case F5: sb.append("F5");
					break;
				case F6: sb.append("F6");
					break;
				case F7: sb.append("F7");
					break;
				case F8: sb.append("F8");
					break;
				case F9: sb.append("F9");
					break;
				case F10: sb.append("F10");
					break;
				case F11: sb.append("F11");
					break;
				case F12: sb.append("F12");
					break;
				case F13: sb.append("F13");
					break;
				case F14: sb.append("F14");
					break;
				case F15: sb.append("F15");
					break;
				case F16: sb.append("F16");
					break;
				case F17: sb.append("F17");
					break;
				case F18: sb.append("F18");
					break;
				case F19: sb.append("F19");
					break;
				case Home: sb.append("Home");
					break;
				case Insert: sb.append("Insert");
					break;
				case PageDown: sb.append("PageDown");
					break;
				case PageUp: sb.append("PageUp");
					break;
				case Tab: sb.append("Tab");
					break;
				default:
					log.debug("Unhandled key {}", key);
					break;
			}
			sb.append(">");
		}
		return sb.toString().toLowerCase();
	}

}
