package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.constants.GameMode;
import gdxrpg.ecs.event.GameModeChangeListener;
import gdxrpg.ecs.systems.ViewportSystem;
import gdxrpg.utils.FpsCounter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.Screen.RefreshType;
import com.googlecode.lanterna.terminal.ResizeListener;
import com.googlecode.lanterna.terminal.Terminal;

public class LanternaGUI {

	private static final Logger log = LoggerFactory.getLogger(LanternaGUI.class);

	private final BaseGame game;

	private Terminal terminal;
	private Screen screen;
	private TextGraphics screenGraphics;

	private final Map<GameMode, Class<? extends LanternaComponent>> modalComponents = new HashMap<>();
	private final ClassToInstanceMap<LanternaComponent> components = MutableClassToInstanceMap.create();
	private Class<? extends LanternaComponent> focussedComponent;
	private LanternaComponent modalComponent = null;
	private List<Class<? extends BaseLanternaComponent>> zoomOrder = Arrays.asList(null, MapComponent.class, LogComponent.class);
	private int zoomedIndex = 0;

	// Keyboard repeat
	private int keysRepeated = 0;
//	private boolean wasProcessing;
	private FpsCounter fpsCounter = new FpsCounter();

	public LanternaGUI(Terminal terminal, Screen screen, final BaseGame game) throws IOException
	{
		this.game = game;
		addGameModeChangeListener();
		addInputCommandHandler(game);

		this.terminal = terminal;
		this.setScreen(screen);
		this.screenGraphics = screen.newTextGraphics();

		// All components
		this.components.putInstance(MapComponent.class, new MapComponent(game));
		this.components.putInstance(HudComponent.class, new HudComponent(game));
		this.components.putInstance(LogComponent.class, new LogComponent(game));
		this.components.putInstance(CmdlineComponent.class, new CmdlineComponent(game));
		this.components.putInstance(StatusLineComponent.class, new StatusLineComponent(game, new FpsCounter()));

		// Modal Components
		this.modalComponents.put(GameMode.HELP_MODE, HelpComponent.class);
		this.components.putInstance(HelpComponent.class, new HelpComponent(game));
		this.modalComponents.put(GameMode.INVENTORY_MODE, InventoryComponent.class);
		this.components.putInstance(InventoryComponent.class, new InventoryComponent(game));

		// Set initial focus
		this.focussedComponent = MapComponent.class;

		screen.startScreen();
		resetGraphics(terminal);
		terminal.addResizeListener(new ResizeListener() {
			@Override
			public void onResized(Terminal terminal, TerminalSize newSize)
			{
				resetGraphics(terminal);
			}
		});
	}

	private void addInputCommandHandler(final BaseGame game)
	{
//		game.getInputHandler().addInputCommandListener(new InputCommandListener() {
//
//			@Override
//			public void onReceivedCommandInvocation(CommandInvocation invoc)
//			{
//				switch (invoc.getCommand().getClass().getName()) {
//					default:
//						log.trace("Ignore {} in GUI", invoc.command);
//						break;
//				}
//				// if (invoc.command == Enumommand.EXAMINE) {
//				// }
//			}
//		});
	}

	private void addGameModeChangeListener()
	{
		this.game.addGameModeChangeListener(new GameModeChangeListener() {

			@Override
			public void onEnterMode(GameMode mode, Object... args)
			{
				hideModal();
				if (modalComponents.containsKey(mode))
				{
					showModal(modalComponents.get(mode));
				}
				switch (mode) {
					case DEFAULT_MODE:
					case RANGED_ATTACK_MODE:
					case RANGED_EXAMINE_MODE:
					case RUN_TO_MODE:
					case HELP_MODE:
					case INVENTORY_MODE:
					case DIALOG_MODE:
					case DIRECTION_MODE:
						break;
					case SHELL_MODE:
						focus(CmdlineComponent.class);
						break;
				}
			}

			@Override
			public void onExitMode(GameMode oldMode, Object... args)
			{
				log.trace("onExitMode {}", oldMode);
				if (oldMode == GameMode.SHELL_MODE)
					focus(MapComponent.class);
				hideModal();
			}
		});
	}

	private boolean detectKeyRepeat(KeyStroke key)
	{
		if (null == key)
		{
			keysRepeated = 0;
			return false;
		}
		else
		{
			keysRepeated += 1;
		}
		if (keysRepeated > game.getConfig().maxKeyRepeat())
		{
			log.debug("Dropping keypress ({} < {})", keysRepeated, game.getConfig().maxKeyRepeat());
			return true;
		}
		return false;
	}

	private void focus(Class<? extends LanternaComponent> clazz)
	{
		components.getInstance(focussedComponent).setFocussed(false);
		components.getInstance(clazz).setFocussed(true);
		focussedComponent = clazz;
	}

	private void hideModal()
	{
		if (null != this.getModalComponent())
		{
			log.debug("Exit modal mode: {} ", this.getModalComponent());
//			this.game.setProcessing(wasProcessing);
			modalComponent.setActive(false);
			this.setModalComponent(null);
			focus(MapComponent.class);
		}
	}

	private void repaint()
	{
		for (LanternaComponent c : components.values())
		{
			if (c.isActive())
			{
				c.repaint();
			}
		}
		if (null != getModalComponent())
			getModalComponent().repaint();
	}

	private void resetDefaultView(Terminal terminal2) throws IOException
	{
		TerminalSize curSize = terminal2.getTerminalSize();
		log.debug("Resized to {}", curSize);
		int w = curSize.getColumns();
		int h = curSize.getRows();
		screenGraphics = getScreen().newTextGraphics();

		int x1 = w / 4 * 3;
		int y1 = h / 2;
		int y2 = h / 2 - 2;
		int y3 = h - 2;
		int y4 = h - 1;

		try
		{
			Entity viewport = game.get(ViewportSystem.class).get();
			CM.get(viewport, Extent.class).height = y1;
			CM.get(viewport, Extent.class).width = x1;
		} catch (Exception e)
		{
		}

		for (Entry<Class<? extends LanternaComponent>, LanternaComponent> kv : components.entrySet())
		{
			Class<? extends LanternaComponent> clazz = kv.getKey();
			LanternaComponent c = kv.getValue();
			c.setActive(true);
			if (clazz == MapComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, 0), new TerminalSize(x1, y1)));
			}
			else if (clazz == HudComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(x1, 0), new TerminalSize(w - x1, h - 2)));
			}
			else if (clazz == LogComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, y1), new TerminalSize(x1, y2)));
			}
			else if (clazz == CmdlineComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, y3), new TerminalSize(w, 1)));
			}
			else if (clazz == StatusLineComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, y4), new TerminalSize(w, 1)));
			}
			else
			{
				c.setActive(false);
			}
		}

		repaint();

		getScreen().refresh();
	}

	private void resetZoomedView(Terminal terminal2) throws IOException
	{
		// TODO Auto-generated method stub
		TerminalSize curSize = terminal2.getTerminalSize();
		log.debug("Resized to {}", curSize);
		int w = curSize.getColumns();
		int h = curSize.getRows();
		screenGraphics = getScreen().newTextGraphics();
		if (getZoomedComponent() == MapComponent.class)
		{
			try
			{
				Entity viewport = game.get(ViewportSystem.class).get();
				CM.get(viewport, Extent.class).height = h - 2;
				CM.get(viewport, Extent.class).width = w;
			} catch (Exception e)
			{
			}
		}
		for (Entry<Class<? extends LanternaComponent>, LanternaComponent> kv : components.entrySet())
		{
			Class<? extends LanternaComponent> clazz = kv.getKey();
			LanternaComponent c = kv.getValue();
			c.setActive(true);
			if (clazz == getZoomedComponent())
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, 0), new TerminalSize(w, h - 2)));
			}
			else if (clazz == CmdlineComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, h - 2), new TerminalSize(w, 1)));
			}
			else if (clazz == StatusLineComponent.class)
			{
				c.setGraphics(screenGraphics.newTextGraphics(new TerminalPosition(0, h - 1), new TerminalSize(w, 1)));
			}
			else
			{
				c.setActive(false);
			}
		}
	}

	private void showModal(Class<? extends LanternaComponent> clazz)
	{
//		wasProcessing = game.isProcessing();
//		this.game.setProcessing(false);

		LanternaComponent c = components.get(clazz);
		c.setActive(true);
		this.setModalComponent(c);
		log.debug("Enter modal mode: {} ", c);
		resetGraphics(terminal);
		focus(clazz);
	}

	private void drawGUI() throws IOException, InterruptedException
	{
		long now;
		long nsSinceLastUpdate = fpsCounter.tick();

		now = System.nanoTime();
		game.update();
		if (log.isTraceEnabled())
			log.trace("Update took: {} ms", (System.nanoTime() - now) / 1_000_000f);

		// if (game.isProcessing()) {
		now = System.nanoTime();
		repaint();
		if (log.isTraceEnabled())
			log.trace("Draw took: {} ms", (System.nanoTime() - now) / 1_000_000f);

		now = System.nanoTime();
		// getScreen().refresh(RefreshType.DELTA);
		getScreen().refresh(RefreshType.DELTA);
		if (log.isTraceEnabled())
			log.trace("Refresh took: {} ms", (System.nanoTime() - now) / 1_000_000f);
		// }

		KeyStroke key = getScreen().pollInput();
		handleInput(key);

		// Poll until repeat is stopped
		if (detectKeyRepeat(key))
		{
			while (null != key)
			{
				key = getScreen().pollInput();
			}
		}
		// log.debug("{}", keyboardInputLastLoop);
		// if (! keyboardInputLastLoop)

		if (log.isTraceEnabled())
			log.trace("Loop time: {} ms", nsSinceLastUpdate / 1_000_000f);
		// Thread.sleep(1);
		int millis = 1000 / game.getConfig().targetFPS();
//		log.info("Sleeping for {} [{}]", millis, game.getConfig().targetFPS());
		Thread.sleep(millis);
	}

	private boolean handleInput(KeyStroke keyStroke)
	{
		if (detectKeyRepeat(keyStroke))
		{
			return false;
		}
		if (null == keyStroke)
		{
			return false;
		}
		// Redraw screen
		KeyPress key = KeyPress.fromNative(KeyStroke.class, keyStroke);
		log.debug("{}", key.toVimString());
		if (key.equalsVimString("<C-l>"))
		{
			resetGraphics(terminal);
			return false;
		}
		else if (key.equalsVimString("<f2>"))
		{
			focus(MapComponent.class);
			return false;
		}
		else if (key.equalsVimString("Z"))
		{
			zoomedIndex = (zoomedIndex + 1) % zoomOrder.size();
			resetGraphics(terminal);
			return false;
		}
		else
		{
			return components.getInstance(focussedComponent).handleKey(key);
		}
	}

	private void resetGraphics(Terminal terminal2)
	{
		try
		{
			// terminal = terminal2;
			// screen = new TerminalScreen(terminal2);
			getScreen().doResizeIfNecessary();

			// Set graphics for modal if any
			int w = terminal2.getTerminalSize().getColumns();
			int h = terminal2.getTerminalSize().getRows();
			if (null != this.getModalComponent())
				this.getModalComponent().setGraphics(
						screenGraphics.newTextGraphics(new TerminalPosition(w / 8, h / 8), new TerminalSize(w / 8 * 6, h / 8 * 6))
						);

			if (getZoomedComponent() == null)
			{
				resetDefaultView(terminal2);
			}
			else
			{
				resetZoomedView(terminal2);
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private Class<? extends BaseLanternaComponent> getZoomedComponent()
	{
		return zoomOrder.get(zoomedIndex);
	}

	private LanternaComponent getModalComponent()
	{
		return modalComponent;
	}

	private Screen getScreen()
	{
		return screen;
	}

	public void mainLoop() throws InterruptedException, IOException
	{
		resetGraphics(terminal);
		repaint();
		getScreen().refresh();
		while (true)
		{
			drawGUI();
		}
	}

	private void setModalComponent(LanternaComponent modalComponent)
	{
		this.modalComponent = modalComponent;
	}

	private void setScreen(Screen screen)
	{
		this.screen = screen;
	}
}
