package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.CM;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.systems.ItemSystem;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.texttable.BorderType;
import gdxrpg.ecs.texttable.TextTable;

import org.bitbucket.kb.keypress.KeyPress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

public class InventoryComponent extends BaseLanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(InventoryComponent.class);

	private int selectedIdx = 0;

	public InventoryComponent(BaseGame game)
	{
		super(game, BorderType.ASCII);
		setActive(false);
	}

	@Override
	public void draw()
	{
		graphics.fill(' ');
		Entity player = game.get(PlayerSystem.class).get();
		ItemContainer inv = CM.get(player, ItemContainer.class);
		TextTable table = game.get(ItemSystem.class).itemContainerAsTable(player);
		selectedIdx = Math.max(0, Math.min(inv.contents.size() - 1, selectedIdx));
		table.setSelectedRow(selectedIdx);
		LanternaTextTableDrawer.drawTable(graphics, table);
		if (inv.contents.size() > 0)
		{
			Entity selectedItem = inv.contents.get(selectedIdx);
			TextTable itemTable = game.get(ItemSystem.class).itemAsTable(selectedItem);
			int xOffset = width() / 4;
			TextGraphics itemGraphics = graphics.newTextGraphics(new TerminalPosition(xOffset, 0), new TerminalSize(width() - xOffset - 2 , height()));
			LanternaTextTableDrawer.drawTable(itemGraphics, itemTable);
			// Map<String, String> asTable =
			// game.get(ItemSystem.class).itemAsTable(selectedItem);
			// int xOffset = graphicsSize.getColumns() / 2;
			// int y = 0;
			// for (Entry<String, String> kv : asTable.entrySet()) {
			// graphics.putString(xOffset, y, kv.getKey() + ":");
			// graphics.putString(graphicsSize.getColumns() -
			// kv.getValue().length(), y, kv.getValue());
			// y += 1;
		}
		// }
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean handleKey(KeyPress key)
	{
		log.debug("{}", key);
		switch (key.toVimString()) {
			case "j":
				selectedIdx += 1;
				draw();
				break;
			case "k":
				selectedIdx -= 1;
				draw();
				break;
			default:
				return super.handleKey(key);
		}
		return true;
	}
}
