package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.systems.ShellSystem;

import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;

public class CmdlineComponent extends BaseLanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(CmdlineComponent.class);

	public CmdlineComponent(BaseGame game)
	{
		super(game);
	}

	@Override
	public void draw()
	{
		ShellSystem shellSystem = game.get(ShellSystem.class);
		StringBuilder input = shellSystem.getInput();
		String prompt = shellSystem.getShellName() + ">";
		int xOffset = prompt.length();
		if (isFocussed())
		{
			graphics.setBackgroundColor(new TextColor.Indexed(52)).fill(' ');
			if (input.length() == 0)
			{
				graphics.setModifiers(EnumSet.of(SGR.REVERSE));
				drawCursor(xOffset, " ");
			}
			else if (input.length() == shellSystem.getCursor())
			{
				drawCursor(xOffset + input.length(), " ");
			}
		}
		else {
			graphics.setBackgroundColor(new TextColor.Indexed(232)).fill(' ');
		}
		graphics.setForegroundColor(TextColor.ANSI.GREEN);
		graphics.putString(0, 0, prompt);
		graphics.setForegroundColor(TextColor.ANSI.WHITE);
		for (int i = 0; i < input.length(); i++)
		{
			String ch = input.substring(i, i + 1);
			if (isFocussed() && i == shellSystem.getCursor())
			{
				graphics.setModifiers(EnumSet.of(SGR.REVERSE));
				drawCursor(xOffset + i, ch);
			}
			else
			{
				graphics.putString(xOffset + i, 0, ch);
			}
		}
	}

	private void drawCursor(int column, String ch)
	{
		EnumSet<SGR> oldModifiers = graphics.getActiveModifiers();
		graphics.setModifiers(EnumSet.of(SGR.REVERSE));
		graphics.putString(column, 0, ch);
		graphics.setModifiers(oldModifiers);
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
