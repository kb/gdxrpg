package gdxrpg.lanterna;

import java.awt.FontFormatException;
import java.io.IOException;
import java.nio.file.Paths;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFontConfiguration;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame.AutoCloseTrigger;

public class LanternaLauncher {

	private static final Logger log = LoggerFactory.getLogger(LanternaLauncher.class);

	public static void main(String[] arg) throws IOException, InterruptedException, FontFormatException
	{
		((ch.qos.logback.classic.Logger) log).detachAndStopAllAppenders();
		log.debug("Instantiating game");
		LanternaGame lanternaGame = new LanternaGame(Paths.get("../engine/assets/games/lypse"));
		SwingTerminalFontConfiguration fontConfig = SwingTerminalFontConfiguration.newInstance(lanternaGame.getConfig().fonts());
		Terminal terminal = new DefaultTerminalFactory()
				.setSwingTerminalFrameAutoCloseTrigger(AutoCloseTrigger.CloseOnExitPrivateMode)
				.setSwingTerminalFrameFontConfiguration(fontConfig)
				.createTerminal();
		if (terminal instanceof SwingTerminalFrame)
		{
			((SwingTerminalFrame) terminal).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		terminal.setCursorVisible(false);
		TerminalScreen screen = new TerminalScreen(terminal);
		lanternaGame.start();
		LanternaGUI gui = new LanternaGUI(terminal, screen, lanternaGame);
		gui.mainLoop();
	}
}
