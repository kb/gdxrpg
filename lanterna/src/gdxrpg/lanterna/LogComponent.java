package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.texttable.BorderType;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;

public class LogComponent extends BaseLanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(LogComponent.class);

	public LogComponent(BaseGame game)
	{
		super(game, BorderType.SINGLE);
	}

	@Override
	public void draw()
	{
		graphics.setBackgroundColor(new TextColor.Indexed(18)).fill('.');
		graphics.setBackgroundColor(new TextColor.Indexed(233)).fill(' ');
		List<String> msgs = game.getMessageList();
		int rows = graphics.getSize().getRows() - 1;
		int y = 0;
		for (String msg : msgs)
		{
			OUT: for (String line : Lists.reverse(Arrays.asList(msg.split("\n"))))
			{
				String wrapped = WordUtils.wrap(line, width() - 4, "\n", false);
				for (String wrappedline : Lists.reverse(Arrays.asList(wrapped)))
				{
					graphics.putString(2, rows - y, wrappedline);
					if (y++ > rows)
					{
						break OUT;
					}
				}
			}
			graphics.putString(0, rows - y + 1, "< ");
		}
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
