package gdxrpg.lanterna;

import org.bitbucket.kb.keypress.KeyPress;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

public interface LanternaComponent {

	// public abstract void repaint(TextGraphics graphics);
	boolean isActive();
	void setActive(boolean active);
	void setFocussed(boolean focussed);
	boolean isFocussed();
	void setGraphics(TextGraphics graphics);
	void repaint();
	boolean handleKey(KeyPress key);
	TerminalSize getPreferredSize();

}
