package gdxrpg.lanterna;

import gdxrpg.BaseGame;
import gdxrpg.ecs.components.Health;
import gdxrpg.ecs.components.ItemContainer;
import gdxrpg.ecs.components.UniqueLabel;
import gdxrpg.ecs.constants.MyColor;
import gdxrpg.ecs.systems.ItemSystem;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.texttable.BorderType;
import gdxrpg.ecs.texttable.TextTable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;

public class HudComponent extends BaseLanternaComponent {

	private static final Logger log = LoggerFactory.getLogger(HudComponent.class);

	public HudComponent(BaseGame game)
	{
		super(game, BorderType.SINGLE);
	}

	@Override
	public void draw()
	{
		graphics.setBackgroundColor(TextColorUtils.str2Indexed(MyColor.GREY23_222));
		graphics.fill('.');
		graphics.putString(0, 0, "IM THE HUD");
		drawHealthBar(2);
		int totalWeight = game.get(ItemSystem.class).getTotalWeight(game.get(PlayerSystem.class).get(ItemContainer.class));
		graphics.putString(0, 3, "WEIGHT: " + (totalWeight / 1000.0f) + "kg");
		graphics.putString(0, 4, "LEVEL: " + game.get(LevelSystem.class).get(UniqueLabel.class).uniqueLabel);
		TextTable table = game.get(PlayerSystem.class).playerAsTable();
		LanternaTextTableDrawer.drawTable(graphics.newTextGraphics(new TerminalPosition(0, 5), new TerminalSize(width(), height() - 3)), table);
	}

	private void drawHealthBar(int y)
	{
		TextColor olfFg = graphics.getForegroundColor();
		String prefix = "HP: [";
		String suffix = "]";
		graphics.putString(0, y, prefix);
		graphics.putString(width() - 3, y, suffix);
		Health health = game.get(PlayerSystem.class).get(Health.class);
		double healthBarWidth = Math.floor(health.percentage() * (width() - prefix.length() - suffix.length() - 2));
		if (health.percentage() > 0.6f)
		{
			graphics.setForegroundColor(TextColorUtils.str2Indexed("00ff00"));
		}
		else if (health.percentage() > 0.3f)
		{
			graphics.setForegroundColor(TextColorUtils.str2Indexed("ffff00"));
		}
		else
		{
			graphics.setForegroundColor(TextColorUtils.str2Indexed("ff0000"));
		}
		for (int i = 0; i < healthBarWidth; i++)
		{
			graphics.putString(prefix.length() + i, y, "|");
		}
		graphics.setForegroundColor(olfFg);
	}

	@Override
	public TerminalSize getPreferredSize()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
