package gdxrpg.gdx;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

public abstract class ComboKeyAdapter extends InputAdapter {
	
	private static final Logger log = LoggerFactory.getLogger(ComboKeyAdapter.class);
	
	private static final long KEY_TIMEOUT = 1 * 1000 * 1000;
	
	enum Modifier{
		ALT,
		CONTROL,
		SHIFT
	}
	
	private Map<Modifier,Long> modifiers = new HashMap<>();
	private final GdxAshleyGdxGame game;
	public int keyHeld;

	public ComboKeyAdapter(GdxAshleyGdxGame game)
	{
		this.game = game;
		releaseModifier(Modifier.CONTROL);
		releaseModifier(Modifier.ALT);
		releaseModifier(Modifier.SHIFT);
	}

	private void releaseModifier(Modifier mod)
	{
		modifiers.put(mod, -1L);
	}
	
	/**
	 * @param now
	 * @param mods
	 * @return <code>true</code> if any of the provided modifiers were true at nanoTime
	 */
	private boolean isModifierActive(long now, Modifier... mods)
	{
		for (Modifier mod : mods) {
			long delta = now - modifiers.get(mod);
//			log.debug("Time since last press of {}: {}", mod, delta/1000/1000);
			if (delta < KEY_TIMEOUT) {
				return true;
			}
		} 
		return false;
	}
	
	public void update()
	{
		if (keyHeld > 0)
			handleKeyCombo();
	}
	
	abstract boolean keyComboPress(String keyCombo);
	
	@Override
	public boolean keyDown(int keycode)
	{
		switch (keycode) {
	      case Input.Keys.SHIFT_LEFT:
	      case Input.Keys.SHIFT_RIGHT:
              modifierPressed(Modifier.SHIFT);
	    	  return false;
	      case Input.Keys.CONTROL_LEFT:
	      case Input.Keys.CONTROL_RIGHT:
	    	  modifierPressed(Modifier.CONTROL);
	    	  return false;
	      case Input.Keys.ALT_LEFT:
	      case Input.Keys.ALT_RIGHT:
	    	  modifierPressed(Modifier.ALT);
	    	  return false;
	      default:
	    	  keyHeld = keycode;
	    	  return handleKeyCombo();
		}
	}

	private void modifierPressed(Modifier mod)
	{
//		log.debug("Modifiers: {}", modifiers);
		modifiers.put(mod, System.nanoTime());
//		log.debug("Modifiers: {}", modifiers);
	}
	
	private boolean handleKeyCombo() {
		long now = System.nanoTime();
		StringBuilder sb = new StringBuilder();
		if (isModifierActive(now, Modifier.values()))
			sb.append("<");
		if (isModifierActive(now, Modifier.CONTROL))
			sb.append("C-");
		else {
			log.debug("{}", now -modifiers.get(Modifier.CONTROL));
		}
		if (isModifierActive(now, Modifier.SHIFT))
			sb.append("S-");
		if (isModifierActive(now, Modifier.ALT))
			sb.append("A-");
		sb.append(KeycodeToString.get(keyHeld).toLowerCase());
		if (isModifierActive(now, Modifier.values()))
			sb.append(">");
		String combo = sb.toString();
		log.debug("KeyCombo: {} <= Modifiers: {}", combo, modifiers);
		return keyComboPress(combo);
	}
	
	@Override
	public boolean keyUp(int keycode)
	{
		switch (keycode) {
	      case Input.Keys.SHIFT_LEFT:
	      case Input.Keys.SHIFT_RIGHT:
	    	  releaseModifier(Modifier.SHIFT);
	    	  return false;
	      case Input.Keys.CONTROL_LEFT:
	      case Input.Keys.CONTROL_RIGHT:
	    	  releaseModifier(Modifier.CONTROL);
	    	  return false;
	      case Input.Keys.ALT_LEFT:
	      case Input.Keys.ALT_RIGHT:
	    	  releaseModifier(Modifier.ALT);
	    	  return false;
	      default:
	    	  keyHeld = 0;
	    	  return false;
		}
	}

	public GdxAshleyGdxGame getGame()
	{
		return game;
	}

}
