package gdxrpg.gdx;

import gdxrpg.ecs.constants.Sounds;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundsCache {

	private final static Map<Sounds,Sound> cache = new HashMap<>();
	static {
		for (Sounds sound : Sounds.values()) {
			Sound gdxSound = Gdx.audio.newSound(Gdx.files.internal(sound.soundPath));
			cache.put(sound, gdxSound);
		}
	}
	public static Sound get(Sounds sound) {
		return cache.get(sound);
	}
	public static void play(Sounds sound) {
		cache.get(sound).play();
	}
}
