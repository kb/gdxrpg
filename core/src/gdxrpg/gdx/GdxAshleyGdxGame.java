package gdxrpg.gdx;

import gdxrpg.BaseGame;
import gdxrpg.InputHandler;
import gdxrpg.ecs.constants.Sounds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class GdxAshleyGdxGame extends BaseGame implements ApplicationListener {
	protected Screen screen;

	private static final Logger log = LoggerFactory.getLogger(GdxAshleyGdxGame.class);

	// boolean turnComplete = true;
	// public RNG rng = new RNG();
	public static final int VIRTUAL_WIDTH = 800;
	public static final int VIRTUAL_HEIGHT = 600;
	public static final float ASPECT_RATIO = (float) VIRTUAL_WIDTH / (float) VIRTUAL_HEIGHT;

	private InputHandler inputCommandProcessor;

	@Override
	public void create()
	{
		super.init();
		setScreen(new MapScreen(this));
	}

	@Override
	public void render()
	{
		float delta = Gdx.graphics.getDeltaTime();
		if (screen != null)
			screen.render(Gdx.graphics.getDeltaTime());
	}

	/**
	 * 
	 * public abstract class Game implements ApplicationListener
	 * 
	 * {@linkplain com.badlogic.gdx.Game}
	 */

	@Override
	public void dispose()
	{
		if (screen != null)
			screen.hide();
	}

	@Override
	public void pause()
	{
		if (screen != null)
			screen.pause();
	}

	@Override
	public void resume()
	{
		if (screen != null)
			screen.resume();
	}

	@Override
	public void resize(int width, int height)
	{
		if (screen != null)
			screen.resize(width, height);
	}

	/**
	 * Sets the current screen. {@link Screen#hide()} is called on any old
	 * screen, and {@link Screen#show()} is called on the new screen, if any.
	 * 
	 * @param screen
	 *            may be {@code null}
	 */
	public void setScreen(Screen screen)
	{
		if (this.screen != null)
			this.screen.hide();
		this.screen = screen;
		if (this.screen != null) {
			this.screen.show();
			this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
	}

	/** @return the currently active {@link Screen}. */
	public Screen getScreen()
	{
		return screen;
	}

	@Override
	public void quit()
	{
		Gdx.app.exit();
	}

	@Override
	public void playSound(Sounds sound)
	{
		SoundsCache.play(sound);
	}

	@Override
	public String getClipboardContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClipboardContent(String content) {
		// TODO Auto-generated method stub
		
	}

}
