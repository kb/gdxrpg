package gdxrpg.gdx;

import gdxrpg.ecs.CM;
import gdxrpg.ecs.EntitySerializer;
import gdxrpg.ecs.EntityType;
import gdxrpg.ecs.components.Extent;
import gdxrpg.ecs.components.Renderable;
import gdxrpg.ecs.components.Utterance;
import gdxrpg.ecs.components.position.Position;
import gdxrpg.ecs.constants.Fonts;
import gdxrpg.ecs.systems.LevelSystem;
import gdxrpg.ecs.systems.PlayerSystem;
import gdxrpg.ecs.systems.SelectionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MapActor extends Actor {
	private ShapeRenderer shapeRenderer;

	private static final Logger log = LoggerFactory.getLogger(MapActor.class);
	private GdxAshleyGdxGame game;

	private MapScreen screen;

	private OrthographicCamera cam;

	public MapActor(GdxAshleyGdxGame game, MapScreen screen)
	{
		this.game = game;
		this.screen = screen;
		this.cam = new OrthographicCamera();
		shapeRenderer = new ShapeRenderer();
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		FontsCache.get(Fonts.UBUNTU_BOLD).draw(batch, "ABCDEFG", 100, 100);
		log.debug("game: {}", game);
		updateCameraPosition();
		Matrix4 oldMatrix = batch.getProjectionMatrix();
		batch.setProjectionMatrix(cam.combined);
		// fonts.get(Fonts.STANDARD).draw(batch, "" +
		// get(FpsCalculator.class).getFPS() + " fps", 400, 400);
		for (Entity cell : game.getEntitiesFor(EntityType.MAP_GROUND.get())) {
			drawEntity(batch, cell);
		}
		for (Entity npc : game.getEntitiesFor(EntityType.NPC.get())) {
			drawEntity(batch, npc);
		}
		for (Entity item : game.getEntitiesFor(EntityType.CORPSE.get())) {
			drawEntity(batch, item);
		}
		for (Entity item : game.getEntitiesFor(EntityType.ITEM_WITH_POSITION.get())) {
			drawEntity(batch, item);
		}
		for (Entity portal : game.getEntitiesFor(EntityType.PORTAL.get())) {
			drawEntity(batch, portal);
		}
		for (Entity selectedEntity : game.get(SelectionManager.class).getSelection()) {
			drawHighlight(batch, selectedEntity);
		}
		for (Entity utter : game.getEntitiesFor(EntityType.UTTERANCE.get())) {
			drawSpeechBubble(batch, utter);
		}
		{
			Entity player = game.get(PlayerSystem.class).get();
			// log.debug("Player: {}", player);
			drawEntity(batch, player);
		}
		batch.setProjectionMatrix(screen.getStage().getCamera().combined);
		super.draw(batch, parentAlpha);
	}

	private void updateCameraPosition()
	{
		Entity player = game.get(PlayerSystem.class).get();
		Position playerPos = CM.get(player, Position.class);
		Entity level = game.get(LevelSystem.class).get();
		Extent levelExtent = CM.get(level, Extent.class);

		BitmapFont font = FontsCache.get(Fonts.STANDARD);
		float charWidth = font.getSpaceWidth();
		float charHeight = font.getLineHeight();

		float maxX = levelExtent.width * charWidth / 2;
		float maxY = levelExtent.height * charHeight/ 2;
		float minX = Gdx.graphics.getWidth() / 2;
		float minY = Gdx.graphics.getHeight() / 2 - 100;

		double x = playerPos.x * charWidth;
		double y = playerPos.y * charHeight;
		Vector3 camPos = new Vector3((float) x, (float) y, 0);

		// TODO
		if (camPos.x < 100) {
			camPos.x = minX;
		}
//		} else if (camPos.x > maxX) {
//			camPos.x = maxX;
//		}
//		if (camPos.y < minY) {
//			camPos.y = 100;
//		} else if (y > maxY) {
//			camPos.y = maxY;
//		}
		// log.debug("maxX: {}", camPos);
		cam.setToOrtho(false, 800, 600);
		cam.position.set(camPos);
		cam.update();
	}

	private void drawEntity(Batch batch, Entity entity)
	{
		// MainGame game = this;
		if (!CM.has(entity, Position.class) || !CM.has(entity, Renderable.class)) {
			log.debug("Has no pos or renderable: {}", EntitySerializer.dump(entity));
			return;
		}

		// Entity viewport =
		// game.game.getSystem(ViewportSystem.class).getCurrentViewport();
		// Offset offset = CM.get(viewport, Offset.class);

		Renderable rend = CM.get(entity, Renderable.class);
		Position pos = CM.get(entity, Position.class);
		// log.debug("Renderable: {}", rend);

		// if (! game.game.getSystem(ViewportSystem.class).isWithin(viewport,
		// pos))
		// {
		// return;
		// }
		// if (CM.has(entity, Player.class)) {
		// log.debug("Renderable: {}", rend.texture);
		// }
		if (null == pos) {
			log.error("Has no pos: {}", entity.getComponents());
		}
		BitmapFont font = FontsCache.get(Fonts.STANDARD);
		int x = (int) (pos.roundX() * font.getSpaceWidth());
		int y = (int) (pos.roundY() * font.getLineHeight());

		batch.setColor(Color.valueOf(rend.fgColor));
		// batch.setColor(y);
		// batch.draw(rend.texture, x, y);
		font.setColor(Color.valueOf(rend.fgColor));
		font.draw(batch, rend.fgChar, x, y);

		// Health health = CM.get(entity, Health.class);
		// if (health != null && ! health.dead) {
		// g2.drawImage(getHealthBar(health, fgColor.getAlpha()), x, y - 1,
		// null);
		// }
	}

	private void drawHighlight(Batch batch, Entity npc)
	{
		Position pos = CM.get(npc, Position.class);
		// ShapeRenderer renderer = new ShapeRenderer();
		batch.end();
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glDisable(GL20.GL_BLEND);
		Gdx.gl.glLineWidth(5);
		shapeRenderer.setColor(Color.RED);
		// shapeRenderer.setProjectionMatrix(cam.combined);
		shapeRenderer.begin(ShapeType.Line);
		BitmapFont font = FontsCache.get(Fonts.STANDARD);
		float width = font.getSpaceWidth();
		float height = font.getLineHeight();
		shapeRenderer.circle((float) pos.x * width + width / 2, (float) pos.y * height + height / 2, (float) Math.max(height, width));
		shapeRenderer.end();
		batch.begin();
		// renderer.begin();
		// renderer.setProjectionMatrix(cam.combined);
		// renderer.end();

	}

	private void drawSpeechBubble(Batch batch, Entity entity)
	{
		// TODO Auto-generated method stub
		if (CM.has(entity, Utterance.class)) {
			Utterance utter = CM.get(entity, Utterance.class);
			Entity source = game.getEntity(utter.sourceId);
			Position pos = CM.get(source, Position.class);
			BitmapFont font = FontsCache.get(Fonts.SPEECH);
			float charWidth = font.getSpaceWidth();
			float x = pos.roundX() * charWidth;
			float charHeight = font.getLineHeight();
			float y = pos.roundY() * charHeight;
			font.draw(batch, "/", x + 1 * charWidth, y + 1 * charHeight);
			font.draw(batch, utter.text, x + 1 * charWidth, y + 2 * charHeight);
		}

	}

}
