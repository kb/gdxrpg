package gdxrpg.gdx;

import gdxrpg.MainInputHandler;
import gdxrpg.command.Command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyKeyAdapter extends ComboKeyAdapter {
	
	private static final Logger log = LoggerFactory.getLogger(MyKeyAdapter.class);
	
	public MyKeyAdapter(GdxAshleyGdxGame game)
	{
		super(game);
	}

	@Override
	boolean keyComboPress(String keyCombo)
	{
		log.debug(keyCombo);
		for (Command inp : Command.values()) {
			// TODO
//			MainInputHandler inputHandler = getGame().getInputHandler();
//			if (inp.defaultKey.equals(keyCombo) && inp.shouldFireInMode(getGame().modeStack)) {
//				inputHandler.handleInputCommand(inp);
//			}
		}
		return false;
	}

}
