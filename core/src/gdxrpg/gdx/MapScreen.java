package gdxrpg.gdx;

import gdxrpg.ecs.systems.FpsCounterSystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class MapScreen implements Screen {
	
	private static final Logger log = LoggerFactory.getLogger(MapScreen.class);
	
	private Actor mapActor;
	private GdxAshleyGdxGame game;
	private boolean focusPrompt = false;
	private MyKeyAdapter keyProcessor;

	private Skin skin;

	private TextButton fpsButton;
	private TextButton button2;
	private TextButton button3;
	private TextButton button4;
	private TextButton button5;

	private Stage stage;
	private Cell<?> mapCell;
	private TextField promptInput;
	private Table promptUI;
	private Table logUI;
	private ScrollPane scrollLogUI;
	private Table layout;
	
	public MapScreen(GdxAshleyGdxGame game)
	{
		this.game = game;
		keyProcessor = new MyKeyAdapter(game);
	}

	@Override
	public void show()
	{

		this.stage = new Stage();
		this.mapActor = new MapActor(game, this);
		
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(new InputAdapter(){
			@Override
			public boolean keyDown(int keycode)
			{
				if (keycode == Input.Keys.ESCAPE) {
					if (focusPrompt) {
						stage.setKeyboardFocus(mapActor);
					} else {
						promptUI.setVisible(true);
						stage.setKeyboardFocus(promptInput);
						logUI.setHeight(50);
						layout.invalidateHierarchy();
						layout.layout();
					}
					return true;
				}
				return super.keyDown(keycode);
			}
		});
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(keyProcessor);
		Gdx.input.setInputProcessor(inputMultiplexer);

		skin = new Skin(Gdx.files.internal("uiskin.json"));
		layout = new Table();
		layout.setFillParent(true);
		layout.setDebug(true); // turn on all debug lines (table, cell, and
								// widget)
		getStage().addActor(mapActor);
		getStage().addActor(layout);
//		stage.getC

		fpsButton = new TextButton("Click me!", skin);
		button2 = new TextButton("Click me!", skin);
		button3 = new TextButton("Click me!", skin);
		button4 = new TextButton("Click me!", skin);
		button5 = new TextButton("Click me!", skin);

		Table rightUI = new Table();
		rightUI.add(fpsButton);
		rightUI.row();
		rightUI.add(button2);
		rightUI.row();
		rightUI.add(button3);
		rightUI.row();
		rightUI.add(button4);
		rightUI.row();
		rightUI.add(button5);
		rightUI.row();
		
		logUI = new Table();
		logUI.setHeight(100);
		Label msg1 = new Label("", skin);
		msg1.setText("0xxxxxxxxxxxxxxx\n1xxxxxxxxxxxxxx\n2xxxxxxxxxxxxxx\n33xxxxxxxxxxxxx\n4xxxxxxxxxxxxxx\n5xxxxxxxxxxxxxx\n6xxxxxxxxxxxxxx\n7xxxxxxxxxxxxxx\n8xxxxxxxxxxxxxx\n9xxxxxxxxxxxxxx\n10xxxxxxxxxxxxxx\n11xxxxxxxxxxxxxx\n12xxxxxxxxxxxxxx\n13xxxxxxxxxxxxxx");
		logUI.add(msg1).fillX().expandX();
		scrollLogUI = new ScrollPane(logUI);
		scrollLogUI.setScrollBarPositions(true, true);
		scrollLogUI.setForceScroll(false, true);
		scrollLogUI.layout();
		scrollLogUI.setHeight(100);

		
		promptUI = new Table();
		Label promptLabel = new Label(">", skin);
		promptInput = new TextField("",skin);
		promptUI.add(promptLabel);
		promptUI.add(promptInput).fillX().expandX();

		Table bottomUI = new Table();
		bottomUI.add(scrollLogUI).fillX().colspan(2).height(100);
		bottomUI.row();
		bottomUI.add(promptUI).fillX().expandX();;
		promptUI.setVisible(false);

		mapCell = layout.add();
		mapCell.expandX().expandY();
		layout.add(rightUI);
		layout.row();
		layout.add(bottomUI).colspan(2).expandX().fillX();
		layout.pack();

		// Add a listener to the button. ChangeListener is fired when the
		// button's checked state changes, eg when clicked,
		// Button#setChecked() is called, via a key press, etc. If the
		// event.cancel() is called, the checked state will be reverted.
		// ClickListener could have been used, but would only fire when clicked.
		// Also, canceling a ClickListener event won't
		// revert the checked state.
		fpsButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor)
			{
				System.out.println("Clicked! Is checked: " + fpsButton.isChecked());
				fpsButton.setText("Good job!");
			}
		});
		scrollLogUI.setScrollPercentY(100);

		// Add an image actor. Have to set the size, else it would be the size
		// of the drawable (which is the 1x1 texture).
		// table.add(new Image(skin.newDrawable("white", Color.RED))).size(64);
	}

	@Override
	public void render(float delta)
	{
//		stage.getBatch().begin();
//		mapActor.draw(stage.getBatch(), delta);
//		stage.getBatch().end();
//		stage.getBatch().setProjectionMatrix(camera.combined);
//		stage.getBatch().setProjectionMatrix(camera.combined);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Limit framerate
		try {
			Thread.sleep((long) (1000 / 30 - delta));
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		// log.debug("DELTA YAY {}", delta);
		if (game.isProcessing())
			game.update(delta);
		// Maximum: 10 times per second
		if (Gdx.graphics.getFrameId() % (Math.max(10, Gdx.graphics.getFramesPerSecond()) / 10) == 0)
			keyProcessor.update();
		fpsButton.setText("OPS: " + game.get(FpsCounterSystem.class).getFPS());
		button2.setText("FPS: " + Gdx.graphics.getFramesPerSecond());
		getStage().act();
		getStage().draw();
	}

	@Override
	public void resize(int width, int height)
	{
//		stage.getViewport().setScreenPosition(0, 0);
//		stage.getViewport().setScreenWidth(width);
//		stage.getViewport().setScreenHeight(height);
//		camera.setToOrtho(false, width, height);
		// calculate new viewport
//		float aspectRatio = (float) width / (float) height;
//		float scale = 4f;
//		Vector2 crop = new Vector2(0f, 0f);
//		if (aspectRatio > game.ASPECT_RATIO)
//		{
//			scale = (float) height / (float) game.VIRTUAL_HEIGHT;
//			crop.x = (width - game.VIRTUAL_WIDTH * scale) / 2f;
//		}
//		else if (aspectRatio < game.ASPECT_RATIO)
//		{
//			scale = (float) width / (float) game.VIRTUAL_WIDTH;
//			crop.y = (height - game.VIRTUAL_HEIGHT * scale) / 2f;
//		}
//		else
//		{
//			scale = (float) width / (float) game.VIRTUAL_WIDTH;
//		}
//
//		float w = (float) game.VIRTUAL_WIDTH * scale;
//		float h = (float) game.VIRTUAL_HEIGHT * scale;
//		viewport = new Rectangle(crop.x, crop.y, w, h);
		//
		// cam = new OrthographicCamera();
		// // cam = new OrthographicCamera(600 * aspectRatio,600f);
//		camera.setToOrtho(false, game.VIRTUAL_WIDTH, game.VIRTUAL_HEIGHT);
		// updateCameraPosition();
		// cam.update();
		// // cam.setToOrtho(true, Gdx.graphics.getWidth(),
		// // Gdx.graphics.getHeight());
		// // cam.rotate(180f);
	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}

	public Stage getStage()
	{
		return stage;
	}

}
