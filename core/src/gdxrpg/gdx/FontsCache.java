package gdxrpg.gdx;

import gdxrpg.ecs.constants.Fonts;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class FontsCache {

	private static Map<Fonts, BitmapFont> fontCache = new HashMap<>();
	static {
		for (Fonts font : Fonts.values()) {
			if (null != Gdx.files) {
				FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(font.fontPath));
				FreeTypeFontParameter parameter = new FreeTypeFontParameter();
				parameter.size = font.fontSize;
				parameter.borderWidth = 2;
				parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS + "\u2591\u2592\u2593";
				BitmapFont bitmapFont = generator.generateFont(parameter);
				bitmapFont.getData().markupEnabled = true;
				fontCache.put(font, bitmapFont);
				generator.dispose(); // don't forget to dispose to avoid memory
										// leaks!
			} else {
				final Logger log = LoggerFactory.getLogger(Fonts.class);
				log.error("Gdx not initialized!");
				throw new RuntimeException("Gdx not initilaized.");
			}
		}
	}

	public static BitmapFont get(Fonts font) {
		return fontCache.get(font);
	}

}
