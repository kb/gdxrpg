package gdxrpg.ecs;

import static org.junit.Assert.assertEquals;
import gdxrpg.ecs.components.AIComponent;
import gdxrpg.ecs.components.Passable;
import gdxrpg.ecs.components.position.Position;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.ashley.core.Entity;


public class EntityFactoryTest {
	
	private static final Logger log = LoggerFactory.getLogger(EntityFactoryTest.class);
	
	private EntityFactory factory;

//	@Test
//	public void testFromMap() throws Exception
//	{
//		Map<String, String> map = new HashMap<>();
//		map.put("x", "10");
//		map.put("y", "20");
//		map.put("pickup", "false");
//		Entity fromMap = factory.fromMap("ITEM", map);
//		log.debug("{}", CM.get(fromMap, Position.class));
//	}
	
	@Test
	public void testGenson()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("{").append("\n");
		sb.append("  \"passable\": true,").append("\n");
		sb.append("  \"position\": {").append("\n");
		sb.append("    \"x\": 11,").append("\n");
		sb.append("    \"y\": 20").append("\n");
		sb.append("  }").append("\n");
		sb.append("}");
		log.debug(sb.toString());
		Entity entity = EntitySerializer.fromSimpleJSON(sb.toString());
		assertEquals(true, CM.get(entity, Passable.class).passable);
		assertEquals(11, CM.get(entity, Position.class).x, 0);
		
	}
	@Test
	public void testGensonWithEnum()
	{
		AIComponent ai = new AIComponent("BEE_LINE");
		StringBuilder sb = new StringBuilder();
		sb.append("{").append("\n");
		sb.append("  \"aIComponent\": {").append("\n");
		sb.append("     \"strategy\": \"BEE_LINE\"").append("\n");
		sb.append("  }").append("\n");
		sb.append("}").append("\n");
		log.debug("{}", sb.toString());
		Entity ai2 = EntitySerializer.fromSimpleJSON(sb.toString());
		log.debug("{}", ai2.getComponents());
	}
	
	@Test
	public void snippetTest()
	{
		
		float _doubleValue = 0f;
//		if (Float.MIN_VALUE > (float)_doubleValue || (float)_doubleValue > Float.MAX_VALUE)
//			throw new RuntimeException("Foo");
	}
}
