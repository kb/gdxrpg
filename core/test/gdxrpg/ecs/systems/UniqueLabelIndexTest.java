package gdxrpg.ecs.systems;

import gdxrpg.ecs.components.UniqueLabel;

import org.junit.Test;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;


public class UniqueLabelIndexTest extends GdxRpgTestCase {
	
	public UniqueLabelIndexTest()
	{
		setLogLevel("TRACE");
	}

	@Test(expected=IllegalArgumentException.class)
	public void testUniqueViolation() throws Exception
	{
		Engine engine = new Engine();
		UniqueLabelIndex labelIndex = new UniqueLabelIndex();
		engine.addSystem(labelIndex);
		
		Entity entity = new Entity();
		Entity entity2 = new Entity();
		entity.add(new UniqueLabel("foo-1"));
		entity2.add(new UniqueLabel("foo-1"));
		engine.addEntity(entity);
		engine.addEntity(entity);
		log.debug(labelIndex.toString());
	}

}
