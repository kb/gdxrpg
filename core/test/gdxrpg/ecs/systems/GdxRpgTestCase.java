package gdxrpg.ecs.systems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

public abstract class GdxRpgTestCase {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	protected void setLogLevel(String level) {
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
		root.setLevel(Level.valueOf(level));

	}
	
}
