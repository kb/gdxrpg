package gdxrpg.utils;

import gdxrpg.utils.CoffeeLoader;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CoffeeLoaderTest {

	private static final Logger log = LoggerFactory.getLogger(CoffeeLoaderTest.class);
	private static final CoffeeLoader coffeeLoader = new CoffeeLoader();

	@Test
		public void testCompileCoffeeScript() throws Exception {
			StopWatch sw = new StopWatch();
			sw.start();
			String script = coffeeLoader.compileCoffeeScript("/test.coffee");
			final long d0 = sw.getTime();
			log.debug("Inital loading: {} ms", d0);
			sw.reset();
			coffeeLoader.compileCoffeeScript("/test.coffee");
			final long d1 = sw.getTime();
			log.debug("Further loading: {} ms", d1);
			Assert.assertEquals(true, d1 < d0);
		}

}
