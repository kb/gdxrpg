package com.mygdx.game.desktop;

import gdxrpg.gdx.GdxAshleyGdxGame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	private static final Logger log = LoggerFactory.getLogger(DesktopLauncher.class);
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 600;
		log.debug("Instantiating game");
		GdxAshleyGdxGame game = new GdxAshleyGdxGame();
		log.debug("Launching LwjglApplication");
		new LwjglApplication(game, config);
	}
}
